function [F_sh]=cylindric_wedge(zen,svfalfa)

% Fraction of sunlit walls based on sun altitude and svf wieghted building angles
% input: 
% sun zenith angle "beta"
% svf related angle "alfa"
warning off MATLAB:divideByZero

beta=zen;
alfa=svfalfa;

% measure the size of the image
sizex=size(svfalfa,2);
sizey=size(svfalfa,1);

xa=1-2./(tan(alfa).*tan(beta));
ha=2./(tan(alfa).*tan(beta));
ba=(1./tan(alfa));
hkil=2.*ba.*ha;

qa=(zeros(sizey,sizex));
% qa(length(svfalfa),length(svfalfa))=0;
qa(xa<0)=tan(beta)/2;

Za=(zeros(sizey,sizex));
% Za(length(svfalfa),length(svfalfa))=0;
Za(xa<0)=((ba(xa<0).^2)-((qa(xa<0).^2)./4)).^0.5;

phi=(zeros(sizey,sizex));
%phi(length(svfalfa),length(svfalfa))=0;
phi(xa<0)=atan(Za(xa<0)./qa(xa<0));

A=(zeros(sizey,sizex));
% A(length(svfalfa),length(svfalfa))=0;
A(xa<0)=(sin(phi(xa<0))-phi(xa<0).*cos(phi(xa<0)))./(1-cos(phi(xa<0)));

ukil=(zeros(sizey,sizex));
% ukil(length(svfalfa),length(svfalfa))=0;
ukil(xa<0)=2*ba(xa<0).*xa(xa<0).*A(xa<0);

Ssurf=hkil+ukil;

F_sh=(2*pi*ba-Ssurf)./(2*pi*ba);%Xa


