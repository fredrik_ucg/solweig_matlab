function [gvf,gvfLup,gvfalb,gvfalbnosh] = sunonsurface_2015a(azimuthA,scale,buildings,shadow,sunwall,first,second,aspect,walls,Tg,Tgwall,Ta,emis_grid,ewall,alb_grid,SBC,albedo_b,Twater,lc_grid,landcover)
% This version of sunonsurfce goes with SOLWEIG 2015a. It also simulates
% Lup and albedo based on landcover information and shadow patterns.
% Fredrik Lindberg, fredrikl@gvc.gu.se

sizex=size(buildings,1);sizey=size(buildings,2);
wallbol=double(walls>0);

% conversion into radians
azimuth=azimuthA*(pi/180);

% loop parameters
index=0;
f=buildings;
Lup=SBC*emis_grid.*(Tg.*shadow+Ta+273.15).^4-SBC*emis_grid.*(Ta+273.15).^4;%+Ta
if landcover == 1
    Tg(lc_grid==3)=Twater-Ta; % Setting water temperature
end
Lwall=SBC*ewall*(Tgwall+Ta+273.15)^4-SBC*ewall*(Ta+273.15)^4;%+Ta
albshadow=alb_grid.*shadow;
alb=alb_grid;
% sh(sh<=0.1)=0;
% sh=sh-(1-vegsh)*(1-psi);
% shadow=sh-(1-vegsh)*(1-psi);
% dx=0;
% dy=0;
% ds=0; %#ok<NASGU>

tempsh=zeros(sizex,sizey);
tempbu=tempsh;
tempbub=tempsh;
tempbubwall=tempsh;
tempwallsun=tempsh;
weightsumsh=tempsh;
weightsumwall=tempsh;
first=first*scale;
if first<1
    first=1;
end
second=second*scale;
%tempTgsh=tempsh;
weightsumLupsh=tempsh;
weightsumLwall=tempsh;
weightsumalbsh=tempsh;
weightsumalbwall=tempsh;
weightsumalbnosh=tempsh;
weightsumalbwallnosh=tempsh;

% other loop parameters
pibyfour=pi/4;
threetimespibyfour=3*pibyfour;
fivetimespibyfour=5*pibyfour;
seventimespibyfour=7*pibyfour;
sinazimuth=sin(azimuth);
cosazimuth=cos(azimuth);
tanazimuth=tan(azimuth);
signsinazimuth=sign(sinazimuth);
signcosazimuth=sign(cosazimuth);

%% The Shadow casting algoritm
for n=1:second
    if ((pibyfour<=azimuth)&&(azimuth<threetimespibyfour))||((fivetimespibyfour<=azimuth)&&(azimuth<seventimespibyfour))
        dy=signsinazimuth*index;
        dx=-1*signcosazimuth*abs(round(index/tanazimuth));
    else
        dy=signsinazimuth*abs(round(index*tanazimuth));
        dx=-1*signcosazimuth*index;
    end
    
    absdx=abs(dx);
    absdy=abs(dy);
    
    xc1=((dx+absdx)/2)+1;
    xc2=(sizex+(dx-absdx)/2);
    yc1=((dy+absdy)/2)+1;
    yc2=(sizey+(dy-absdy)/2);
    
    xp1=-((dx-absdx)/2)+1;
    xp2=(sizex-(dx+absdx)/2);
    yp1=-((dy-absdy)/2)+1;
    yp2=(sizey-(dy+absdy)/2);
    
    tempbu(xp1:xp2,yp1:yp2)=buildings(xc1:xc2,yc1:yc2);%moving building
    tempsh(xp1:xp2,yp1:yp2)=shadow(xc1:xc2,yc1:yc2);%moving shadow
    tempLupsh(xp1:xp2,yp1:yp2)=Lup(xc1:xc2,yc1:yc2);%moving Lup/shadow
    tempalbsh(xp1:xp2,yp1:yp2)=albshadow(xc1:xc2,yc1:yc2);%moving Albedo/shadow
    tempalbnosh(xp1:xp2,yp1:yp2)=alb(xc1:xc2,yc1:yc2);%moving Albedo
    f=min(f,tempbu);%utsmetning av buildings
    
    shadow2=tempsh.*f;
    weightsumsh=weightsumsh+shadow2;
    
    Lupsh=tempLupsh.*f;
    weightsumLupsh=weightsumLupsh+Lupsh;
    
    albsh=tempalbsh.*f;
    weightsumalbsh=weightsumalbsh+albsh;
 
    albnosh=tempalbnosh.*f;
    weightsumalbnosh=weightsumalbnosh+albnosh;    
    
    tempwallsun(xp1:xp2,yp1:yp2)=sunwall(xc1:xc2,yc1:yc2);%moving buildingwall insun image
    tempb=tempwallsun.*f;
    tempbwall=f*-1+1;
    tempbub=(tempb+tempbub)>0==1;
    tempbubwall=(tempbwall+tempbubwall)>0==1;
    weightsumLwall=weightsumLwall+tempbub*Lwall;
    weightsumalbwall=weightsumalbwall+tempbub*albedo_b;
    weightsumwall=weightsumwall+tempbub;
    weightsumalbwallnosh=weightsumalbwallnosh+tempbubwall*albedo_b;
    
    ind = 1;
    if n<=first
        weightsumwall_first=weightsumwall/ind;
        weightsumsh_first=weightsumsh/ind;
        wallsuninfluence_first=weightsumwall_first>0;
        weightsumLwall_first=(weightsumLwall)/ind;%*Lwall
        weightsumLupsh_first=weightsumLupsh/ind;
        
        weightsumalbwall_first=weightsumalbwall/ind;%*albedo_b
        weightsumalbsh_first=weightsumalbsh/ind;
        weightsumalbwallnosh_first=weightsumalbwallnosh/ind;%*albedo_b
        weightsumalbnosh_first=weightsumalbnosh/ind;
        wallinfluence_first=weightsumalbwallnosh_first>0;
        %         gvf1=(weightsumwall+weightsumsh)/first;
        %         gvf1(gvf1>1)=1;
        ind=ind+1;
    end
    index=index+1;
    
%     p1=subplot(2,3,1);
%     imagesc(shadow2),colorbar,axis image, axis off,title('shadow2')
%     p2=subplot(2,3,2);
%     imagesc(weightsumwall+weightsumsh),colorbar,axis image, axis off,title('weightsumwall+weightsumsh')
%     p3=subplot(2,3,3);
%     imagesc(weightsumLupsh),colorbar,axis image, axis off,title('weightsumLupsh')
%     p4=subplot(2,3,4);
%     imagesc(weightsumalbwallnosh),colorbar,axis image, axis off,title('weightsumalbwall')
%     p5=subplot(2,3,5);
%     imagesc(weightsumalbnosh),colorbar,axis image, axis off,title('weightsumalbsh')
%     p6=subplot(2,3,6);
%     imagesc(weightsumalbwallnosh+weightsumalbnosh),colorbar,axis image, axis off,title('weightsumwall')
%     linkaxes([p1 p2 p3 p4 p5 p6])
%     pause(0.001)
    
end
wallsuninfluence_second=weightsumwall>0;
wallinfluence_second=weightsumalbwallnosh>0;
% gvf2(gvf2>1)=1;

% Removing walls in shadow due to selfshadowing
azilow=azimuth-pi/2;
azihigh=azimuth+pi/2;
if azilow>=0 && azihigh<2*pi % 90 to 270  (SHADOW)
    facesh=double((aspect<azilow | aspect>=azihigh)-wallbol+1);
    facesh(facesh==2)=0;
elseif azilow<0 && azihigh<=2*pi % 0 to 90
    azilow=azilow+2*pi;
    facesh=(aspect>azilow | aspect<=azihigh)*-1+1; %(SHADOW)
elseif azilow>0 && azihigh>=2*pi % 270 to 360
    azihigh=azihigh-2*pi;
    facesh=(aspect>azilow | aspect<=azihigh)*-1+1;%(SHADOW)
end

% removing walls in self shadoing
keep=(weightsumwall==second)-facesh;
keep(keep==-1)=0;

% gvf from shadow only
gvf1=((weightsumwall_first+weightsumsh_first)/(first+1)).*wallsuninfluence_first+...
    (weightsumsh_first)/(first).*(wallsuninfluence_first*-1+1);
weightsumwall(keep==1)=0;
gvf2=((weightsumwall+weightsumsh)/(second+1)).*wallsuninfluence_second+...
    (weightsumsh)/(second).*(wallsuninfluence_second*-1+1);

% gvf from shadow and Lup
gvfLup1=((weightsumLwall_first+weightsumLupsh_first)/(first+1)).*wallsuninfluence_first+...
    (weightsumLupsh_first)/(first).*(wallsuninfluence_first*-1+1);
weightsumLwall(keep==1)=0;
gvfLup2=((weightsumLwall+weightsumLupsh)/(second+1)).*wallsuninfluence_second+...
    (weightsumLupsh)/(second).*(wallsuninfluence_second*-1+1);

% gvf from shadow and albedo
gvfalb1=((weightsumalbwall_first+weightsumalbsh_first)/(first+1)).*wallsuninfluence_first+...
    (weightsumalbsh_first)/(first).*(wallsuninfluence_first*-1+1);
weightsumalbwall(keep==1)=0;
gvfalb2=((weightsumalbwall+weightsumalbsh)/(second+1)).*wallsuninfluence_second+...
    (weightsumalbsh)/(second).*(wallsuninfluence_second*-1+1);

% gvf from albedo only
gvfalbnosh1=((weightsumalbwallnosh_first+weightsumalbnosh_first)/(first+1)).*wallinfluence_first+....*wallsuninfluence_first
    (weightsumalbnosh_first)/(first).*(wallinfluence_first*-1+1);%
gvfalbnosh2=((weightsumalbwallnosh+weightsumalbnosh)/(second)).*wallinfluence_second+...
    (weightsumalbnosh)/(second).*(wallinfluence_second*-1+1);

% Weighting
gvf=(gvf1*0.5+gvf2*0.4)/0.9;
gvfLup=(gvfLup1*0.5+gvfLup2*0.4)/0.9;
gvfLup=gvfLup+((SBC*emis_grid.*(Tg.*shadow+Ta+273.15).^4)-SBC*emis_grid.*(Ta+273.15).^4).*(buildings*-1+1);%+Ta
gvfalb=(gvfalb1*0.5+gvfalb2*0.4)/0.9;
gvfalb=gvfalb+alb_grid.*(buildings*-1+1).*shadow;
gvfalbnosh=(gvfalbnosh1*0.5+gvfalbnosh2*0.4)/0.9;
gvfalbnosh=gvfalbnosh.*buildings+alb_grid.*(buildings*-1+1);

