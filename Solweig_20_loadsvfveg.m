function [svfveg,svfEveg,svfSveg,svfWveg,svfNveg,svfaveg,svfEaveg,svfSaveg,svfWaveg,svfNaveg,scale,header,headernum,headername,sizey,sizex]=Solweig_20_loadsvfveg(inputfiles)
% This function loads five SVF grids one non-directional and four others
% from the cardinal points (N,E,S,W)
%20130208 - changed to use cell input

name=[];
for i=1:10
    [name(:,:,i), header]=Esriasciiimport(inputfiles{i});
    
    sizex=size(name,2);
    sizey=size(name,1);
    
    % put the headernumber in a vector
    for j=1:6
        [cell,rest]=strtok(header{j},' ');
        headernum(j,:)=cellstr(rest);
        headername(j,:)=cellstr(cell);
        if i == 5
            scale=1/str2num(rest);
        end
    end
    
end

svfveg=name(:,:,1);
svfEveg=name(:,:,2);
svfSveg=name(:,:,3);
svfWveg=name(:,:,4);
svfNveg=name(:,:,5);
svfaveg=name(:,:,6);
svfEaveg=name(:,:,7);
svfSaveg=name(:,:,8);
svfWaveg=name(:,:,9);
svfNaveg=name(:,:,10);