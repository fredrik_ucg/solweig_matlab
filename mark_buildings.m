function [build,index,loctemp]=mark_buildings(build,index,loctemp,sizex,sizey)
temp=build(:,:,index-1);
% set(gcf,'Toolbar','none','menubar','none','Name','Vegetation DEM generation','Numbertitle','off');
[temp2,loc]=imfill(temp);
loctemp(index,1:length(loc))=loc;
build(:,:,index)=temp2;

imagesc(build(:,:,index));
axis image
index=index+1;
% (1:sizey,1:sizex,index)