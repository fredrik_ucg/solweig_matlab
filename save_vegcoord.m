function []=save_vegcoord(vegcoord_out,vegunit_outputfile)
veg_header=['ID ','ttype ','dia ','height ','trunk ','x ','y ','build'];
fn2=fopen(vegunit_outputfile,'w');
fprintf(fn2,'%6s', veg_header);
fprintf(fn2,'\r\n');
for p=1:length(vegcoord_out(:,1))
    fprintf(fn2,'%0.1f ',vegcoord_out(p,:));
    fprintf(fn2,'\r\n');
end
fclose(fn2);