function f=shadowingfunction(a,azimuth,altitude,scale)
%This m.file calculates shadows on a DEM

% conversion
degrees=pi/180;
% Degrees factor moved out to line 5
% azimuth=azimuth*(pi/180);
% altitude=altitude*(pi/180);
azimuth=azimuth*degrees;
altitude=altitude*degrees;

% measure the size of the image
sizex=size(a,1);
sizey=size(a,2);

% initialise parameters
f=a;
dx=0;
dy=0;
dz=0;
ds=0;
temp=zeros(sizex,sizey);
index=1;

% other loop parameters
amaxvalue=max(max(a));
pibyfour=pi/4;
threetimespibyfour=3*pibyfour;
fivetimespibyfour=5*pibyfour;
seventimespibyfour=7*pibyfour;
sinazimuth=sin(azimuth);
cosazimuth=cos(azimuth);
tanazimuth=tan(azimuth);
signsinazimuth=sign(sinazimuth);
signcosazimuth=sign(cosazimuth);
dssin=abs(1/sinazimuth);
dscos=abs(1/cosazimuth);
tanaltitudebyscale=tan(altitude)/scale;

% main loop
% max(max(a)) moved out from the loop to line 26
% while ((max(max(a))>=dz) && (dx<=sizex) && (dy<=sizey))
while ((amaxvalue>=dz) && (abs(dx)<=sizex) && (abs(dy)<=sizey))
	% Constant operations moved out to lines 27-30
    % if ((pi/4<=azimuth)&&(azimuth<3*pi/4))||((5*pi/4<=azimuth)&&(azimuth<7*pi/4))
	if ((pibyfour<=azimuth)&&(azimuth<threetimespibyfour))||((fivetimespibyfour<=azimuth)&&(azimuth<seventimespibyfour))
		% Unnecessary multiplication. sign(sin(azimuth)) moved out to lines 31 and 34.
        % dy=1*sign(sin(azimuth))*index;
		dy=signsinazimuth*index;
		% sign(cos(azimuth)) moved out to lines 32 and 35. tan(azimuth) moved out to line 33.
		% dx=-1*sign(cos(azimuth))*abs(round(index/tan(azimuth)));
        dx=-1*signcosazimuth*abs(round(index/tanazimuth));
		% abs(1/sin(azimuth)) moved out to lines 31 and 36
		% ds=abs(1/sin(azimuth));
        ds=dssin;
    else
		% Unnecessary multiplication. sign(sin(azimuth)) moved out to lines 31 and 34. tan(azimuth) moved out to line 33.
        % dy=1*sign(sin(azimuth))*abs(round(index*tan(azimuth)));
		dy=signsinazimuth*abs(round(index*tanazimuth));
		% sign(cos(azimuth)) moved out to lines 32 and 35
		% dx=-1*sign(cos(azimuth))*index;
        dx=-1*signcosazimuth*index;
		% abs(1/cos(azimuth)) moved out to lines 32 and 37
		% ds=abs(1/cos(azimuth));
        ds=dscos;
    end
    
% note: dx and dy represent absolute values while ds is an incremental value
	% tan(altitude)/scale moved out to line 38
    % dz=ds*index*tan(altitude)/scale;
	dz=ds*index*tanaltitudebyscale;
    temp(1:sizex,1:sizey)=0;
	
	absdx=abs(dx);
	absdy=abs(dy);
    
	% abs(dx) moved out to line 74
	% xc1=((dx+abs(dx))/2)+1;
    xc1=((dx+absdx)/2)+1;
	% abs(dx) moved out to line 74
	% xc2=(sizex+(dx-abs(dx))/2);
    xc2=(sizex+(dx-absdx)/2);
	% abs(dy) moved out to line 75
	% yc1=((dy+abs(dy))/2)+1;
    yc1=((dy+absdy)/2)+1;
	% abs(dy) moved out to line 75
	% yc2=(sizey+(dy-abs(dy))/2
    yc2=(sizey+(dy-absdy)/2);
    
	% abs(dx) moved out to line 74
	% xp1=-((dx-abs(dx))/2)+1;
    xp1=-((dx-absdx)/2)+1;
	% abs(dx) moved out to line 74
	% xp2=(sizex-(dx+abs(dx))/2);
    xp2=(sizex-(dx+absdx)/2);
	% abs(dy) moved out to line 75
	% yp1=-((dy-abs(dy))/2)+1;
    yp1=-((dy-absdy)/2)+1;
	% abs(dy) moved out to line 75
	% yp2=(sizey-(dy+abs(dy))/2);
    yp2=(sizey-(dy+absdy)/2);
    
    temp(xp1:xp2,yp1:yp2)= a(xc1:xc2,yc1:yc2)-dz;
    
    f=max(f,temp);
    index=index+1;
end

f=f-a;
f=not(not(f));
f=double(f);
