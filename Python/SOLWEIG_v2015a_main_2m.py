
# This is the main file for the SOLWEIG model, python version

# Input variables:
# dsm = digital surface model
# scale = height to pixel size (2m pixel gives scale = 0.5)
# header = ESRI Ascii Grid header
# sizey,sizex = no. of pixels in x and y
# row,col = point of interest. Used if all data from one should be calculated
# svf,svfN,svfW,svfE,svfS = SVFs for building and ground
# svfveg,svfNveg,svfEveg,svfSveg,svfWveg = Veg SVFs blocking sky
# svfaveg,svfEaveg,svfSaveg,svfWaveg,svfNaveg = Veg SVFs blocking buildings
# vegdem = Vegetation canopy DSM
# vegdem2 = Vegetation trunk zone DSM
# albedo_b = buildings
# albedo_g = ground (if landcover==0)
# absK = human absorption coefficient for shortwave radiation
# absL = human absorption coefficient for longwave radiation
# ewall = Emissivity of building walls
# eground = Emissivity of ground (if landcover==0)
# Fside = The angular factors between a person and the surrounding surfaces
# Fup = The angular factors between a person and the surrounding surfaces
# PA = Posture of a human
# met = meteorological inputdata
# YYYY = Year
# altitude = Sun altitude (degree)
# azimuth = Sun azimuth (degree)
# zen = Sun zenith angle (radians)
# jday = day of year
# showimage = show image during execuation
# usevegdem = use vegetation scheme
# onlyglobal = calculate dir and diff from global
# buildings = Boolena grid to identify building pixels
# location = geographic location
# height = height of measurments point
# trans Trensmissivity of shortwave theough vegetation
# output = output settings
# fileformat = fileformat of output grids
# landcover = use landcover scheme !!!NEW IN 2015a!!!
# sensorheight = Sensorheight of wind sensor
# leafon = foliated vegetation or not
# lc_grid = grid with landcoverclasses
# lc_class = table with landcover properties
# dectime = decimal time
# altmax = maximum sun altitude
# dirwalls = aspect of walls
# walls = one pixel row outside building footprints
# cyl = consider man as cyliner instead of cube
# elvis = old thing from Jonsson et al.

import numpy as np
from osgeo import gdal
import Solweig_v2015_metdata_noload as metload
import Solweig_2015a_calc as so
from Tgmaps_v1 import Tgmaps_v1
from clearnessindex_2013b import clearnessindex_2013b
from osgeo.gdalconst import *
import wallalgorithms as wa

import matplotlib.pylab as plt

infolder = 'C:/Users/xlinfr/Documents/PythonScripts/SOLWEIG/SOLWEIGdata_2m/'

showimage = 0
usevegdem = 0
onlyglobal = 0
height = 1.1
trans = 0.03
landcover = 0
sensorheight = 2.0
cyl = 0
elvis = 0

# load surface grids
#dsmload = 'C:/Users/xlinfr/Documents/NedladdadeFiler/Jasim/Builds_ground_DSM4_clip2.TIF'
dsmload = infolder + "krdsm_2m.tif"
dataSet = gdal.Open(dsmload)
dsm = dataSet.ReadAsArray().astype(np.float)
dsm[dsm < 0] = 0

# dsm = np.loadtxt(infolder + "DSM_KRbig.asc", skiprows=6)
dataSet = gdal.Open(infolder + "krdem_2m.tif")
dem = dataSet.ReadAsArray().astype(np.float)
dem[dem < 0] = 0

# vegdem = np.loadtxt(infolder + "CDSM_KRbig.asc", skiprows=6)
# vegdem2 = vegdem * 0.25

rows = dsm.shape[0]
cols = dsm.shape[1]
scale = 0.5
walllimit = 3
# self.iface.messageBar().pushMessage("SEBE", str(walllimit))
walls = wa.findwalls(dsm, walllimit)
dirwalls = wa.filter1Goodwin_as_aspect_v3(walls, scale, dsm)

# dataSet = gdal.Open(infolder + "a2.tif")
# dirwalls = dataSet.ReadAsArray().astype(np.float)
# dataSet = gdal.Open(infolder + "h2.tif")
# walls = dataSet.ReadAsArray().astype(np.float)

vegdem = np.zeros([rows, cols])
vegdem2 = np.zeros([rows, cols])
bush = np.zeros([rows, cols])



dataSet = gdal.Open(infolder + "/svfs/svf.tif")
svf = dataSet.ReadAsArray().astype(np.float)
dataSet = gdal.Open(infolder + "/svfs/svfN.tif")
svfN = dataSet.ReadAsArray().astype(np.float)
dataSet = gdal.Open(infolder + "/svfs/svfS.tif")
svfS = dataSet.ReadAsArray().astype(np.float)
dataSet = gdal.Open(infolder + "/svfs/svfE.tif")
svfE = dataSet.ReadAsArray().astype(np.float)
dataSet = gdal.Open(infolder + "/svfs/svfW.tif")
svfW = dataSet.ReadAsArray().astype(np.float)

if usevegdem == 1:
    dataSet = gdal.Open(infolder + "svfveg.tif")
    svfveg = dataSet.ReadAsArray().astype(np.float)
    dataSet = gdal.Open(infolder + "svfNveg.tif")
    svfNveg = dataSet.ReadAsArray().astype(np.float)
    dataSet = gdal.Open(infolder + "svfSveg.tif")
    svfSveg = dataSet.ReadAsArray().astype(np.float)
    dataSet = gdal.Open(infolder + "svfEveg.tif")
    svfEveg = dataSet.ReadAsArray().astype(np.float)
    dataSet = gdal.Open(infolder + "svfWveg.tif")
    svfWveg = dataSet.ReadAsArray().astype(np.float)

    dataSet = gdal.Open(infolder + "svfaveg.tif")
    svfaveg = dataSet.ReadAsArray().astype(np.float)
    dataSet = gdal.Open(infolder + "svfNaveg.tif")
    svfNaveg = dataSet.ReadAsArray().astype(np.float)
    dataSet = gdal.Open(infolder + "svfSaveg.tif")
    svfSaveg = dataSet.ReadAsArray().astype(np.float)
    dataSet = gdal.Open(infolder + "svfEaveg.tif")
    svfEaveg = dataSet.ReadAsArray().astype(np.float)
    dataSet = gdal.Open(infolder + "svfWaveg.tif")
    svfWaveg = dataSet.ReadAsArray().astype(np.float)
else:
    svfveg = np.ones((rows, cols))
    svfNveg = np.ones((rows, cols))
    svfSveg = np.ones((rows, cols))
    svfEveg = np.ones((rows, cols))
    svfWveg = np.ones((rows, cols))
    svfaveg = np.ones((rows, cols))
    svfNaveg = np.ones((rows, cols))
    svfSaveg = np.ones((rows, cols))
    svfEaveg = np.ones((rows, cols))
    svfWaveg = np.ones((rows, cols))

header = None
row = 30
col = 30

albedo_b = 0.20
albedo_g = 0.15

absK = 0.7
absL = 0.98
ewall = 0.95
eground = 0.95

PA = 'STAND'
if PA == 'STAND':
    Fside = 0.22
    Fup = 0.06
else:
    Fside = 0.166666
    Fup = 0.166667

UTC = 1
lon = 11.94
lat = 57.70
# alt = dsm.mean()
alt = 3.0

metfile = 0
if metfile == 1:
    met = np.loadtxt(infolder + "gbg1997060607_2015a.txt",skiprows=1, delimiter=' ')
    # met = np.loadtxt(infolder + "gbg19970606_2015a_nodirect.txt",skiprows=1, delimiter=' ')
else:
    met = np.zeros((1, 24)) - 999.

    year = 2011
    month = 6
    day = 6
    hour = 12
    minu = 30

    if (year % 4) == 0:
        if (year % 100) == 0:
            if (year % 400) == 0:
                leapyear = 1
            else:
                leapyear = 0
        else:
            leapyear = 1
    else:
        leapyear = 0

    if leapyear == 1:
        dayspermonth = [31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
    else:
        dayspermonth = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]

    doy = np.sum(dayspermonth[0:month - 1]) + day

    Ta = 25.
    RH = 50
    radG = 880.
    radD = 150.
    radI = 950.

    met[0, 0] = year
    met[0, 1] = doy
    met[0, 2] = hour
    met[0, 3] = minu
    met[0, 11] = Ta
    met[0, 10] = RH
    met[0, 14] = radG
    met[0, 21] = radD
    met[0, 22] = radI



location = {'longitude': lon, 'latitude': lat, 'altitude': alt}
YYYY, altitude, azimuth, zen, jday, leafon, dectime, altmax = metload.Solweig_2015a_metdata_noload(met, location, UTC)

if landcover == 1:
    dataSet = gdal.Open(infolder + "landcover.tif")
    lc_grid = dataSet.ReadAsArray().astype(np.float)
    sitein = "landcoverclasses_2016a.txt"
    f = open(sitein)
    lin = f.readlines()
    lc_class = np.zeros((lin.__len__() - 1, 6))
    for i in range(1, lin.__len__()):
        lines = lin[i].split()
        for j in np.arange(1, 7):
            lc_class[i - 1, j - 1] = float(lines[j])

    buildings = np.copy(lc_grid)
    buildings[buildings == 7] = 1
    buildings[buildings == 6] = 1
    buildings[buildings == 5] = 1
    buildings[buildings == 4] = 1
    buildings[buildings == 3] = 1
    buildings[buildings == 2] = 0
else:
    buildings = dsm - dem
    buildings[buildings < 2.] = 1.
    buildings[buildings >= 2.] = 0.

# # amaxvalue
# vegmax = vegdem.max()
# amaxvalue = dsm.max() - dsm.min()
# amaxvalue = np.maximum(amaxvalue, vegmax)
# # Elevation vegdems if buildingDEM includes ground heights
# vegdem = vegdem + dsm
# vegdem[vegdem == dsm] = 0
# vegdem2 = vegdem2 + dsm
# vegdem2[vegdem2 == dsm] = 0
# #% Bush separation
# bush = np.logical_not((vegdem2 * vegdem)) * vegdem

outputfolder = "c/temp/"

## From core but outside core loop
# if not row:
#     #% This is settings for calculating PET at POI
#     pet = []
#     pet.append({'body' : 75,'age':35,'height':1.80,'activity':80,'sex':1.,'clo':0.9})

#% fn=fopen([outputfolder 'Output_v2015a_POI_' PA '.txt'],'w');
    # fprintf(fn, '%9s', 'year', 'DOY', 'hour', 'dectime', 'altitude', 'azimuth', 'Kdirect', 'Kdiffuse', 'Kglobal', 'Kdown', 'Kup', 'KsideI', 'Knorth', 'Keast', 'Ksouth', 'Kwest', 'Ldown', 'Lup', 'Lnorth', 'Least', 'Lsouth', 'Lwest', 'Ta', 'Tg', 'RH', 'Ea', 'Esky', 'Sstr', 'Tmrt', 'I0', 'CI', 'gvf', 'CITg', 'Shadow', 'SVF_b', 'SVF_b+v', 'PET', 'UTCI')
    # fprintf(fn, '\r\n')

#%Surface to air temperature difference at sunrise
#% Tstart=0;%3.41; % dynamic as from 2015a
#%Initialization of maps
Knight = np.zeros((rows, cols))
Tmrtday = np.zeros((rows, cols))
Lupday = np.zeros((rows, cols))
Ldownday = np.zeros((rows, cols))
Kupday = np.zeros((rows, cols))
Kdownday = np.zeros((rows, cols))
gvfday = np.zeros((rows, cols))
Tmrtdiurn = np.zeros((rows, cols))
Lupdiurn = np.zeros((rows, cols))
Ldowndiurn = np.zeros((rows, cols))
Tgmap1 = np.zeros((rows, cols))
Tgmap1E = np.zeros((rows, cols))
Tgmap1S = np.zeros((rows, cols))
Tgmap1W = np.zeros((rows, cols))
Tgmap1N = np.zeros((rows, cols))

tmp = svf+svfveg-1.
tmp[tmp < 0.] = 0.
#%matlab crazyness around 0
svfalfa = np.arcsin(np.exp((np.log((1.-tmp))/2.)))

#%Creating vectors from meteorological input
DOY = met[:, 1]
hours = met[:, 2]
minu = met[:,3]
Ta = met[:, 11]
RH = met[:, 10]
radG = met[:, 14]
radD = met[:, 21]
radI = met[:, 22]
P = met[:, 12]
Ws = met[:, 9]
#%Wd=met(:,13);
Twater = []

# used to calculate tmrtday etc.
# daytime = nonzero((altitude > 0.))
# daytime = length(daytime)

#%Number of daytime hours
# Initialisation of time related variables
if Ta.__len__() == 1:
    timestepdec = 0.0
else:
    timestepdec = dectime[1] - dectime[0]
timeadd = 0.
timeaddE = 0.
timeaddS = 0.
timeaddW = 0.
timeaddN = 0.
firstdaytime = 1.
#%bugfix so that model can start during daytime
#%Parameterisarion for Lup
if not height:
    height = 1.1

#%Radiative surface influence, Rule of thumb by Schmid et al. (1990).
first = np.round(height)

if first == 0.:
    first = 1.

second = np.round((height*20.))

if usevegdem == 1:
    # % Vegetation transmittivity of shortwave radiation
    psi = leafon * trans
    psi[leafon == 0] = 0.5
    # amaxvalue
    vegmax = vegdem.max()
    amaxvalue = dsm.max() - dsm.min()
    amaxvalue = np.maximum(amaxvalue, vegmax)

    # Elevation vegdsms if buildingDEM includes ground heights
    vegdem = vegdem + dsm
    vegdem[vegdem == dsm] = 0
    vegdem2 = vegdem2 + dsm
    vegdem2[vegdem2 == dsm] = 0

    # % Bush separation
    bush = np.logical_not((vegdem2 * vegdem)) * vegdem

    svfbuveg = (svf - (1. - svfveg) * (1. - trans))  # % major bug fixed 20141203
else:
    psi = leafon * 0. + 1.
    svfbuveg = svf
    amaxvalue = dsm.max() - dsm.min()




#% If Buildings is empty, building walls will be created using an edge function.
# if not buildings:
#     buildings = findbuildingedges(dsm)
#     buildings = np.dot(buildings, -1.)+1.
#     buildings = np.double(buildings)

#%Surface to air temperature difference at sunrise
#% Tstart=3.41; % dynamic as from 2015a
#% Ts parameterisation maps
if landcover == 1.:
    [TgK, Tstart, alb_grid, emis_grid, TgK_wall, Tstart_wall, TmaxLST, TmaxLST_wall] = Tgmaps_v1(lc_grid, lc_class)
else:
    TgK = Knight+0.37
    Tstart = Knight-3.41
    alb_grid = Knight+albedo_g
    emis_grid = Knight+eground
    TgK_wall = 0.37
    Tstart_wall = -3.41
    TmaxLST = 15.
    TmaxLST_wall = 15.
    lc_grid = None
    lc_class = None

# gvfLup = np.zeros([rows, cols])

# If metfile starts at night
CI = 1.

poitmrt = np.zeros((Ta.__len__()))
i = 0
# pl = plt.imshow(Knight)
# test = '/Tmrt_' + str(int(YYYY[0, i])) + '_' + str(int(DOY[i])) + '_' + str(int(hour[i])) + str(int(minu[i])) + '.tif'
# Loop through time series
for i in np.arange(0, Ta.__len__()):
    print i
    # Daily water body temperature
    if landcover == 1:
        if ((dectime[i] - np.floor(dectime[i]))) == 0 or (i == 0):
            Twater = np.mean(Ta[jday[0] == np.floor(dectime[i])])
    else:
        Twater = []

    # Nocturnal cloudfraction from Offerle et al. 2003
    if (dectime[i] - np.floor(dectime[i])) == 0:
        # alt = altitude[i:altitude.__len__()]
        daylines = np.where(np.floor(dectime) == dectime[i])
        alt = altitude[0][daylines]
        alt2 = np.where(alt > 1)
        rise = alt2[0][0]
        [_, CI, _, _, _] = clearnessindex_2013b(zen[0, i + rise + 1], jday[0, i + rise + 1], Ta[i + rise + 1],
                                                RH[i + rise + 1] / 100., radG[i + rise + 1], location, P[i + rise + 1])  # i+rise+1 to match matlab code. correct?
        if (CI > 1) or (CI == np.inf):
            CI = 1

    # Main calcualtions
    Tmrt, Kdown, Kup, Ldown, Lup, Tg, ea, esky, I0, CI, shadow, firstdaytime, timestepdec, timeadd, Tgmap1, timeaddE, \
    Tgmap1E, timeaddS, Tgmap1S, timeaddW, Tgmap1W, timeaddN, Tgmap1N = so.Solweig_2015a_calc(i, dsm, scale, rows,
                        cols, svf, svfN, svfW, svfE, svfS, svfveg, svfNveg, svfEveg, svfSveg, svfWveg, svfaveg,
                        svfEaveg, svfSaveg, svfWaveg, svfNaveg, vegdem, vegdem2, albedo_b,
                        absK, absL, ewall, Fside, Fup, altitude[0][i], azimuth[0][i], zen[0][i], jday[0][i],
                        usevegdem, onlyglobal, buildings, location, psi[0][i], landcover, lc_grid, dectime[i],
                        altmax[0][i], dirwalls, walls, cyl, elvis, Ta[i], RH[i], radG[i], radD[i], radI[i], P[i],
                        amaxvalue, bush, Twater, TgK, Tstart, alb_grid, emis_grid, TgK_wall, Tstart_wall, TmaxLST,
                        TmaxLST_wall, first, second, svfalfa, svfbuveg, firstdaytime, timeadd, timeaddE, timeaddS,
                        timeaddW, timeaddN, timestepdec, Tgmap1, Tgmap1E, Tgmap1S, Tgmap1W, Tgmap1N, CI)

    filename = 'TmrtoutsideUMEP_' + str(int(YYYY[0, i])) + '_' + str(int(DOY[i])) + '_' + str(int(hours[i])) + str(int(minu[i])) + '.tif'
    print(azimuth[0][i])
    print(altitude[0][i])
    print(hours[i])

    # plt.matshow(Tmrt)
    # plt.colorbar()
    # plt.show()
    # poitmrt[i] = Tmrt[51, 118]
    k=4

    # plt.plot(poitmrt)
    # plt.show()

    # def saveraster(self, gdal_data, filename, raster):
    rows = dataSet.RasterYSize
    cols = dataSet.RasterXSize

    outDs = gdal.GetDriverByName("GTiff").Create(filename, cols, rows, int(1), GDT_Float32)
    outBand = outDs.GetRasterBand(1)

    # write the data
    outBand.WriteArray(Tmrt, 0, 0)
    # flush data to disk, set the NoData value and calculate stats
    outBand.FlushCache()
    outBand.SetNoDataValue(-9999)

    # georeference the image and set the projection
    outDs.SetGeoTransform(dataSet.GetGeoTransform())
    outDs.SetProjection(dataSet.GetProjection())