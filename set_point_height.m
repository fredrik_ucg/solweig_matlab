function [a,height]=set_point_height(a,row,col,height)
% This function specifies the height above ground at the Point of Interest

a(row,col)=a(row,col)+height;
