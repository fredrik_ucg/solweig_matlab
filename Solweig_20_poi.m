function [x,y] = Solweig_20_poi(figurehandler,a,sizex,sizey,vegdem)
% This function is used to specify a Point of Interest within the model
% domain

x = [];
y = [];

% Referencing the figure
S.fh = figurehandler;
set(S.fh, 'resize', 'off');
position = get(S.fh, 'Position');

% Drawing the DEM inside the figure
if isempty(vegdem)
    imagesc(a(1:sizey,1:sizex)), axis image, hold on
else
    imagesc(a(1:sizey,1:sizex)+vegdem(1:sizey,1:sizex)), axis image, hold on
end

% Positioning the DEM
S.ax = gca;
set(S.ax,'units','pixels');
gca_pos = get(S.ax,'Position');

if (sizey >= sizex)
    % Portrait or square image => scaling accordingly
    aspect_radio = sizey / sizex;
    img_width = floor(gca_pos(4) / aspect_radio);
    img_height = floor(gca_pos(4));
    img_xpos = floor((position(3) - img_width) / 2);
    img_ypos = floor(gca_pos(2));
else
    % Panoramic image => scaling accordingly
    aspect_radio = sizex / sizey;
    img_width = floor(gca_pos(3));
    img_height = floor(gca_pos(3) / aspect_radio);
    img_xpos = floor(gca_pos(1));
    img_ypos = floor((position(4) - img_height) / 2);
end

set(S.ax,'position',[img_xpos img_ypos img_width img_height]);

% Filling the structure with the axes data
S.XLM = get(S.ax,'xlim');
S.YLM = get(S.ax,'ylim');
S.AXP = get(S.ax,'position');
S.DFX = diff(S.XLM);
S.DFY = diff(S.YLM);

% Creating the textbox that will display the current position of the mouse
tex_xpos = 0;
tex_ypos = img_ypos + img_height + 2;
tex_width = position(3);
tex_height = 20;

% This textbox will act as a label
message = 'Point of Interest:';
S.tx = uicontrol('style','text',...
    'units','pixels',...
    'position',[tex_xpos tex_ypos tex_width tex_height],...
    'BackgroundColor',get(S.fh,'color'),...
    'fontsize',12,'fontweight','bold',...
    'string',[message, '  row:none  col:none'],...
    'FontName', 'FixedWidth',...
    'horizontalalignment', 'center');

% Drawing the crosshairs
linX = line([0 0], [0 sizey],...
    'color', 'green',...
    'linewidth', 2);
linY = line([0 sizex], [0 0],...
    'color', 'green',...
    'linewidth', 2);

% Setting the callbacks functions to the figure
set(S.fh,'windowbuttonmotionfcn',{@fh_wbmfcn,S}) % Set the motion detector.
set(S.fh,'windowbuttondownfcn',{@fh_wbdfcn,S}) % Set the button press detector.

% Setting the wait condition for this function: it will not exit until a valid
% point of interest has been selected
set(S.fh, 'Clipping', 'off');
waitfor(S.fh, 'Clipping', 'on');

% WindowButtonMotionFcn callback function
    function [] = fh_wbmfcn(varargin)
        % WindowButtonMotionFcn for the figure.
        S = varargin{3};  % Getting the structure.
        F = get(S.fh,'currentpoint');  % The current point w.r.t the figure.
        % Figure out of the current point is over the axes or not -> logicals.
        s_axp1 = uint32(S.AXP(1));
        s_axp2 = uint32(S.AXP(2));
        s_axp13 = uint32(S.AXP(1) + S.AXP(3));
        s_axp24 = uint32(S.AXP(2) + S.AXP(4));
        f1 = uint32(F(1));
        f2 = uint32(F(2));
        tf1 = s_axp1 <= f1 && f1 <= s_axp13;
        tf2 = s_axp2 <= f2 && f2 <= s_axp24;
        
        if tf1 && tf2
            % Calculate the current point w.r.t. the axes.
            Cx =  S.XLM(1) + (F(1)-S.AXP(1)).*(S.DFX/S.AXP(3));
            Cy =  S.YLM(1) + (F(2)-S.AXP(2)).*(S.DFY/S.AXP(4));
            Cy = sizey - floor(Cy);
            set(S.tx,'string',num2str([floor(Cy),floor(Cx)],[message, '  row:%4u  col:%4u']))
            % Updating the crosshairs
            set(linX, 'XData', Cx*[1 1]);
            set(linY, 'YData', Cy*[1 1]);
        end
    end

% WindowButtonDownFcn callback function
    function [] = fh_wbdfcn(varargin)
        % WindowButtonDownFcn for the figure.
        S = varargin{3};  % Getting the structure.
        if strcmp(get(S.fh,'SelectionType'),'normal')
            F = get(S.fh,'currentpoint');  % The current point w.r.t the figure.
            % Figure out of the current point is over the axes or not -> logicals.
            s_axp1 = uint32(S.AXP(1));
            s_axp2 = uint32(S.AXP(2));
            s_axp13 = uint32(S.AXP(1) + S.AXP(3));
            s_axp24 = uint32(S.AXP(2) + S.AXP(4));
            f1 = uint32(F(1));
            f2 = uint32(F(2));
            tf1 = s_axp1 <= f1 && f1 <= s_axp13;
            tf2 = s_axp2 <= f2 && f2 <= s_axp24;
            
            if tf1 && tf2
                % Calculate the current point w.r.t. the axes.
                Cx =  S.XLM(1) + (F(1)-S.AXP(1)).*(S.DFX/S.AXP(3));
                Cy =  S.YLM(1) + (F(2)-S.AXP(2)).*(S.DFY/S.AXP(4));
                Cy = sizey - floor(Cy);
                % Assigning the coordinates to the outputs and exiting
                y = floor(Cx);
                x = floor(Cy);
                plot(y,x,'r +');
                hold off
                set(S.fh, 'Clipping', 'on');
                set(S.fh,'windowbuttonmotionfcn','') % Setting the default motion detector.
                set(S.fh,'windowbuttondownfcn','') % Setting the default button press detector.
            end
        end
    end
end
