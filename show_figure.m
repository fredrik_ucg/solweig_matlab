function [figurehandler]=show_figure(position,menubar,name,toolbar)
figurehandler=figure('Position',position,'Toolbar',toolbar,'menubar',menubar,'Name',name,'Numbertitle','off');
set(figurehandler,'CloseRequestFcn',@closeBar);

function closeBar(src,evnt)
