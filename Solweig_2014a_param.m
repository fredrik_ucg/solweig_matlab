function [albedo_b,albedo_g,absK,absL,ewall,eground,Fside,Fup,PA,trans]=Solweig_2014a_param(param)
% This function is used to set all input parameters such as albedo etc...

if strcmpi(param,'n')
    albedo_b=input('albedo:(standard value=0.20)  ');                %Average albedo buildings
    albedo_g=input('albedo:(standard value=0.15)  ');                %Average albedo ground (paved)
    %Absorption coefficient of short and long wave radiation of a person
    absK=input('absorptioncoefficient for shortwave radiation: (standardvalue=0.7)    ');
    absL=input('absorptioncoefficient for longwave radiation: (standardvalue=0.97)    ');
    ewall=input('Emissivity of building walls (standard value=0.9)   ');      %Emissivities
    eground=input('Emissivity of ground (standard value=0.95)   ');   %Emissivities
    %The angular factors between a person and the surrounding surfaces
    paste=input('standing or sitting person? (Press 1 for standing OR 2 for sitting)   ');
    if paste==1,Fside=0.22;Fup=0.06;PA='STAND'; end
    if paste==2,Fside=0.166;Fup=Fside;PA='SIT'; end
    trans=input('Transmissivity of K trough vegetation (standard value=0.05)   ');   %Transmissivity
%     a(x,y)=a(x,y)+input('Set height above ground for point of interest (standard value=1.1)   ');   %Emissivities
else
    albedo_b=0.20;                       %Average albedo buildings
    albedo_g=0.15;                       %Average albedo ground (paved)
    absK=0.7;absL=0.97;                %Absorption coefficient of short and long wave radiation of a person
    Fside=0.22;Fup=0.06;PA='STAND';    %The angular factors between a person and the surrounding surfaces
    ewall=0.95;eground=0.95;            %Emissivities
    trans=0.05;                         %Transmissivity of K trough vegetation
%     a(x,y)=a(x,y)+1.1;                 %Instrument position and height
end

