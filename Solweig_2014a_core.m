function [modva]=Solweig_2014a_core(outputfolder,a,scale,header,sizey,sizex,x,y,svf,svfN,svfW,svfE,svfS,svfveg,svfNveg,svfEveg,svfSveg,svfWveg,svfaveg,svfEaveg,svfSaveg,svfWaveg,svfNaveg,vegdem,vegdem2,albedo_b,albedo_g,absK,absL,ewall,eground,Fside,Fup,PA,met,YYYY,altitude,azimuth,zen,jday,showimage,usevegdem,onlyglobal,buildings,location,height,trans,output,fileformat,landuse,sensorheight,leafon)
% This is the core function of the SOLWEIG model
% 2014-05-13
% Fredrik Lindberg, fredrikl@gvc.gu.se
% G�teborg Urban Climate Group
% Gothenburg University
%
% Input variables:
% a = digital surface model
% scale = height to pixel size (2m pixel gives scale = 0.5)
% header = ESRI Ascii Grid header
% sizey,sizex = no. of pixels in x and y
% x,y = point of interest
% svf,svfN,svfW,svfE,svfS = SVFs for building and ground
% svfveg,svfNveg,svfEveg,svfSveg,svfWveg = Veg SVFs blocking sky
% svfaveg,svfEaveg,svfSaveg,svfWaveg,svfNaveg = Veg SVFs blocking buildings
% vegdem = Vegetation canopy DSM
% vegdem2 = Vegetation trunk zone DSM
% albedo_b = buildings
% albedo_g = ground
% absK = human absorption coefficient for shortwave radiation
% absL = human absorption coefficient for longwave radiation
% ewall = Emissivity of building walls
% eground = Emissivity of ground
% Fside = The angular factors between a person and the surrounding surfaces
% Fup = The angular factors between a person and the surrounding surfaces
% PA = Posture of a human
% met = meteorological inputdata
% YYYY = Year
% altitude = Sun altitude (degree)
% azimuth = Sun azimuth (degree)
% zen = SUn zenith angle (radians)
% jday = day of year
% showimage = show image during execuation
% usevegdem = use vegetation scheme
% onlyglobal = calculate dir and diff from global
% buildings = Boolena grid to identify building pixels
% location = geographic location
% height = height of measurments point
% trans Trensmissivity of shortwave theough vegetation
% output = output settings
% fileformat = fileformat of output grids
% landuse = landuse not used yet
% sensorheight = Sensorheight of wind sensor
% leafon = foliated vegetation or not

%% Core program start
%Initialization output textfile
if isempty(x)
else
    % This is settings for calulating PET at POI
    pet.mbody=75;
    pet.age=35;
    pet.height=1.80;
    pet.activity=80;
    pet.sex=1;
    pet.clo=0.9;
    if(exist(outputfolder,'dir')==0)
        mkdir(outputfolder);
    end
    fn=fopen([outputfolder 'Output_v2014a_POI_' PA '.txt'],'w');
    fprintf(fn,'%9s','year','DOY','hour','dectime','altitude','azimuth','Kdirect','Kdiffuse','Kglobal','Kdown','Kup',...
        'Knorth','Keast','Ksouth','Kwest','Ldown','Lup','Lnorth','Least','Lsouth','Lwest','Ta','Tg',...
        'RH','Ea','Esky','Sstr','Tmrt','I0','CI','gvf','CITg','Shadow','SVF_b','SVF_b+v','PET','UTCI');
    fprintf(fn,'\r\n');
end

%Imagescaling and plotting vectors
clim=[-10 70];
modva=[];

%Instrument offset in degrees
t=0;

%Stefan Bolzmans Constant
SBC=5.67051e-8;

%Surface to air temperature difference at sunrise
Tstart=0;%3.41;

%Initialization of maps
Knight=zeros(sizey,sizex);
Tmrtday=zeros(sizey,sizex);Lupday=Tmrtday;Ldownday=Tmrtday;Kupday=Tmrtday;
Kdownday=Tmrtday;gvfday=Tmrtday;
Tmrtdiurn=Tmrtday;Lupdiurn=Tmrtday;Ldowndiurn=Tmrtday;
% Tgmapgvf=Knight;

tmp=(svf+svfveg-1);tmp(tmp<0)=0; %matlab crazyness around 0
svfalfa=asin(exp((log(1-(tmp)))/2));clear tmp

if showimage == 1
    screensize=get(0,'ScreenSize');
    screenwidth=screensize(3);
    screenheight=screensize(4);
    figure('Position',[(screenwidth-0.85*screenwidth) (screenheight-0.85*screenheight) (screenheight*0.7) (screenheight*0.6)],'color',[1 1 1],'Name',' ','Numbertitle','off','Toolbar','none','menubar','none');
end

%Creating vectors from meteorological input (2013b)
% year=time(:,1);month=time(:,2);day=time(:,3);%min=met(:,5);
DOY=met(:,1);hour=met(:,2);
dectime=met(:,3);Ta=met(:,11);RH=met(:,10);
radG=met(:,14);radD=met(:,21);radI=met(:,22);P=met(:,12);
Ws=met(:,9);%Wd=met(:,13);
daytime=find(altitude>0);daytime=length(daytime); %Number of daytime hours
timestepdec=dectime(2)-dectime(1);
timeadd=0;firstdaytime=0;

%Parameterisarion for Lup
if isempty(height),height=1.1;else end
first=round(height); %Radiative surface influence, Rule of thumb by Schmid et al. (1990).
if first==0, first=1; end
second=round(height*20);
azimuthA=0:20:359; %Search directions for Ground View Factors (GVF)

if usevegdem==1
    psi=leafon*trans;
    psi(leafon==0)=0.5;
%     psi=trans;% Vegetation transmittivity of shortwave radiation
    
    % amaxvalue (used in calculation of vegetation shadows)
    vegmax=max(vegdem(:));
    amaxvalue=range(a(:));
    amaxvalue=max(amaxvalue,vegmax);
    
    % Elevation vegdems if buildingDSM includes ground heights
    vegdem=vegdem+a;vegdem(vegdem==a)=0;
    vegdem2=vegdem2+a;vegdem2(vegdem2==a)=0;
    
    % Bush separation
    bush=not(vegdem2.*vegdem).*vegdem;
else
%     psi=1;
    psi=leafon*0+1;
end

svfbuveg=(svf-(1-svfveg)*(trans));

% If Buildings is empty, building walls will be created using an edge function.
if isempty(buildings)
    buildings=findbuildingedges(a);
    buildings=buildings*(-1)+1;
    buildings=double(buildings);
end

% Main loop
for i=1:length(altitude)
    
    %Show the progressbar if the images are not shown
    if showimage == 0
%         progressbar(i/length(altitude),0);
    end
    
    % Find sunrise decimal hour - new in 2014a
    [ ~, ~, ~, SNUP ] = daylen( jday(i), location.latitude );
    
    % If metfile starts at night
    CI=1;
    
    %Vapor pressure
    ea=6.107*10^((7.5*Ta(i))/(237.3+Ta(i)))*(RH(i)/100);
    
    %Determination of clear-sky emissivity from Prata (1996)
    msteg=46.5*(ea/(Ta(i)+273.15));
    esky=(1-(1+msteg)*exp(-((1.2+3.0*msteg)^0.5)));%-0.04; old error fron jonsson et al. 2006
    
    if altitude(i)>0 %%%%%% DAYTIME %%%%%%
        
        %Clearness Index on Earth's surface after Crawford and Dunchon (1999) with a correction
        %factor for low sun elevations after Lindberg et al. (2008)
        [I0, CI, Kt,~,~]=clearnessindex_2013b(zen(1,i),jday(1,i),Ta(i),RH(i)/100,radG(i),location,P(i));
        if CI>1 && CI<inf,CI=1;end
        
        %Estimation of radD and radI if not measured after Reindl et al. (1990)
        if onlyglobal == 1
            [radI(i), radD(i)]=diffusefraction(radG(i),altitude(i),Kt,Ta(i),RH(i));
        end
        
        %Shadow images
        if usevegdem==1
            [vegsh,sh]=shadowingfunction_20(a,vegdem,vegdem2,azimuth(1,i),altitude(1,i),scale,amaxvalue,bush);
            shadow=(sh-(1-vegsh)*(1-psi(i)));
        else
            sh=shadowingfunctionglobalradiation(a,azimuth(1,i),altitude(1,i),scale);
            vegsh=ones(sizey,sizex);
            shadow=sh;
        end
        
        %Ground View Factors based on shadowpatterns and sunlit walls
        gvf=zeros(sizey,sizex);
        sunwall=wallinsun_veg(a,buildings,azimuth(i),sh,vegsh);
        
        for j=1:length(azimuthA)
            if usevegdem==1
                [sos]=sunonsurface_veg(azimuthA(j),scale,buildings,sh,first,second,sunwall,vegsh,psi(i));
            else
                [sos]=sunonsurface(azimuthA(j),scale,buildings,sh,first,second,sunwall);
            end
            gvf=gvf+sos;
        end
        gvf=gvf/length(azimuthA)+(buildings*-1+1);
        
        %Building height angle from svf
        F_sh=cylindric_wedge(zen(i),svfalfa);%Fraction shadow on building walls based on sun altitude and svf
        F_sh(isnan(F_sh))=0.5;
        
        %%%%%%% Calculation of shortwave daytime radiative fluxes %%%%%%%
        Kdown=radI(i)*shadow*sin(altitude(i)*(pi/180))+radD(i)*svfbuveg+...
            radG(i)*albedo_b*(1-svfbuveg).*(1-F_sh);%*sin(altitude(i)*(pi/180));
        
        Kup=albedo_g*(radI(i)*gvf*sin(altitude(i)*(pi/180))+radD(i)*svfbuveg+...
            radG(i)*albedo_b*(1-svfbuveg).*(1-F_sh));
        
        [Keast,Ksouth,Kwest,Knorth]=Kside_veg_v24(radI(i),radG(i),radD(i),shadow,svfS,svfW,svfN,svfE,...
            svfEveg,svfSveg,svfWveg,svfNveg,azimuth(i),altitude(i),psi(i),t,albedo_b,F_sh);
        
        %%%% Surface temperature parameterisation during daytime %%%%
        dfm=abs(172-jday(i)); %Day from midsommer
        Tgamp=0.000006*dfm^3-0.0017*dfm^2+0.0127*dfm+17.084+Tstart; %sinus function for daily surface temperature wave
        %         Tg=Tgamp*sin((((hour(i))-rise)/(15-rise))*pi/2)-Tstart;
        Tg=Tgamp*sin((((dectime(i)-floor(dectime(i)))-SNUP/24)/(15/24-SNUP/24))*pi/2)-Tstart; %new sunrise time 2014a
        
        if Tg<0 % temporary for removing low Tg during morning 20140513, from SOLWEIG1D
            Tg=0;
        end
        
        %New estimation of Tg reduction for non-clear situation based on Reindl et al. 1990
        [radI0]=diffusefraction(I0,altitude(i),1,Ta(i),RH(i));
        corr=0.1473*log(90-(zen(i)/pi*180))+0.3454;% 20070329 temporary correction of latitude from Lindberg et al. 2008
        CI_Tg=(radI(i)/radI0)+(1-corr);
        if CI_Tg>1 && CI_Tg<inf ,CI_Tg=1;end
        Tg=Tg*CI_Tg; %new estimation
        Tw=Tg;
        
        %%%% Lup, daytime %%%%
        %Surface temperature wave delay - new as from 2014a
        Tgmap0=gvf*Tg+Ta(i); % current timestep
        if firstdaytime==1 %"first in morning"
            Tgmap1=Tgmap0;
        end
       if timeadd>=(59/1440) %more or equal to 59 min
            weight1=exp(-33.27*timeadd); %surface temperature delay function - 1 step
            Tgmap1=Tgmap0*(1-weight1)+Tgmap1*weight1;
            Lup=SBC*eground*((Tgmap1+273.15).^4);
            if timestepdec>(59/1440)
                timeadd=timestepdec;
            else
                timeadd=0;
            end
        else
            timeadd=timeadd+timestepdec;
            weight1=exp(-33.27*timeadd); %surface temperature delay function - 1 step
            Lup=SBC*eground*((Tgmap0*(1-weight1)+Tgmap1*weight1+273.15).^4);
        end
        
%                     subplot(2,3,1),imagesc(gvf,[0 1]),axis image,colorbar,title(['timeadd= ' num2str(timeadd*1440)],'FontSize',10)
%                     subplot(2,3,2),imagesc(Tgmap0),axis image,colorbar,title(['Tgmap0, Tg= ' num2str(Tg)],'FontSize',10)
%                     subplot(2,3,3),imagesc(Tgmap1),axis image,colorbar,title('Tgmap1','FontSize',10)
%                     subplot(2,3,4),imagesc((Tgmap0*(1-weight1)+Tgmap1*weight1)),axis image,colorbar
%                     subplot(2,3,5),imagesc(Lup),axis image,colorbar,title(['weight1= ' num2str(weight1)],'FontSize',10)
%                     date=datevec(datenum([YYYY(i) 0 0 0 0 0])+DOY(i));
%                     dec=dectime(i)-floor(dectime(i));
%                     date(1,4)=floor(dec*24);
%                     date(1,5)=round((dec-date(1,4)/24)*1440);
%                     title(['Time=' datestr(date,'yyyy/mm/dd HH:MM')],'FontSize',10)
%                     pause(0.2)
        %         end
        firstdaytime=0;
        
    else %%%%%%% NIGHTTIME %%%%%%%%
        
        %Nocturnal cloudfraction from Offerle et al. 2003
        if (dectime(i)-floor(dectime(i)))==0
            alt=altitude(1,i:size(altitude,2));
            alt=find(alt>1);
            rise=alt(1);
            [~, CI , ~, ~, ~]=clearnessindex_2013b(zen(1,i+rise),jday(1,i+rise),Ta(i+rise),RH(i+rise)/100,radG(i+rise),location,P(i+rise));
            if CI>1 && CI<inf,CI=1;end
        end
        
        Tw=0;Tg=0;CI_Tg=inf;F_sh=[];
        
        %Nocturnal Kfluxes set to 0
        Kdown=Knight;Kwest=Knight;Kup=Knight;Keast=Knight;Ksouth=Knight;Knorth=Knight;shadow=Knight;
        
        %%%% Lup %%%%
        Lup=SBC*eground*((Knight+Ta(i)+Tg+273.15).^4);
        
        I0=0;gvf=zeros(sizey,sizex);timeadd=0;firstdaytime=1;
    end
    
    %%%% Ldown %%%%
    Ldown=(svf+svfveg-1)*esky*SBC*((Ta(i)+273.15)^4)+(2-svfveg-svfaveg)*ewall*SBC*((Ta(i)+273.15)^4)+...
        (svfaveg-svf)*ewall*SBC*((Ta(i)+273.15+Tw)^4)+(2-svf-svfveg)*(1-ewall)*esky*SBC*((Ta(i)+273.15)^4); %Jonsson et al. (2006)
    %Ldown=Ldown-25;% Shown by Jonsson et al. (2006) and Duarte et al. (2006)
    
    if CI < 0.95  %non-clear conditions
        c=1-CI;
        Ldown=Ldown*(1-c)+c*SBC*((Ta(i)+273.15)^4);
    end
    
    %%%% Lside %%%%
    [Least,Lsouth,Lwest,Lnorth]=Lside_veg_v2(svfS,svfW,svfN,svfE,svfEveg,svfSveg,svfWveg,svfNveg,...
        svfEaveg,svfSaveg,svfWaveg,svfNaveg,azimuth(i),altitude(i),Ta(i),Tw,SBC,ewall,Ldown,Lup,esky,t,F_sh);
    
    %%%% Calculation of radiant flux density and Tmrt %%%%
    Sstr=absK*(Kdown*Fup+Kup*Fup+Knorth*Fside+Keast*Fside+Ksouth*Fside+Kwest*Fside)...
        +absL*(Ldown*Fup+Lup*Fup+Lnorth*Fside+Least*Fside+Lsouth*Fside+Lwest*Fside);
    Tmrt=sqrt(sqrt((Sstr/(absL*SBC))))-273.2;
    
    %%%% Save hourly images as ESRIGRID and/or TIF %%%%
    if altitude(i)>0
        Tmrtday=Tmrtday+Tmrt;Lupday=Lupday+Lup;Ldownday=Ldownday+Ldown;
        Kupday=Kupday+Kup;Kdownday=Kdownday+Kdown;gvfday=gvfday+gvf;
    end
    Tmrtdiurn=Tmrtdiurn+Tmrt;Lupdiurn=Lupdiurn+Lup;Ldowndiurn=Ldowndiurn+Ldown;
    Solweig_outputfiles_hour_2014a(output,fileformat,Tmrt,Kup,Kdown,Lup,Ldown,gvf,outputfolder,YYYY(i),DOY(i),hour(i),PA,header,sizey,dectime(i));
    
    %%%% Saving to textfile for Point Of Interest %%%%
    if isempty(x)
    else
        
        %Recalculating wind speed based on powerlaw
        WsPET=(1.1/sensorheight)^0.14*Ws(i);        
        WsUTCI=(10/sensorheight)^0.14*Ws(i);
        petout=petcalculator3(Ta(i),RH(i),Tmrt(x,y),WsPET,pet);
        UTCI_approx=utci_calculator_solweigpoint(Ta(i),RH(i),Tmrt(x,y),WsUTCI);
        
        fprintf(fn,'%9.3f',year(i),DOY(i),hour(i),dectime(i),altitude(i),azimuth(i),radI(i),radD(i)...
            ,radG(i), Kdown(x,y), Kup(x,y), Knorth(x,y), Keast(x,y), Ksouth(x,y),Kwest(x,y),...
            Ldown(x,y), Lup(x,y), Lnorth(x,y), Least(x,y), Lsouth(x,y), Lwest(x,y),Ta(i),Ta(i)+Tg,RH(i),...
            ea,esky,Sstr(x,y),Tmrt(x,y),I0,CI,gvf(x,y),CI_Tg,shadow(x,y),svf(x,y),svfbuveg(x,y),petout,UTCI_approx);
        fprintf(fn,'\r\n');
        modva(i,:)=[year(i) DOY(i) hour(i) dectime(i) altitude(i) azimuth(i) radI(i) radD(i) ...
            radG(i) Kdown(x,y) Kup(x,y) Knorth(x,y) Keast(x,y) Ksouth(x,y) Kwest(x,y) ...
            Ldown(x,y) Lup(x,y) Lnorth(x,y) Least(x,y) Lsouth(x,y) Lwest(x,y) Ta(i) ...
            RH(i) ea esky Sstr(x,y) Tmrt(x,y) I0 CI Ta(i)+Tg CI_Tg shadow(x,y)];
    end
    
    %%%% Image display during execution%%%%
    if showimage == 1
        imagesc(Tmrt,clim),axis image; colorbar, colormap(jet)
        date=datevec(datenum([YYYY(i) 0 0 0 0 0])+DOY(i));
        dec=dectime(i)-floor(dectime(i));
        date(1,4)=floor(dec*24);
        date(1,5)=round((dec-date(1,4)/24)*1440);
        title(['T_{mrt} (�C) on ' datestr(date,'yyyy/mm/dd HH:MM')],'FontSize',14)
        hold on
        plot(y,x,'r *');
        pause(0.1);
    end
end

%Closing the progressbar
if showimage == 0
    progressbar(1,0);
else
    close
end

%Closing text file
if isempty(x)
else
    fclose(fn);
end

%%%% Plotting and saving the results %%%%
Tmrtday=Tmrtday/daytime;Lupday=Lupday/daytime;Ldownday=Ldownday/daytime;
Kupday=Kupday/daytime;Kdownday=Kdownday/daytime;gvfday=gvfday/daytime;
Tmrtdiurn=Tmrtdiurn/length(altitude);Lupdiurn=Lupdiurn/length(altitude);
Ldowndiurn=Ldowndiurn/length(altitude);
Solweig_2013b_output(modva,Tmrtday,x,y)
Solweig_outputfiles_day_2013b(output,fileformat,Tmrtday,Kupday,Kdownday,Lupday,Ldownday,gvfday,outputfolder,YYYY(1),DOY(1),PA,header,sizey);
Solweig_outputfiles_diurnal_2013b(output,fileformat,Tmrtdiurn,Lupdiurn,Ldowndiurn,svf,svfveg,outputfolder,YYYY(1),DOY(1),PA,header,sizey);


