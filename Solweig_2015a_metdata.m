function [met,met_header,YYYY,altitude,azimuth,zen,jday,leafon,dectime,altmax]=Solweig_2015a_metdata(inputmetfolder,inputmetfile,location,UTC)
% This function is used to process the input meteorological file. 
% It also calculates Sun position based on the time specified in the met-file
% New version from (2015a) alters hour to previous hour and change format
% to SUEWS 2014c format

% Read meteorological file
Newdata1=importdata([inputmetfolder inputmetfile],' ',1); % changed to space in 2015a
met=Newdata1.data;
met_header=Newdata1.textdata;
% halftimestep=floor((met(2,3)-met(1,3))*1440/2);
dectime=met(:,2)+met(:,3)./24+met(:,4)/(60*24);

halftimestepdec=(dectime(2)-dectime(1))/2;
% time.min=30;%min=30 (sunposition in the middle of the hour)!!!!!!!!!
% time.sec=0;
time.UTC=UTC;

% Read ModelledYears.txt (will change later)
% Newdata=importdata([inputmetfolder 'ModelledYears.txt'],' ',1);
% newyear=1;
% YYYYstart=Newdata.data(newyear,1);
% leafon1=Newdata.data(newyear,2);
% leafoff1=Newdata.data(newyear,3);
leafon1=97; % this should change
leafoff1=300; % this should change

altitude=zeros(1,length(Newdata1.data(:,1)));
azimuth=zeros(1,length(Newdata1.data(:,1)));
zen=zeros(1,length(Newdata1.data(:,1)));
jday=zeros(1,length(Newdata1.data(:,1)));
YYYY=zeros(1,length(Newdata1.data(:,1)));
leafon=zeros(1,length(Newdata1.data(:,1)));
altmax=zeros(1,length(Newdata1.data(:,1)));

for i=1:length(Newdata1.data(:,1))

    % Finding maximum altitude in 15 min intervals (20141027)
    if i==1 || mod(dectime(i,1),floor(dectime(i,1)))==0
        fifteen=0;
        sunmaximum=-90;
        sunmax.zenith=90;
        while sunmaximum<=90-sunmax.zenith
            sunmaximum=90-sunmax.zenith;
            fifteen=fifteen+15/1440;
            [time.year,time.month,time.day,time.hour,time.min,time.sec]=datevec(datenum([met(i,1),1,0])+floor(dectime(i,1))+(10*60)/1440+fifteen);
            sunmax=sun_position(time,location);
        end
    end
    altmax(1,i)=sunmaximum;
    
    % Creating time structure
    [time.year,time.month,time.day,time.hour,time.min,time.sec]=datevec(datenum([met(i,1),1,0])+dectime(i,1)-halftimestepdec);
    
    % Sun position
    sun=sun_position(time,location);
    altitude(1,i)=90-sun.zenith; azimuth(1,i)=sun.azimuth; zen(1,i)=sun.zenith*(pi/180);
    
    %day of year and check for leap year
    A=logical(mod(time.year,4));
    B=logical(mod(time.year,100));
    C=logical(mod(time.year,400));
    leapyear=ismember(time.year,time.year(~C | (~A & B)));
    if leapyear==1
        dayspermonth=[31 29 31 30 31 30 31 31 30 31 30 31];
    else
        dayspermonth=[31 28 31 30 31 30 31 31 30 31 30 31];
    end
    jday(1,i)=sum(dayspermonth(1:time.month-1))+time.day;
    YYYY(1,i)=met(i,1);
    if met(i,2)>leafon1 || met(i,2)<leafoff1
        leafon(1,i)=1;
    else
        leafon(1,i)=0;
    end

%     if i>24 && met(i,2)==0 && met(i,1)==0 % Multiple years
%         newyear=newyear+1;
%         YYYYstart=Newdata.data(newyear,1);
%         leafon1=Newdata.data(newyear,2);
%         leafoff1=Newdata.data(newyear,3);
%     end
    
end

% if time.month<10, XM='0'; else XM='';end
% if time.day<10, XD='0'; else XD='';end
% time=timemat;
