function [weight]=annulus_weight(altitude,aziinterval)

n=90;

steprad=(360/aziinterval)*(pi/180);
annulus=(91-altitude);% 91 before
% annulus=(91-altitude);% Temporary for vegsvf testing
w=(1/(2*pi))*sin(pi/(2*n))*sin((pi*(2*annulus-1))/(2*n));

weight=steprad*w;