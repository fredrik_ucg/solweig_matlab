function [Least,Lsouth,Lwest,Lnorth]=Lside_veg_v2015a(svfS,svfW,svfN,svfE,...
    svfEveg,svfSveg,svfWveg,svfNveg,svfEaveg,svfSaveg,svfWaveg,svfNaveg,...
    azimuth,altitude,Ta,Tw,SBC,ewall,Ldown,esky,t,F_sh,CI,LupE,LupS,LupW,LupN)

% This m-file is the current one that estimates L from the four cardinal points 20100414

%Building height angle from svf
svfalfaE=asin(exp((log(1-svfE))/2));
svfalfaS=asin(exp((log(1-svfS))/2));
svfalfaW=asin(exp((log(1-svfW))/2));
svfalfaN=asin(exp((log(1-svfN))/2));

vikttot=4.4897;
aziW=azimuth+t;aziN=azimuth-90+t;aziE=azimuth-180+t;aziS=azimuth-270+t;

F_sh=2*F_sh-1;%(cylindric_wedge scaled 0-1)

c=1-CI;
Lsky_allsky=esky*SBC*((Ta+273.15)^4)*(1-c)+c*SBC*((Ta+273.15)^4);

%% Least
[viktveg,viktwall,viktsky,viktrefl]=Lvikt_veg(svfE,svfEveg,svfEaveg,vikttot);

if altitude>0 % daytime
%     alfaB=atan(svfalfaE);
%     betaB=atan(tan((svfalfaE).*F_sh));
%     betasun=((alfaB-betaB)/2)+betaB;
    betasun = atan(0.5*tan(svfalfaE).*(1+F_sh)); 
    if azimuth > (180-t)  &&  azimuth <= (360-t)
        Lwallsun=SBC*ewall*((Ta+273.15+Tw*sin(aziE*(pi/180))).^4)*...
            viktwall.*(1-F_sh).*cos(betasun)*0.5;
        Lwallsh=SBC*ewall*((Ta+273.15).^4)*viktwall.*F_sh*0.5;
    else
        Lwallsun=0;
        Lwallsh=SBC*ewall*((Ta+273.15).^4)*viktwall.*0.5;
    end
else %nighttime
    Lwallsun=0;
    Lwallsh=SBC*ewall*((Ta+273.15).^4)*viktwall*0.5;
end
Lsky=((svfE+svfEveg-1)*Lsky_allsky).*viktsky*0.5;
Lveg=SBC*ewall*((Ta+273.15).^4)*viktveg*0.5;
Lground=LupE*0.5;
Lrefl=(Ldown+LupE).*(viktrefl)*(1-ewall)*0.5;
Least=Lsky+Lwallsun+Lwallsh+Lveg+Lground+Lrefl;

% Plotting testing
% subplot(3,4,2),imagesc(viktveg,[0 1]),axis image,title('viktveg')
% subplot(3,4,3),imagesc(viktrefl,[0 1]),axis image,title('viktrefl')
% subplot(3,4,4),imagesc(viktwall,[0 1]),axis image,title('viktwall')
% subplot(3,4,5),imagesc(viktsky,[0 1]),axis image,title('viktsky')
% subplot(3,4,6),imagesc(Lground),axis image,title('Lground')
% subplot(3,4,7),imagesc(Least,[300 450]),axis image,title(['LNorth (300-450) Alt=' num2str(altitude) ' Az=' num2str(azimuth)])
% subplot(3,4,8),imagesc(Lwallsun,[000 200]),axis image,title('LwallsunNorth (0-200)')
% subplot(3,4,9),imagesc(Lwallsh,[000 200]),axis image,title('LwallshadowNorth (0-200)')
% subplot(3,4,10),imagesc(Lveg,[000 200]),axis image,title('LwallvegNorth (0-200)')
% subplot(3,4,11),imagesc(Lrefl,[000 200]),axis image,title('LreflectedNorth (0-200)')
% subplot(3,4,12),imagesc(Lsky,[000 200]),axis image,title('LskyNorth (0-200)')
% pause(0.2)

clear alfaB betaB betasun Lsky Lwallsh Lwallsun Lveg Lground Lrefl viktveg viktwall viktsky

%% Lsouth
[viktveg,viktwall,viktsky,viktrefl]=Lvikt_veg(svfS,svfSveg,svfSaveg,vikttot);

if altitude>0 % daytime
%     alfaB=atan(svfalfaS);
%     betaB=atan(tan((svfalfaS).*F_sh));
%     betasun=((alfaB-betaB)/2)+betaB;
    betasun = atan(0.5*tan(svfalfaS).*(1+F_sh)); 
    if azimuth <= (90-t)  ||  azimuth > (270-t)
        Lwallsun=SBC*ewall*((Ta+273.15+Tw*sin(aziS*(pi/180))).^4)*...
            viktwall.*(1-F_sh).*cos(betasun)*0.5;
        Lwallsh=SBC*ewall*((Ta+273.15).^4)*viktwall.*F_sh*0.5;
    else
        Lwallsun=0;
        Lwallsh=SBC*ewall*((Ta+273.15).^4)*viktwall.*0.5;
    end
else %nighttime
    Lwallsun=0;
    Lwallsh=SBC*ewall*((Ta+273.15).^4)*viktwall*0.5;
end
Lsky=((svfS+svfSveg-1)*Lsky_allsky).*viktsky*0.5;
Lveg=SBC*ewall*((Ta+273.15).^4)*viktveg*0.5;
Lground=LupS*0.5;
Lrefl=(Ldown+LupS).*(viktrefl)*(1-ewall)*0.5;
Lsouth=Lsky+Lwallsun+Lwallsh+Lveg+Lground+Lrefl;

clear alfaB betaB betasun Lsky Lwallsh Lwallsun Lveg Lground Lrefl viktveg viktwall viktsky

%% Lwest
[viktveg,viktwall,viktsky,viktrefl]=Lvikt_veg(svfW,svfWveg,svfWaveg,vikttot);

if altitude>0 % daytime
%     alfaB=atan(svfalfaW);
%     betaB=atan(tan((svfalfaW).*F_sh));
%     betasun=((alfaB-betaB)/2)+betaB;
    betasun = atan(0.5*tan(svfalfaW).*(1+F_sh)); 
    if azimuth > (360-t)  ||  azimuth <= (180-t)
        Lwallsun=SBC*ewall*((Ta+273.15+Tw*sin(aziW*(pi/180))).^4)*...
            viktwall.*(1-F_sh).*cos(betasun)*0.5;
        Lwallsh=SBC*ewall*((Ta+273.15).^4)*viktwall.*F_sh*0.5;
    else
        Lwallsun=0;
        Lwallsh=SBC*ewall*((Ta+273.15).^4)*viktwall.*0.5;
    end
else %nighttime
    Lwallsun=0;
    Lwallsh=SBC*ewall*((Ta+273.15).^4)*viktwall*0.5;
end
Lsky=((svfW+svfWveg-1)*Lsky_allsky).*viktsky*0.5;
Lveg=SBC*ewall*((Ta+273.15).^4)*viktveg*0.5;
Lground=LupW*0.5;
Lrefl=(Ldown+LupW).*(viktrefl)*(1-ewall)*0.5;
Lwest=Lsky+Lwallsun+Lwallsh+Lveg+Lground+Lrefl;

clear alfaB betaB betasun Lsky Lwallsh Lwallsun Lveg Lground Lrefl viktveg viktwall viktsky

%% Lnorth
[viktveg,viktwall,viktsky,viktrefl]=Lvikt_veg(svfN,svfNveg,svfNaveg,vikttot);

if altitude>0 % daytime
%     alfaB=atan(svfalfaN);
%     betaB=atan(tan((svfalfaN).*F_sh));
%     betasun=((alfaB-betaB)/2)+betaB;
    betasun = atan(0.5*tan(svfalfaN).*(1+F_sh)); 
    if azimuth > (90-t)  &&  azimuth <= (270-t)
        Lwallsun=SBC*ewall*((Ta+273.15+Tw*sin(aziN*(pi/180))).^4)*...
            viktwall.*(1-F_sh).*cos(betasun)*0.5;
        Lwallsh=SBC*ewall*((Ta+273.15).^4)*viktwall.*F_sh*0.5;
    else
        Lwallsun=0;
        Lwallsh=SBC*ewall*((Ta+273.15).^4)*viktwall.*0.5;
    end
else %nighttime
    Lwallsun=0;
    Lwallsh=SBC*ewall*((Ta+273.15).^4)*viktwall*0.5;
end
Lsky=((svfN+svfNveg-1)*Lsky_allsky).*viktsky*0.5;
Lveg=SBC*ewall*((Ta+273.15).^4)*viktveg*0.5;
Lground=LupN*0.5;
Lrefl=(Ldown+LupN).*(viktrefl)*(1-ewall)*0.5;
Lnorth=Lsky+Lwallsun+Lwallsh+Lveg+Lground+Lrefl;



clear alfaB betaB betasun Lsky Lwallsh Lwallsun Lveg Lground Lrefl viktveg viktwall viktsky
