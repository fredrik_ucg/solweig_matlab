function [build,index,loctemp]=undo_mark_building(build,index,loctemp,sizex,sizey)
build(:,:,index)=build(:,:,index-2);
imagesc(build(1:sizey,1:sizex,index)), axis image
loctemp(index-1,:)=0;
index=index+1;