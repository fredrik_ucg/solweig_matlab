function [svf,svfE,svfS,svfW,svfN,iazimuth,ialtitude]=Skyviewfactor4d(outputfiles,a,scale,header,sizex,sizey)
%This m.file calculates Skyview factors on a DEM for the four cardinal points
%This new version is NOT using 1000 randow shadow casting, but implies
%the theory of annulus weights (e.g. Steyn, 1980). The number of shadow
%castings is reduced to 653.
%20130208 - changed to use cell input

svf=sparse(zeros(sizey,sizex));
svfE=svf;svfS=svf;svfW=svf;svfN=svf;

noa=19;% No. of anglesteps minus 1
step=89/noa;
iangle=[((step/2):step:89) 90];
annulino=[round(0:step:89) 90];
[iazimuth, aziinterval]=svf_angles_100121(iangle);
aziintervalaniso=ceil(aziinterval/2);
ialtitude=zeros(1,653);
index=1;
for i=1:length(iangle)
    for j=1:aziinterval(i)
        progressbar(index/653,0);
        altitude=iangle(i);
        ialtitude(index)=altitude;
        azimuth=iazimuth(index);%sh=1;
        sh=shadowingfunctionglobalradiation(a,azimuth,altitude,scale);
%         for k=annulino(i):annulino(i+1)-1
        for k=annulino(i)+1:annulino(i+1)% changed to include 90
            weight=annulus_weight(k,aziinterval(i))*sh;
            svf=svf+weight;
            if azimuth >= 0  &&  azimuth < 180
                weight=annulus_weight(k,aziintervalaniso(i))*sh;
                svfE=svfE+weight;
            end
            if azimuth >= 90  &&  azimuth < 270
                weight=annulus_weight(k,aziintervalaniso(i))*sh;
                svfS=svfS+weight;
            end
            if azimuth >= 180  &&  azimuth < 360
                weight=annulus_weight(k,aziintervalaniso(i))*sh;
                svfW=svfW+weight;
            end
            if azimuth >= 270  ||  azimuth < 90
                weight=annulus_weight(k,aziintervalaniso(i))*sh;
                svfN=svfN+weight;
            end
        end
        index=index+1;
    end
end
progressbar(1,0);
% close
svfS=svfS+(3.0459e-004);svfW=svfW+(3.0459e-004); % Last azimuth is 90. Hence, manual add of last annuli for svfS and SVFW

%Forcing svf not be greater than 1 (some MATLAB crazyness)
svf(svf>1)=1;svfE(svfE>1)=1;svfS(svfS>1)=1;svfW(svfW>1)=1;svfN(svfN>1)=1;

%Save svfs image as ESRIGRID
out=[];
out(:,:,1)=svf(1:sizey,1:sizex);
out(:,:,2)=svfE(1:sizey,1:sizex);
out(:,:,3)=svfS(1:sizey,1:sizex);
out(:,:,4)=svfW(1:sizey,1:sizex);
out(:,:,5)=svfN(1:sizey,1:sizex);

for i=1:5
    fn2=fopen(outputfiles{i},'w');
    for p=1:6
        fprintf(fn2,'%6s', header{p});
        fprintf(fn2,'\r\n');
    end
    for p=1:size(out(:,:,1),1)
        fprintf(fn2,'%6.4f ',out(p,:,i));
        fprintf(fn2,'\r\n');
    end
    fclose(fn2);
end

% figure
% imagesc(svf); colormap(gray);colorbar;axis square;
% figure
% subplot(2,2,2),imagesc(svfE), axis square, colormap(gray), title('SVF facing East'), colorbar;
% subplot(2,2,4),imagesc(svfS), axis square, colormap(gray), title('SVF facing South'), colorbar;
% subplot(2,2,3),imagesc(svfW), axis square, colormap(gray), title('SVF facing West'), colorbar;
% subplot(2,2,1),imagesc(svfN), axis square, colormap(gray), title('SVF facing North'), colorbar;
