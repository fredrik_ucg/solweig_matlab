function [D]=sun_distance(jday)

% Calculates solar irradiance variation based on mean earth sun distance
% with day of year as input.
% Partridge and Platt, 1975

b = 2*pi.*jday/365;
D = sqrt(1.00011 + 0.034221 * cos(b) + 0.001280 * sin(b) + 0.000719 * cos(2*b) + 0.000077 * sin(2*b));
