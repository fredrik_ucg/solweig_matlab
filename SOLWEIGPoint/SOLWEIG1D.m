function varargout = SOLWEIG1D(varargin)
% SOLWEIG1D M-file for SOLWEIG1D.fig
% This is a main file for the SOLWEIG GUI
% Created by Fredrik Lindberg
% 2013-02-15, 
% Urban Climate Group, Gothenburg University
% fredrikl@gvc.gu.se
% mcc('-e','-R','-logfile,"errorlog1D.txt"','-v','SOLWEIG1D.m')

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
    'gui_Singleton',  gui_Singleton, ...
    'gui_OpeningFcn', @SOLWEIG1D_OpeningFcn, ...
    'gui_OutputFcn',  @SOLWEIG1D_OutputFcn, ...
    'gui_LayoutFcn',  [] , ...
    'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

% --- Executes just before SOLWEIG1D is made visible.
function SOLWEIG1D_OpeningFcn(hObject, eventdata, handles, varargin)
% cities location
data=importdata([pwd filesep 'locations.txt'],'\t',1);
textdata=data.textdata;
data=data.data;

for i=2:size(textdata,1)
    textdata2{i-1,1}=strcat(textdata{i,1},', ',textdata{i,2});
end
set(handles.popupmenu_location,'String',textdata2)

set(handles.edit_lon,'String',num2str(data((1),1)))
set(handles.edit_lat,'String',num2str(data((1),2)))
set(handles.edit_alt,'String',num2str(data((1),3)))
set(handles.edit_utc,'String',num2str(data((1),4)))

% ground cover
data2=importdata([pwd filesep 'landcoverclasses_2015a.txt'],' ',1);
textdata2=data2.textdata;
% data=data.data;

for i=2:size(textdata2,1)
    textdata3{i-1,1}=textdata2{i,1};
end
set(handles.popupmenu_ground,'String',textdata3)
set(handles.popupmenu_ground,'Value',3)

% Choose default command line output for SOLWEIG1D
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes SOLWEIG1D wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = SOLWEIG1D_OutputFcn(hObject, eventdata, handles)
% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in pushbutton_loadimage.
function pushbutton_loadimage_Callback(hObject, eventdata, handles)
set(handles.pushbutton_fill,'Enable','off')
set(handles.pushbutton_svf,'Enable','off')
set(handles.checkbox_datafile,'UserData',[]);
[inputfile, directory]=uigetfile({'*.jpg;*.png;*.gif;*.tif;*.pgm'},'Select image file');
% set(handles.filename,'String',inputfile)
if inputfile==0
else
    data=importdata([directory inputfile]);
    hold off
    imagesc(data,'parent',handles.axes1);
    set(handles.axes1,'Visible','off')
    axis image
    set(handles.pushbutton_loadimage,'UserData',data);
    guidata(hObject, handles);
    if isempty(data)
    else
        set(handles.pushbutton_radius,'Enable','on')
    end
end

% --- Executes on button press in pushbutton_radius.
function pushbutton_radius_Callback(hObject, eventdata, handles)
data=get(handles.pushbutton_loadimage,'UserData');
imagesc(data,'parent',handles.axes1);
set(handles.axes1,'Visible','off')
set(handles.checkbox_datafile,'UserData',[]);
axis image
hold on
point=0;posx=[];posy=[];
while point~=3
    [x,y]=ginput(1);
    y=floor(y);x=floor(x);
    plot(x,y,'r +');
    point=point+1;
    posx(point)=x;
    posy(point)=y;
end

% Calculate radius and centre
A=posx(1)^2-posx(2)^2+posy(1)^2-posy(2)^2;
B=2*(posx(2)-posx(1));
C=2*(posy(2)-posy(1));
D=posx(2)^2-posx(3)^2+posy(2)^2-posy(3)^2;
E=2*(posx(3)-posx(2));
F=2*(posy(3)-posy(2));
centerX=(D/F-A/C)/(B/C-E/F);
centerY=(D/E-A/B)/(C/B-F/E);
radie2(1)=sqrt((posx(1)-centerX)^2+(posy(1)-centerY)^2);
radie2(2)=sqrt((posx(2)-centerX)^2+(posy(2)-centerY)^2);
radie2(3)=sqrt((posx(3)-centerX)^2+(posy(3)-centerY)^2);
radie=mean(radie2);

plot(centerX,centerY,'r *');
pause(1)
hold off

posdata=[centerX centerY radie];
set(handles.pushbutton_radius,'UserData',posdata);
guidata(hObject, handles);
set(handles.slider2,'Enable','on')
set(handles.text1,'Enable','on')
set(handles.pushbutton_sundiagram,'Enable','on')

% --- Executes on slider movement.
function slider2_Callback(hObject, eventdata, handles)
if isempty(get(handles.slider2,'UserData'))==0
    data=get(handles.pushbutton_loadimage,'UserData');
    hold off
    imagesc(data),axis image
    set(handles.axes1,'Visible','off')
    hold on
    set(handles.slider2,'UserData',[]);
end

data=get(handles.pushbutton_loadimage,'UserData');
tres=get(hObject,'Value');
BW = im2bw(data, tres);
imagesc(BW,[0 1]),axis image,colormap(gray)
set(handles.axes1,'Visible','off')
set(handles.pushbutton_fill,'UserData',BW);
guidata(hObject, handles);
set(handles.pushbutton_fill,'Enable','on')
set(handles.pushbutton_svf,'Enable','on')

% --- Executes during object creation, after setting all properties.
function slider2_CreateFcn(hObject, eventdata, handles)
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end

% --- Executes on button press in pushbutton_fill.
function pushbutton_fill_Callback(hObject, eventdata, handles)
BW=get(handles.pushbutton_fill,'UserData');
BW=double(BW);
J=roipoly(BW);
J=J*-1+1;
BW=BW.*J;
imagesc(BW),axis image,colormap(gray)
set(handles.axes1,'Visible','off')
set(handles.pushbutton_fill,'UserData',BW);
guidata(hObject, handles);
set(handles.checkbox_sh,'Enable','off')

function edit_camera_Callback(hObject, eventdata, handles)
get(hObject,'String');
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function edit_camera_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on button press in pushbutton_svf.
function pushbutton_svf_Callback(hObject, eventdata, handles)
BW=get(handles.pushbutton_fill,'UserData');
camradie=str2double(get(handles.edit_camera,'String'));
posdata=get(handles.pushbutton_radius,'UserData');
centerX=posdata(1);
centerY=posdata(2);
radie=posdata(3);
radie=radie*(180/camradie);% correction for camera field of view

%%%%% Pixel version %%%%%
weightmap=BW*0;
for i=1:size(BW,2)%columns
    for j=1:size(BW,1)%rows
        a=i-centerX;%i
        b=j-centerY;%j
        c=sqrt(a^2+b^2);
        weight=1/(2*pi*c)*sin(pi/(2*radie))*sin(pi*(2*c-1)/(2*radie));%*vf;
        weightmap(j,i)=weight;
    end
end
svfimage=weightmap.*BW;
svf=sum(svfimage(:));

svftext=num2str(svf,2);

set(handles.edit_svf,'String',svftext);
set(handles.edit_svf,'Value',svf);

guidata(hObject, handles);

% --- Executes on button press in pushbutton_date.
function pushbutton_date_Callback(hObject, eventdata, handles)
selectedDate = uical([2012 06 21],'en');
set(handles.text_date,'Value',selectedDate)
guidata(hObject, handles);
set(handles.text_date, 'String', datestr(selectedDate, 24));

% --- Executes on selection change in popupmenu_location.
function popupmenu_location_Callback(hObject, eventdata, handles)
row=get(hObject,'Value');
data=importdata([pwd filesep 'locations.txt']);
data=data.data;
set(handles.edit_lon,'String',num2str(data((row),1)))
set(handles.edit_lat,'String',num2str(data((row),2)))
set(handles.edit_alt,'String',num2str(data((row),3)))
set(handles.edit_utc,'String',num2str(data((row),4)))
set(handles.edit_lon,'Value',(data((row),1)))
set(handles.edit_lat,'Value',(data((row),2)))
set(handles.edit_alt,'Value',(data((row),3)))
set(handles.edit_utc,'Value',(data((row),4)))
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function popupmenu_location_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on button press in pushbutton_sundiagram.
function pushbutton_sundiagram_Callback(hObject, eventdata, handles)
% if get(handles.popupmenu_location,'Value')==1
%     msgboxText{1} =  'Choose a geographical location';
%     msgbox(msgboxText,'No geographical location selected', 'error');
% else

location.longitude=str2double(get(handles.edit_lon,'String'));
location.latitude=str2double(get(handles.edit_lat,'String'));
location.altitude=str2double(get(handles.edit_alt,'String'));
location.UTC=str2double(get(handles.edit_utc,'String'));

jday=get(handles.text_date,'Value');
if jday==0
    jday=735041;
end
dates=datevec(jday);
timestep=0.5;

posdata=get(handles.pushbutton_radius,'UserData');
centerX=posdata(1);
centerY=posdata(2);
radie=posdata(3);

data=get(handles.pushbutton_loadimage,'UserData');

h=SunpathdiagramOnPhoto_Solweigpoint( location, dates, timestep, data ,centerX,centerY,radie,jday);
set(handles.slider2,'UserData',h);
% end

% --- Executes during object creation, after setting all properties.
function edit_svf_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit_svf_Callback(hObject, eventdata, handles)
input = str2double(get(hObject,'String'));
if input>1|| input<0
    msgboxText{1} =  'Choose a decimal number between 0 and 1';
    msgbox(msgboxText,'Number has to be between 0 and 1', 'error');
end
guidata(hObject, handles);

function edit_height_Callback(hObject, eventdata, handles)
input = str2double(get(hObject,'String'));
if input>500 || input<0
    msgboxText{1} =  'Choose a number between 0 and 500';
    msgbox(msgboxText,'Number has to be between 0 and 500', 'error');
end
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function edit_height_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit_sex_Callback(hObject, eventdata, handles)
input = (get(hObject,'String'));
if strcmp(input,'m')+strcmp(input,'f')==0
    msgboxText{1} =  'Choose either "m" or "f" ';
    msgbox(msgboxText,'Gender has to be either "m" or "f" ', 'error');
end
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function edit_sex_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit_age_Callback(hObject, eventdata, handles)
input = str2double(get(hObject,'String'));
if input>150 || input<0
    msgboxText{1} =  'Choose a decimal between 0 and 150';
    msgbox(msgboxText,'Number has to be between 0 and 150', 'error');
end
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function edit_age_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit_activity_Callback(hObject, eventdata, handles)
input = str2double(get(hObject,'String'));
if input>800 || input<0
    msgboxText{1} =  'Choose a number between 0 and 800';
    msgbox(msgboxText,'Number has to be between 0 and 800', 'error');
end
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function edit_activity_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit_weight_Callback(hObject, eventdata, handles)
input = str2double(get(hObject,'String'));
if input>500 || input<0
    msgboxText{1} =  'Choose a number between 0 and 500';
    msgbox(msgboxText,'Number has to be between 0 and 500', 'error');
end
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function edit_weight_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit_absK_Callback(hObject, eventdata, handles)
input = str2double(get(hObject,'String'));
if input>1 || input<0
    msgboxText{1} =  'Choose a decimal number between 0 and 1';
    msgbox(msgboxText,'Number has to be between 0 and 1', 'error');
end
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function edit_absK_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit_absL_Callback(hObject, eventdata, handles)
input = str2double(get(hObject,'String'));
if input>1 || input<0
    msgboxText{1} =  'Choose a decimal number between 0 and 1';
    msgbox(msgboxText,'Number has to be between 0 and 1', 'error');
end
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function edit_absL_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit_clo_Callback(hObject, eventdata, handles)
input = str2double(get(hObject,'String'));
if input>5 || input<0
    msgboxText{1} =  'Choose a decimal number between 0 and 5';
    msgbox(msgboxText,'Number has to be between 0 and 5', 'error');
end
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function edit_clo_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on button press in checkbox_sh.
function checkbox_sh_Callback(hObject, eventdata, handles)
get(hObject,'Value');
guidata(hObject, handles);

function edit_eg_Callback(hObject, eventdata, handles)
input = str2double(get(hObject,'String'));
if input>1 || input<0
    msgboxText{1} =  'Choose a decimal number between 0 and 1';
    msgbox(msgboxText,'Number has to be between 0 and 1', 'error');
end
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function edit_eg_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit_albedo_w_Callback(hObject, eventdata, handles)
input = str2double(get(hObject,'String'));
if input>1 || input<0
    msgboxText{1} =  'Choose a decimal number between 0 and 1';
    msgbox(msgboxText,'Number has to be between 0 and 1', 'error');
end
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function edit_albedo_w_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit_albedo_g_Callback(hObject, eventdata, handles)
input = str2double(get(hObject,'String'));
if input>1 || input<0
    msgboxText{1} =  'Choose a decimal number between 0 and 1';
    msgbox(msgboxText,'Number has to be between 0 and 1', 'error');
end
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function edit_albedo_g_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit_ew_Callback(hObject, eventdata, handles)
input = str2double(get(hObject,'String'));
if input>1 || input<0
    msgboxText{1} =  'Choose a decimal number between 0 and 1';
    msgbox(msgboxText,'Number has to be between 0 and 1', 'error');
end
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function edit_ew_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on selection change in popupmenu_paste.
function popupmenu_paste_Callback(hObject, eventdata, handles)
get(hObject,'String');
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function popupmenu_paste_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit_lon_Callback(hObject, eventdata, handles)
input = str2double(get(hObject,'String'));
if input>180 || input<-180
    msgboxText{1} =  'Choose a decimal number between 180 and -180';
    msgbox(msgboxText,'Number has to be between 180 and -180', 'error');
end
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function edit_lon_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit_lat_Callback(hObject, eventdata, handles)
input = str2double(get(hObject,'String'));
if input>90 || input<-90
    msgboxText{1} =  'Choose a decimal number between 90 and -90';
    msgbox(msgboxText,'Number has to be between 90 and -90', 'error');
end
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function edit_lat_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit_alt_Callback(hObject, eventdata, handles)
input = str2double(get(hObject,'String'));
if input>9000 || input<-100
    msgboxText{1} =  'Choose a decimal number between 9000 and -100';
    msgbox(msgboxText,'Number has to be between 9000 and -100', 'error');
end
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function edit_alt_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit_utc_Callback(hObject, eventdata, handles)
input = str2double(get(hObject,'String'));
if input>12 || input<-12
    msgboxText{1} =  'Choose a decimal number between 12 and -12';
    msgbox(msgboxText,'Number has to be between 12 and -12', 'error');
end
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function edit_utc_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit_min_Callback(hObject, eventdata, handles)
input = str2double(get(hObject,'String'));
if input>59 || input<0
    msgboxText{1} =  'Choose a number between 0 and 59';
    msgbox(msgboxText,'Number has to be between 0 and 59', 'error');
end
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function edit_min_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit_hour_Callback(hObject, eventdata, handles)
input = str2double(get(hObject,'String'));
if input>24 || input<0
    msgboxText{1} =  'Choose a number between 0 and 24';
    msgbox(msgboxText,'Number has to be between 0 and 24', 'error');
end
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function edit_hour_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit_utci_Callback(hObject, eventdata, handles)

% --- Executes during object creation, after setting all properties.
function edit_utci_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit_pet_Callback(hObject, eventdata, handles)

% --- Executes during object creation, after setting all properties.
function edit_pet_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit_tmrt_Callback(hObject, eventdata, handles)

% --- Executes during object creation, after setting all properties.
function edit_tmrt_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit_ta_Callback(hObject, eventdata, handles)
input = str2double(get(hObject,'String'));
if input>70 || input<-70
    msgboxText{1} =  'Choose a number between -70 and 70';
    msgbox(msgboxText,'Number has to be between -70 and 70', 'error');
end
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function edit_ta_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit_rh_Callback(hObject, eventdata, handles)
input = str2double(get(hObject,'String'));
if input>100 || input<0
    msgboxText{1} =  'Choose a number between 0 and 100';
    msgbox(msgboxText,'Number has to be between 0 and 100', 'error');
end
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function edit_rh_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit_ws_Callback(hObject, eventdata, handles)
input = str2double(get(hObject,'String'));
if input>50 || input<0
    msgboxText{1} =  'Choose a number between 0 and 50';
    msgbox(msgboxText,'Number has to be between 0 and 50', 'error');
end
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function edit_ws_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit_grad_Callback(hObject, eventdata, handles)
input = str2double(get(hObject,'String'));
if input>1000 || input<0
    msgboxText{1} =  'Choose a number between 0 and 1000';
    msgbox(msgboxText,'Number has to be between 0 and 1000', 'error');
end
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function edit_grad_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit_irad_Callback(hObject, eventdata, handles)
input = str2double(get(hObject,'String'));
if input>1200 || input<0
    msgboxText{1} =  'Choose a number between 0 and 1200';
    msgbox(msgboxText,'Number has to be between 0 and 1200', 'error');
end
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function edit_irad_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit_drad_Callback(hObject, eventdata, handles)
input = str2double(get(hObject,'String'));
if input>1000 || input<0
    msgboxText{1} =  'Choose a number between 0 and 1000';
    msgbox(msgboxText,'Number has to be between 0 and 1000', 'error');
end
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function edit_drad_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on button press in pushbutton_load.
function pushbutton_load_Callback(hObject, eventdata, handles)
[inputfile, directory]=uigetfile({'*.txt'},'Select text file');
set(handles.text_file,'String','File loaded')
set(handles.text_filename,'String',{directory inputfile})
guidata(hObject, handles);

% --- Executes on button press in checkbox_datafile.
function checkbox_datafile_Callback(hObject, eventdata, handles)
stat=get(hObject,'Value');
if stat==1
    set(handles.pushbutton_load,'Enable','on')
    set(handles.edit_ta,'Enable','off')
    set(handles.edit_rh,'Enable','off')
    set(handles.edit_ws,'Enable','off')
    set(handles.edit_grad,'Enable','off')
    set(handles.edit_drad,'Enable','off')
    set(handles.edit_irad,'Enable','off')
else
    set(handles.pushbutton_load,'Enable','off')
    set(handles.edit_ta,'Enable','on')
    set(handles.edit_rh,'Enable','on')
    set(handles.edit_ws,'Enable','on')
    set(handles.edit_grad,'Enable','on')
    set(handles.edit_drad,'Enable','on')
    set(handles.edit_irad,'Enable','on')
    set(handles.text_file,'String',' ')
end
guidata(hObject, handles);

% --- Executes on button press in checkbox_reindl.
function checkbox_reindl_Callback(hObject, eventdata, handles)
get(hObject,'Value');
guidata(hObject, handles);

% --- Executes on button press in checkbox_cyl.
function checkbox_cyl_Callback(hObject, eventdata, handles)
get(hObject,'Value');
guidata(hObject, handles);

function edit_sensorheight_Callback(hObject, eventdata, handles)
input = str2double(get(hObject,'String'));
if input>1200 || input<0
    msgboxText{1} =  'Choose a number between 0 and 100';
    msgbox(msgboxText,'Number has to be between 0 and 100', 'error');
end

% --- Executes during object creation, after setting all properties.
function edit_sensorheight_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popupmenu_ground.
function popupmenu_ground_Callback(hObject, eventdata, handles)
get(hObject,'String');
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function popupmenu_ground_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton_solweig.
function pushbutton_solweig_Callback(hObject, eventdata, handles)
% windheight=get(handles.uipanel_magl,'UserData');
albedo_b=str2double(get(handles.edit_albedo_w,'String'));
% albedo_g=str2double(get(handles.edit_albedo_g,'String'));
ewall=str2double(get(handles.edit_ew,'String'));
% eground=str2double(get(handles.edit_eg,'String'));
absK=str2double(get(handles.edit_absK,'String'));
absL=str2double(get(handles.edit_absL,'String'));
paste=get(handles.popupmenu_paste,'Value');
if paste==1,Fside=0.22;Fup=0.06;end
if paste==2,Fside=0.166;Fup=Fside;end

% if get(handles.popupmenu_location,'Value')==1
%     msgboxText{1} =  'Choose a geographical location';
%     msgbox(msgboxText,'No geographical location selected', 'error');
% else
location.longitude=str2double(get(handles.edit_lon,'String'));
location.latitude=str2double(get(handles.edit_lat,'String'));
location.altitude=str2double(get(handles.edit_alt,'String'));
location.utc=str2double(get(handles.edit_utc,'String'));
% end
% ground cover
ground=get(handles.popupmenu_ground,'Value');
data2=importdata([pwd filesep 'landcoverclasses_2015a_1D.txt'],' ',1);
albedo_g=data2.data(ground,2);
eground=data2.data(ground,3);
TgK=data2.data(ground,4);
Tstart=data2.data(ground,5);
cyl=get(handles.checkbox_cyl,'Value');
onlyglobal=get(handles.checkbox_reindl,'Value');
svf=str2double(get(handles.edit_svf,'String'));
sensorheight=str2double(get(handles.edit_sensorheight,'String'));

% PET variables
pet.mbody=str2double(get(handles.edit_weight,'String'));
pet.age=str2double(get(handles.edit_age,'String'));
pet.height=str2double(get(handles.edit_height,'String'));
pet.activity=str2double(get(handles.edit_activity,'String'));
sex=get(handles.edit_sex,'String');
if sex=='m'
    pet.sex=1;
else
    pet.sex=2;
end
% pet.sex=str2double(get(handles.edit_sex,'String'));
pet.clo=str2double(get(handles.edit_clo,'String'));
datafile=get(handles.checkbox_datafile,'Value');
%met data
if datafile==0
    jday=get(handles.text_date,'Value');
    if jday==0,jday=735041;end
    dates=datevec(jday);
    YYYY=dates(1);
    MM=dates(2);
    DD=dates(3);
    
    %day of year and check for leap year
    A=logical(mod(YYYY,4));
    B=logical(mod(YYYY,100));
    C=logical(mod(YYYY,400));
    leapyear=ismember(YYYY,YYYY(~C | (~A & B)));
    if leapyear==1
        dayspermonth=[31 29 31 30 31 30 31 31 30 31 30 31];
    else
        dayspermonth=[31 28 31 30 31 30 31 31 30 31 30 31];
    end
    met(1)=YYYY;
    met(2)=sum(dayspermonth(1:MM-1))+DD;
    met(3)=str2double(get(handles.edit_hour,'String'));
    % decimal time
    dectime=met(2)+str2double(get(handles.edit_hour,'String'))/...
        24+str2double(get(handles.edit_min,'String'))/1440;
    met(10)=str2double(get(handles.edit_ws,'String'));
    met(11)=str2double(get(handles.edit_rh,'String'));
    met(12)=str2double(get(handles.edit_ta,'String'));
    met(13)=1013; %P
    met(15)=str2double(get(handles.edit_grad,'String'));
    met(22)=str2double(get(handles.edit_drad,'String'));
    met(23)=str2double(get(handles.edit_irad,'String'));    
    
    [time.year,time.month,time.day,time.hour,time.min,time.sec]=datevec(datenum([YYYY,1,0])+dectime);%-halftimestepdec
    jday=met(2);
    % Sun position
    time.UTC=location.utc;
    sun=sun_position(time,location);
    altitude=90-sun.zenith; azimuth=sun.azimuth; zen=sun.zenith*(pi/180);
    
    % Finding maximum altitude in 15 min intervals (20141027)
    fifteen=0;
    sunmaximum=-90;
    sunmax.zenith=90;
    while sunmaximum<=90-sunmax.zenith
        sunmaximum=90-sunmax.zenith;
        fifteen=fifteen+15/1440;
        [time.year,time.month,time.day,time.hour,time.min,time.sec]=datevec(datenum([met(1),1,0])+floor(met(3))+(10*60)/1440+fifteen);
        sunmax=sun_position(time,location);
    end
    altmax=sunmaximum;
else
    metdatafile=get(handles.text_filename,'String');
    metdatafolder=metdatafile{1};
    metdatafile=metdatafile{2};
    %     [met,met_header,YYYY,altitude,azimuth,zen,jday,leafon]=Solweig_2014a_metdata(metdatafolder,metdatafile,location,location.utc);
    [met,~,YYYY,altitude,azimuth,zen,jday,leafon,dectime,altmax]=Solweig_2015a_metdata(metdatafolder,metdatafile,location,location.utc);
end

% shadow informationset
if isempty(get(handles.pushbutton_fill,'UserData'))==1
    sh1=get(handles.checkbox_sh,'Value');
    sh=ones(size(met(:,1),1),1)-sh1;
else
    if jday==0
        jday=735041;
    end
    
    posdata=get(handles.pushbutton_radius,'UserData');
    centerX=posdata(1);
    centerY=posdata(2);
    radie=posdata(3);
    data=get(handles.pushbutton_fill,'UserData');
    
    [sh,xx,yy]=shadowinfo_hemispherical(azimuth,zen...
        ,data,centerX,centerY,radie);
    
    if datafile==0
        hold on
        h=get(handles.checkbox_datafile,'UserData');
        if isempty(h)==0
            delete(h)
        end
        h=plot(xx,yy,'or');
        set(handles.checkbox_datafile,'UserData',h);
    end
end

% Main calculation
data=TmrtPET_calculator_solweigpoint_2015a_sensitivitytest(albedo_b,albedo_g,absK,absL,ewall,eground,Fside,Fup...
    ,met,altitude,azimuth,zen,jday,onlyglobal,location,svf,pet,sh,datafile,sensorheight,YYYY,TgK,Tstart...
    ,dectime,altmax,cyl);
% data=TmrtPET_calculator_solweigpoint_2014b(albedo_b,albedo_g,absK,absL,ewall,eground,Fside,Fup...
%     ,met,altitude,azimuth,zen,jday,onlyglobal,location,height,svf,pet,sh,datafile,sensorheight,YYYY);

% Output to GUI
if datafile==0
    set(handles.edit_tmrt,'String',num2str(data(1,1),4));
    set(handles.edit_pet,'String',num2str(data(1,2),4));
    set(handles.edit_utci,'String',num2str(data(1,3),4));
    if onlyglobal==1
        set(handles.edit_drad,'String',num2str(data(4),3));
        set(handles.edit_irad,'String',num2str(data(5),3));
    end
    
end

% Menu options
% ----------------------------------------------------------------
function File_Callback(hObject, eventdata, handles)

% --------------------------------------------------------------------
function Untitled_3_Callback(hObject, eventdata, handles)

% --------------------------------------------------------------------
function Help_Callback(hObject, eventdata, handles)
open([pwd '\manual\SOLWEIG1D-User manual.pdf'])

% --------------------------------------------------------------------
function About_Callback(hObject, eventdata, handles)
about()

% --------------------------------------------------------------------
function Exit_Callback(hObject, eventdata, handles)
close all


