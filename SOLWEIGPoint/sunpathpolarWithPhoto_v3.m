function hpol = sunpathpolarWithPhoto_v3(theta,rho,line_style,data,centerX,centerY,radie)
%POLAR  Polar coordinate plot.
%   POLAR(THETA, RHO) makes a plot using polar coordinates of
%   the angle THETA, in radians, versus the radius RHO.
%   POLAR(THETA,RHO,S) uses the linestyle specified in string S.
%   See PLOT for a description of legal linestyles.
%
%   See also PLOT, LOGLOG, SEMILOGX, SEMILOGY.

%   Copyright 1984-2002 The MathWorks, Inc.
%   $Revision: 5.22 $  $Date: 2002/04/08 21:44:28 $
%   Changed by Fredrik Lindberg 20120515

% make a radial grid
imagesc(data)
axis image
set(gca,'XTick',[],'YTick',[])
hold on;

% check radial limits and ticks
rmin = 0; %rmax = v(4); rticks = max(ticks-1,2);
rticks=9;
rmax=round(radie);

% define a circle
th = 0:pi/50:2*pi;
xunit = cos(th);
yunit = sin(th);
% now really force points on x/y axes to lie on them exactly
inds = 1:(length(th)-1)/4:length(th);
xunit(inds(2:2:4)) = zeros(2,1);
yunit(inds(1:2:5)) = zeros(3,1);

% draw radial circles
c82 = 1; cos(82*pi/180);
s82 = -0.1; sin(82*pi/180);
rinc = (rmax-rmin)/rticks;
linetext={'80','70','60','50','40','30','20','10',''};index=1;
for i=(rmin+rinc):rinc:rmax
    hhh = plot(xunit*i+(centerX),yunit*i+(centerY),'-.r','linewidth',1);
    text((i+rinc/50)*c82+centerX,(i+rinc/50)*s82+centerY, ...
        linetext{index},'color',[1 0 0]...
        ,'FontSize',8)
    index=index+1;
end
set(hhh,'linestyle','-') % Make outer circle solid

% plot spokes
th = (1:6)*2*pi/12;
cst = cos(th); snt = sin(th);
cs = [-cst; cst];
sn = [-snt; snt];
plot(rmax*cs+centerX,rmax*sn+centerY,'-.r','linewidth',1)

% annotate spokes in degrees
linetext={'120','150','180','210','240','270',''};
rt = 1.05*rmax;
for i = 1:length(th)
    text(rt*cst(i)+centerX,rt*snt(i)+centerY,linetext{i},...
        'horizontalalignment','center',...
        'fontsize',8,'color',[1 0 0]);
    if i > 2
        loc = int2str(-90+i*30);
    else
        loc = int2str(270+i*30);
    end
    text(-rt*cst(i)+centerX,-rt*snt(i)+centerY,loc,'horizontalalignment','center',...
        'fontsize',8,'color',[1 0 0])
end

% transform data to Cartesian coordinates.
yy = (rho.*cos(theta));
xx = (rho.*sin(theta));
yy = centerY+rmax-(90+yy)*rmax/90;
xx = (90+xx)*rmax/90+centerX-rmax;
% plot data on top of grid
if strcmp(line_style,'auto')
    q = plot(xx,yy);
else
    q = plot(xx,yy,line_style);
end
if nargout > 0
    hpol = q;
end

set(get(gca,'xlabel'),'visible','on')
set(get(gca,'ylabel'),'visible','on')



