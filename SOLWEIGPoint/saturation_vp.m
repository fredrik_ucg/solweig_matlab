function es=saturation_vp(Ta)
    % **********************************************
    %       real FUNCTION es(ta)
    % **********************************************
    % calculates saturation vapour pressure over water in hPa for input air temperature (ta) in celsius according to:
    % Hardy, R.; ITS-90 Formulations for Vapor Pressure, Frostpoint Temperature, Dewpoint Temperature and Enhancement Factors in the Range -100 to 100 �C;
    % Proceedings of Third International Symposium on Humidity and Moisture; edited by National Physical Laboratory (NPL), London, 1998, pp. 214-221
    % http://www.thunderscientific.com/tech_info/reflibrary/its90formulas.pdf (retrieved 2008-10-01)
    


    g=([-2.8365744E3,...
        -6.028076559E3,...
        1.954263612E1,...
        -2.737830188E-2,...
        1.6261698E-5,...
        7.0229056E-10,...
        -1.8680009E-13,...
        2.7150305]);
    
    tk=Ta+273.15; 		%! air temp in K
    es=g(8)*log(tk);
    for i=1:7
        es=es+g(i)*tk^(i-3);
    end
    es=exp(es)*0.01;	 %*0.01: convert Pa to hPa
