Name                   Code Alb  Emis TsPerDeg Tstart 
Cobble_stone_(v2014a)  1    0.20 0.95 0.37     -3.41
Dark_asphalt(all data) 2    0.18 0.95 0.58     -9.78
Dark_asphalt(filtered) 7    0.18 0.95 0.47     -5.05
Water                  3    0.05 0.98 0.00      0.00
Grass_unmanaged        4    0.16 0.94 0.13      0.93
Grass_managed          5    0.16 0.94 0.30     -2.0
test                   6    0.16 0.94 0.30     -2.0
