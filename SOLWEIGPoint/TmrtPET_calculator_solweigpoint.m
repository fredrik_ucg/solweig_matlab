function [data]=TmrtPET_calculator_solweigpoint(albedo,absK,absL,ewall,eground,Fside,Fup,met,altitude,azimuth,zen,jday,onlyglobal,location,height,svf,pet,sh,datafile)

% This mfile calculates Tmrt and PET for one sunlit point using SOLWEIG equations

%Initialization output textfile
fn=fopen([pwd, filesep, 'OutputData_SOLWEIGpoint_Calculator_', datestr(now,30), '.txt'],'w');
fprintf(fn,'%9s','year','month','day','hour','altitude','azimuth','Kdirect','Kdiffuse','Kglobal','Kdown','Kup',...
    'Knorth','Keast','Ksouth','Kwest','Ldown','Lup','Lnorth','Least','Lsouth','Lwest','Ta',...
    'RH','Ea','Esky','Sstr','Tmrt','I0','PET','Ws','sh','UTCI');
fprintf(fn,'\r\n');

%Constants
svfE=svf;svfW=svf;svfN=svf;svfS=svf;

%Instrument offset in degrees
t=0;
aziE=azimuth+t;aziS=azimuth-90+t;aziW=azimuth-180+t;aziN=azimuth-270+t;

%Stefan Bolzmans Constant
SBC=5.67051e-8;

%Wall fraction from svf
svfviktE=26.163*svfE.^5-53.292*svfE.^4+39.328*svfE.^3-9.7129*svfE.^2+1.9765*svfE-0.0456;
svfviktS=26.163*svfS.^5-53.292*svfS.^4+39.328*svfS.^3-9.7129*svfS.^2+1.9765*svfS-0.0456;
svfviktW=26.163*svfW.^5-53.292*svfW.^4+39.328*svfW.^3-9.7129*svfW.^2+1.9765*svfW-0.0456;
svfviktN=26.163*svfN.^5-53.292*svfN.^4+39.328*svfN.^3-9.7129*svfN.^2+1.9765*svfN-0.0456;

%Building height angle from svf
svfalfa=asin(exp((log(1-svf))/2));
svfalfaE=asin(exp((log(1-svfE))/2));
svfalfaS=asin(exp((log(1-svfS))/2));
svfalfaW=asin(exp((log(1-svfW))/2));
svfalfaN=asin(exp((log(1-svfN))/2));

%Creating vectors from meteorological input
year=met(:,1);month=met(:,2);day=met(:,3);hour=met(:,4);%min=met(:,5);
Ta=met(:,5);RH=met(:,6);radG=met(:,7);radD=met(:,8);radI=met(:,9);
Ws=met(:,10);
%octas=met(:,11);Ws=met(:,12);Wd=met(:,13);
daytime=find(altitude>0);daytime=length(daytime); %Number of daytime hours

%Parameterisarion for Lup
if isempty(height),height=1.1;else end
first=round(height); %Radiative surface influence, Rule of thumb by Schmid et al. (1990).
if first==0, first=1; end
second=round(height*20);
azimuthA=0:20:359; %Search directions for Ground View Factors (GVF)

%% Main loop
for i=1:length(altitude)
    
    progressbar(i/length(altitude),0);
    
    
    %Surface temperature parameterization
    dfm=abs(172-jday(i)); %Day from midsommer
    Tstart=3.41; %
    Tgamp=0.000006*dfm^3-0.0017*dfm^2+0.0127*dfm+17.084+Tstart; %sinus function for daily surface temperature wave
    doi=jday;
    doi=doi==jday(i);
    alt=altitude(doi);
    if datafile==0 && alt<0
        rise=1;
    else
        rise=find(alt>0);
        rise=rise(1); %hour of sunrise
    end
    
    %Vapor pressure
    ea=6.107*10^((7.5*Ta(i))/(237.3+Ta(i)))*(RH(i)/100);
    
    %Determination of clear-sky emissivity from Prata (1996)
    msteg=46.5*(ea/(Ta(i)+273.15));
    esky=(1-(1+msteg)*exp(-((1.2+3.0*msteg)^0.5)))-0.04;
    
    %%%%%%%%%%%%%%%%%%%% DAYTIME CALCULATIONS %%%%%%%%%%%%%%%%%%%
    if altitude(i)>0
        
        %Clearness Index on Earth's surface after Crawford and Dunchon (1999) with a correction
        %factor for low sun elevations after Lindberg et al. (2009)
        [I0 CI Kt I0et]=clearnessindex(zen(1,i),jday(1,i),Ta(i),RH(i)/100,radG(i),location);
        if CI>1 && CI<inf,CI=1;end
        
        %Estimation of radD and radI if not measured after Reindl et al. (1990)
        if onlyglobal == 1
            [radI(i) radD(i)]=diffusefraction(radG(i),altitude(i),Kt,Ta(i),RH(i));
        end
        
        %Ground View Factors based on shadowpatterns and sunlit walls
        gvf=sh(i);
        
        %Building height angle from svf
        F_sh=cylindric_wedge(zen(i),svfalfa);%Fraction shadow on building walls based on sun altitude and  global svf
        F_sh(isnan(F_sh))=0.5;
        
        %Daytime short wave radiaiton fluxes, V 2.2
        Kdown=sh(i)*radI(i)*sin(altitude(i)*(pi/180))+svf*radD(i)+...
            (1-svf)*radG(i)*albedo.*(1-F_sh);%*sin(altitude(i)*(pi/180));
        Kup=albedo*(gvf*radI(i)*sin(altitude(i)*(pi/180))+svf*radD(i)+...
            (1-svf)*radG(i)*albedo.*(1-F_sh));%*sin(altitude(i)*(pi/180));
        
        if azimuth(i) > (360-t)  ||  azimuth(i) <= (180-t)
            Keast=sh(i)*radI(i)*cos(altitude(i)*(pi/180))*sin(aziE(i)*(pi/180))+...
                (svfviktE/4.417)*radD(i)+((4.417-svfviktE)/4.417).*radG(i)*albedo.*(1-F_sh);%*sin(altitude(i)*(pi/180));
        else
            Keast=(svfviktE/4.417)*radD(i)+((4.417-svfviktE)/4.417).*radG(i)*albedo.*(1-F_sh);%*sin(altitude(i)*(pi/180));
        end
        if azimuth(i) > (90-t)  &&  azimuth(i) <= (270-t)
            Ksouth=sh(i)*radI(i)*cos(altitude(i)*(pi/180))*sin(aziS(i)*(pi/180))+...
                (svfviktS/4.417)*radD(i)+((4.417-svfviktS)/4.417).*radG(i)*albedo.*(1-F_sh);%*sin(altitude(i)*(pi/180));
        else
            Ksouth=(svfviktS/4.417)*radD(i)+((4.417-svfviktS)/4.417).*radG(i)*albedo.*(1-F_sh);%*sin(altitude(i)*(pi/180));
        end
        if azimuth(i) > (180-t)  &&  azimuth(i) <= (360-t)
            Kwest=sh(i)*radI(i)*cos(altitude(i)*(pi/180))*sin(aziW(i)*(pi/180))+...
                (svfviktW/4.417)*radD(i)+((4.417-svfviktW)/4.417).*radG(i)*albedo.*(1-F_sh);%*sin(altitude(i)*(pi/180));
        else
            Kwest=(svfviktW/4.417)*radD(i)+((4.417-svfviktW)/4.417).*radG(i)*albedo.*(1-F_sh);%*sin(altitude(i)*(pi/180));
        end
        if azimuth(i) <= (90-t)  ||  azimuth(i) > (270-t)
            Knorth=sh(i)*radI(i)*cos(altitude(i)*(pi/180))*sin(aziN(i)*(pi/180))+...
                (svfviktN/4.417)*radD(i)+((4.417-svfviktN)/4.417).*radG(i)*albedo.*(1-F_sh);%*sin(altitude(i)*(pi/180));
        else
            Knorth=(svfviktN/4.417)*radD(i)+((4.417-svfviktN)/4.417).*radG(i)*albedo.*(1-F_sh);%*sin(altitude(i)*(pi/180));
        end
        
        %Calculation of longwave daytime radiative fluxes
        
        %Surface temperatures parameterisation
        Tg=Tgamp*sin((((hour(i)+1)-rise)/(14-rise))*pi/2)-Tstart;
        if Tg<0
            Tg=0;
        end
        %New estimation of Tg reduction for non-clear situation based on Reindl et al. 1990
        [radI0 radDmod]=diffusefraction(I0,altitude(i),1,Ta(i),RH(i));
        corr=0.1473*log(90-(zen(i)/pi*180))+0.3454;% 20070329 temporary correction of latitude from Lindberg et al. 2009
        CI_Tg=(radI(i)/radI0)+(1-corr);
        if CI_Tg>1 && CI_Tg<inf ,CI_Tg=1;end
        Tg=Tg*CI_Tg;
        Tw=Tg;
        
        %Ldown
        Ldown=svf*esky*SBC*((Ta(i)+273.15)^4)+(1-svf)*ewall*SBC*((Ta(i)+273.15+Tw)^4)+...
            (1-svf)*(1-ewall)*esky*SBC*((Ta(i)+273.15)^4); %Jonsson et al. (2006)
        Ldown=Ldown-25;% Shown by Jonsson et al. (2006) and Duarte et al. (2006)

        if CI < 0.95  %non-clear conditions
            c=1-CI;
            Ldown=Ldown*(1-c)+c*SBC*((Ta(i)+273.15)^4);
        end
        
        %Lup
        Lup=SBC*eground*((gvf*Tg+Ta(i)+273.15).^4);
        
        %Lside
        [Least,Lsouth,Lwest,Lnorth]=Lside(svfviktE,svfviktS,svfviktW,svfviktN,svfalfa,svfalfaE,svfalfaS,svfalfaW,svfalfaN,azimuth(i),altitude(i),zen(i),Ta(i),Tw,SBC,ewall,Ldown,Lup,esky,svfE,svfS,svfN,svfW,t);
        
        %Calculation of radiant flux density and Tmrt during daytime
        Sstr=absK*(Kdown*Fup+Kup*Fup+Knorth*Fside+Keast*Fside+Ksouth*Fside+Kwest*Fside)...
            +absL*(Ldown*Fup+Lup*Fup+Lnorth*Fside+Least*Fside+Lsouth*Fside+Lwest*Fside);
        Tmrt=sqrt(sqrt((Sstr/(absL*SBC))))-273.2;
        
        %%%%%%%%%%%%%%%%%% NIGHTTIME CALCULATIONS %%%%%%%%%%%%%%%%%%%%%%
    else
        %Nocturnal cloudfraction from Offerle et al. 2003
        if hour(i)==0
            dfm=abs(172-jday(i)); %Day from midsommer
            Tstart=3.41; %
            Tgamp=0.000006*dfm^3-0.0017*dfm^2+0.0127*dfm+17.084+Tstart; %sinus function for daily surface temperature wave
            doi=jday;
            doi=doi==jday(i);
            alt=altitude(doi);
            rise=find(alt>0);
            rise=rise(1); %hour of sunrise
            [I0 CI Kt I0et]=clearnessindex(zen(1,i+rise-1),jday(1,i+rise-1),Ta(i+rise-1),RH(i+rise-1)/100,radG(i+rise-1),location);
            if CI>1 && CI<inf,CI=1;end
        end
        if datafile==0 && altitude<0
            CI=1;
        end
        Tw=0;Tg=Ta(i);CI_Tg=CI;F_sh=[];%  Tw=0; 20130205
        
        %Nocturnal Kfluxes set to 0
        Kdown=0;Kwest=0;Kup=0;Keast=0;Ksouth=0;Knorth=0;%sh=0;
        
        %Ldown
        Ldown=svf*esky*SBC*((Ta(i)+273.15)^4)+(1-svf)*ewall*SBC*((Ta(i)+273.15+Tw)^4)+...
            (1-svf)*(1-ewall)*esky*SBC*((Ta(i)+273.15)^4); %Jonsson et al (2006)
        Ldown=Ldown-25;% Shown by Jonsson et al 2006 and Duarte et al 2006
        
        if CI < 0.95  %non-clear conditions
            c=1-CI;
            Ldown=Ldown*(1-c)+c*SBC*((Ta(i)+273.15)^4);
        end
        
        %Lup
        Lup=SBC*eground*((Tg+273.15).^4);
        
        %Lside
        [Least,Lsouth,Lwest,Lnorth]=Lside(svfviktE,svfviktS,svfviktW,svfviktN,svfalfa,svfalfaE,svfalfaS,svfalfaW,svfalfaN,azimuth(i),altitude(i),zen(i),Ta(i),Tw,SBC,ewall,Ldown,Lup,esky,svfE,svfS,svfN,svfW,t);
        
        %Calculation of nocturnal radiant flux density and Tmrt
        Sstr=Ldown*Fup*absL+Lup*Fup*absL+Lnorth*Fside*absL+Least*Fside*absL+Lsouth*Fside*absL+Lwest*Fside*absL;
        Tmrt=sqrt(sqrt((Sstr/(absL*SBC))))-273.2;
        
        Tg=Tgamp*sin((((hour(i)+1)-rise)/(14-rise))*pi/2)-Tstart;
        I0=0;gvf=1;
        
    end
    petout=petcalculator3(Ta(i),RH(i),Tmrt,Ws(i),pet);
    [UTCI_approx]=utci_calculator_solweigpoint(Ta(i),RH(i),Tmrt,Ws(i)/0.5);%0.5 for 10magl Ws
    fprintf(fn,'%9.3f',year(i),month(i),day(i),hour(i),altitude(i),azimuth(i),radI(i),radD(i)...
            ,radG(i), Kdown, Kup, Knorth, Keast, Ksouth,Kwest,...
            Ldown, Lup, Lnorth, Least, Lsouth, Lwest,Ta(i),RH(i),...
            ea,esky,Sstr,Tmrt,I0,petout,Ws(i),sh(i),UTCI_approx);
     fprintf(fn,'\r\n');
    data(i,:)=[Tmrt petout UTCI_approx radD(i) radI(i)];
end
fclose(fn);
% Close the progressbar
progressbar(1,0);


