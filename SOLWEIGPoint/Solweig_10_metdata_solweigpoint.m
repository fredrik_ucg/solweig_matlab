function [altitude,azimuth,zen,jday]=Solweig_10_metdata_solweigpoint(met,location)
% This function is used to process the input meteorological file. 
% It also calculates Sun position based on the time specified in the met-file

% Newdata1=importdata(inputdata,'\t',1);
% met=Newdata1.data;
% met_header=Newdata1.textdata;
if size(met,2)==11
    time.min=met(11);
else
    time.min=30;
end
time.sec=0;%UTC=+1 and min=30 (sunposition one halfhour before metdata)
time.UTC=location.utc;

for i=1:length(met(:,1))
    time.year=met(i,1);
    time.month=met(i,2);
    time.day=met(i,3);
    time.hour=met(i,4);
    sun=sun_position(time,location);
    altitude(1,i)=90-sun.zenith; azimuth(1,i)=sun.azimuth; zen(1,i)=sun.zenith*(pi/180);
    %day of year and check for leap year
    A=logical(mod(time.year,4));
    B=logical(mod(time.year,100));
    C=logical(mod(time.year,400));
    leapyear=ismember(time.year,time.year(~C | (~A & B)));
    if leapyear==1
        dayspermonth=[31 29 31 30 31 30 31 31 30 31 30 31];
    else
        dayspermonth=[31 28 31 30 31 30 31 31 30 31 30 31];
    end
    jday(1,i)=sum(dayspermonth(1:time.month-1))+time.day;
end
