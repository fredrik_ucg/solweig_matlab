function [ sh ,xx,yy]= shadowinfo_hemispherical(azimuth,zenith,data,centerX,centerY,radie)
%shadowinfo_hemispherical looks if direct sunlight is present on a
%hemispherical photograph based on sun position

% Setup
dag=(zenith<=pi/2);
rmax=round(radie);
theta=azimuth/(180/pi);%radians
rho=zenith*180/pi;%degrees

% transform data to Cartesian coordinates.
for i=1:size(theta,2)
    if dag(i)==1
        yy = (rho(i).*cos(theta(i)));
        xx = (rho(i).*sin(theta(i)));
        yy = centerY+rmax-(90+yy)*rmax/90;
        xx = (90+xx)*rmax/90+centerX-rmax;
        % plot data on top of grid
        % if strcmp(line_style,'auto')        
        sh(i,1)=data(floor(yy),floor(xx));
    else
        sh(i,1)=0;
        if size(dag,2)==1
            xx=[];
            yy=[];
        end
    end
end
