function [data]=TmrtPET_calculator_solweigpoint_2014b(albedo_b,albedo_g,absK,absL,ewall,eground,Fside,Fup,met,altitude,azimuth,zen,jday,onlyglobal,location,height,svf,pet,sh,datafile,sensorheight,YYYY)

% This mfile calculates Tmrt and PET for one sunlit point using SOLWEIG equations

%Initialization output textfile
userdir=getuserdir;
fn=fopen([userdir, filesep, 'OutputData_SOLWEIGpoint_Calculator_', datestr(now,30), '.txt'],'w');
fprintf(fn,'%9s','year','DOY','hour','dectime','altitude','azimuth','Kdirect','Kdiffuse','Kglobal','Kdirside','Kdown','Kup',...
    'Knorth','Keast','Ksouth','Kwest','Ldown','Lup','Lnorth','Least','Lsouth','Lwest','Ta','Tg',...
    'RH','Ea','Esky','Sstr','Tmrt','I0','PET','WsPET','sh','UTCI');
fprintf(fn,'\r\n');

%Constants
svfE=svf;svfW=svf;svfN=svf;svfS=svf;
svfEveg=1;svfSveg=1;svfWveg=1;svfNveg=1;svfEaveg=1;svfSaveg=1;svfWaveg=1;svfNaveg=1;

%Instrument offset in degrees
t=0;
aziE=azimuth+t;aziS=azimuth-90+t;aziW=azimuth-180+t;aziN=azimuth-270+t;

%Stefan Bolzmans Constant
SBC=5.67051e-8;

%Surface temperature difference at sunrise
Tstart=3.41;

%Wall fraction from svf
svfviktE=26.163*svfE.^5-53.292*svfE.^4+39.328*svfE.^3-9.7129*svfE.^2+1.9765*svfE-0.0456;
svfviktS=26.163*svfS.^5-53.292*svfS.^4+39.328*svfS.^3-9.7129*svfS.^2+1.9765*svfS-0.0456;
svfviktW=26.163*svfW.^5-53.292*svfW.^4+39.328*svfW.^3-9.7129*svfW.^2+1.9765*svfW-0.0456;
svfviktN=26.163*svfN.^5-53.292*svfN.^4+39.328*svfN.^3-9.7129*svfN.^2+1.9765*svfN-0.0456;

%Building height angle from svf
svfalfa=asin(exp((log(1-svf))/2));
svfalfaE=asin(exp((log(1-svfE))/2));
svfalfaS=asin(exp((log(1-svfS))/2));
svfalfaW=asin(exp((log(1-svfW))/2));
svfalfaN=asin(exp((log(1-svfN))/2));

%Creating vectors from meteorological input as from SEUWS 2014a
DOY=met(:,1);hour=met(:,2);dectime=met(:,3);Ta=met(:,11);RH=met(:,10);
radG=met(:,14);radD=met(:,21);radI=met(:,22);P=met(:,12);Ws_uncorr=met(:,9);

%Recalculating wind speed based on powerlaw
WsPET=(1.1/sensorheight)^0.14*Ws_uncorr;
WsUTCI=(10/sensorheight)^0.14*Ws_uncorr;

%% Main loop
for i=1:length(altitude)
    
    progressbar(i/length(altitude),0);
    
    % Find sunrise decimal hour - new in 2014a
    [ ~, ~, ~, SNUP ] = daylen( jday(i), location.latitude );
    
    % If metfile starts at night
    CI=1;    
    
    %Vapor pressure
    ea=6.107*10^((7.5*Ta(i))/(237.3+Ta(i)))*(RH(i)/100);
    
    %Determination of clear-sky emissivity from Prata (1996)
    msteg=46.5*(ea/(Ta(i)+273.15));
    esky=(1-(1+msteg)*exp(-((1.2+3.0*msteg)^0.5)));%-0.04
    
    %%%%%%%%%%%%%%%%%%%% DAYTIME CALCULATIONS %%%%%%%%%%%%%%%%%%%
    if altitude(i)>0

        %Clearness Index on Earth's surface after Crawford and Dunchon (1999) with a correction
        %factor for low sun elevations after Lindberg et al. (2009)
        [I0 CI Kt]=clearnessindex_2013b(zen(1,i),jday(1,i),Ta(i),RH(i)/100,radG(i),location,P(i));
        if CI>1 && CI<inf,CI=1;end
        
        %Estimation of radD and radI if not measured after Reindl et al. (1990)
        if onlyglobal == 1
            [radI(i) radD(i)]=diffusefraction(radG(i),altitude(i),Kt,Ta(i),RH(i));
        end
        
        %Ground View Factors based on shadowpatterns
        gvf=sh(i);
        
        %Building height angle from svf
        F_sh=cylindric_wedge(zen(i),svfalfa);%Fraction shadow on building walls based on sun altitude and  global svf
        F_sh(isnan(F_sh))=0.5;
        
        %Daytime short wave radiaiton fluxes, v2014b, cylinder
        Kdirside=sh(i)*radI(i)*0.28*cos(altitude(i)*(pi/180));
        
        %*abs(cos(azimuth(i)*(pi/180)))+abs(cos(pi/2-azimuth(i)*(pi/180)))...
        %    *(0.28*cos(altitude(i)*(pi/180))+0.06*(altitude(i)*(pi/180)));        
        
        Kdown=sh(i)*radI(i)*sin(altitude(i)*(pi/180))+svf*radD(i)+(1-svf)*radG(i)*albedo_b.*(1-F_sh);
        %*sin(altitude(i)*(pi/180));
                
        Kup=albedo_g*(gvf*radI(i)*sin(altitude(i)*(pi/180))+svf*radD(i)+...%VAD H�NDER MED DENNA?
            (1-svf)*radG(i)*albedo_b.*(1-F_sh));%*sin(altitude(i)*(pi/180));
        
%         if azimuth(i) > (360-t)  ||  azimuth(i) <= (180-t)
%             Keast=sh(i)*radI(i)*cos(altitude(i)*(pi/180))*sin(aziE(i)*(pi/180))+...
%                 (svfviktE/4.417)*radD(i)+((4.417-svfviktE)/4.417).*radG(i)*albedo_b.*(1-F_sh);%*sin(altitude(i)*(pi/180));
%         else
            Keast=(svfviktE/4.417)*radD(i)+((4.417-svfviktE)/4.417).*radG(i)*albedo_b.*(1-F_sh);%*sin(altitude(i)*(pi/180));
%         end
%         if azimuth(i) > (90-t)  &&  azimuth(i) <= (270-t)
%             Ksouth=sh(i)*radI(i)*cos(altitude(i)*(pi/180))*sin(aziS(i)*(pi/180))+...
%                 (svfviktS/4.417)*radD(i)+((4.417-svfviktS)/4.417).*radG(i)*albedo_b.*(1-F_sh);%*sin(altitude(i)*(pi/180));
%         else
            Ksouth=(svfviktS/4.417)*radD(i)+((4.417-svfviktS)/4.417).*radG(i)*albedo_b.*(1-F_sh);%*sin(altitude(i)*(pi/180));
%         end
%         if azimuth(i) > (180-t)  &&  azimuth(i) <= (360-t)
%             Kwest=sh(i)*radI(i)*cos(altitude(i)*(pi/180))*sin(aziW(i)*(pi/180))+...
%                 (svfviktW/4.417)*radD(i)+((4.417-svfviktW)/4.417).*radG(i)*albedo_b.*(1-F_sh);%*sin(altitude(i)*(pi/180));
%         else
            Kwest=(svfviktW/4.417)*radD(i)+((4.417-svfviktW)/4.417).*radG(i)*albedo_b.*(1-F_sh);%*sin(altitude(i)*(pi/180));
%         end
%         if azimuth(i) <= (90-t)  ||  azimuth(i) > (270-t)
%             Knorth=sh(i)*radI(i)*cos(altitude(i)*(pi/180))*sin(aziN(i)*(pi/180))+...
%                 (svfviktN/4.417)*radD(i)+((4.417-svfviktN)/4.417).*radG(i)*albedo_b.*(1-F_sh);%*sin(altitude(i)*(pi/180));
%         else
            Knorth=(svfviktN/4.417)*radD(i)+((4.417-svfviktN)/4.417).*radG(i)*albedo_b.*(1-F_sh);%*sin(altitude(i)*(pi/180));
%         end
        
        %Calculation of longwave daytime radiative fluxes
        
        %Surface temperatures parameterisation
        dfm=abs(172-jday(i)); %Day from midsommer
%         Tstart=3.41; %
        Tgamp=0.000006*dfm^3-0.0017*dfm^2+0.0127*dfm+17.084+Tstart; %sinus function for daily surface temperature wave
%         Tg=Tgamp*sin((((hour(i)+1)-rise)/(14-rise))*pi/2)-Tstart;
        Tg=Tgamp*sin((((dectime(i)-floor(dectime(i)))-SNUP/24)/(15/24-SNUP/24))*pi/2)-Tstart; %new sunrise time 2014a

        if Tg<0 % temporary for removing low Tg during morning 20130205
            Tg=0;
        end
        %New estimation of Tg reduction for non-clear situation based on Reindl et al. 1990
        [radI0]=diffusefraction(I0,altitude(i),1,Ta(i),RH(i));
        corr=0.1473*log(90-(zen(i)/pi*180))+0.3454;% 20070329 temporary correction of latitude from Lindberg et al. 2009
        CI_Tg=(radI(i)/radI0)+(1-corr);
        if CI_Tg>1 && CI_Tg<inf ,CI_Tg=1;end
        Tg=Tg*CI_Tg;
        Tw=Tg;
        
        %Ldown
        Ldown=svf*esky*SBC*((Ta(i)+273.15)^4)+(1-svf)*ewall*SBC*((Ta(i)+273.15+Tw)^4)+...
            (1-svf)*(1-ewall)*esky*SBC*((Ta(i)+273.15)^4); %Jonsson et al. (2006)
        %Ldown=Ldown-25;% Shown by Jonsson et al. (2006) and Duarte et al. (2006)

        if CI < 0.95  %non-clear conditions
            c=1-CI;
            Ldown=Ldown*(1-c)+c*SBC*((Ta(i)+273.15)^4);
        end
        
        %Lup
        Lup=SBC*eground*((gvf*Tg+Ta(i)+273.15).^4);
        
        %Lside
%         [Least,Lsouth,Lwest,Lnorth]=Lside(svfviktE,svfviktS,svfviktW,svfviktN,svfalfa,svfalfaE,svfalfaS,svfalfaW,svfalfaN,azimuth(i),altitude(i),zen(i),Ta(i),Tw,SBC,ewall,Ldown,Lup,esky,svfE,svfS,svfN,svfW,t);
        [Least,Lsouth,Lwest,Lnorth]=Lside_veg_v2(svfS,svfW,svfN,svfE,...
            svfEveg,svfSveg,svfWveg,svfNveg,svfEaveg,svfSaveg,svfWaveg,svfNaveg,...
            azimuth(i),altitude(i),Ta(i),Tw,SBC,ewall,Ldown,Lup,esky,t,F_sh);

        %%%%%%%%%%%%%%%%%% NIGHTTIME CALCULATIONS %%%%%%%%%%%%%%%%%%%%%%
    else
        %Nocturnal cloudfraction from Offerle et al. 2003
        if (dectime(i)-floor(dectime(i)))==0
            alt=altitude(1,i:size(altitude,2));
            alt=find(alt>1);
            rise=alt(1);
            [~, CI , ~]=clearnessindex_2013b(zen(1,i+rise),jday(1,i+rise),Ta(i+rise),RH(i+rise)/100,radG(i+rise),location,P(i));
            if CI>1 && CI<inf,CI=1;end
        end
        if datafile==0 && altitude<0
            CI=1;
        end
        Tw=0;Tg=0;CI_Tg=CI;F_sh=[];%  Tw=0; 20130205
        
        %Nocturnal Kfluxes set to 0
        Kdown=0;Kwest=0;Kup=0;Keast=0;Ksouth=0;Knorth=0;%sh=0;
        Kdirside=0;
        %Ldown
        Ldown=svf*esky*SBC*((Ta(i)+273.15)^4)+(1-svf)*ewall*SBC*((Ta(i)+273.15+Tw)^4)+...
            (1-svf)*(1-ewall)*esky*SBC*((Ta(i)+273.15)^4); %Jonsson et al (2006)
        Ldown=Ldown-25;% Shown by Jonsson et al 2006 and Duarte et al 2006
        
        if CI < 0.95  %non-clear conditions
            c=1-CI;
            Ldown=Ldown*(1-c)+c*SBC*((Ta(i)+273.15)^4);
        end
        
        %Lup
        Lup=SBC*eground*((Ta(i)+273.15).^4);
        
        %Lside
        [Least,Lsouth,Lwest,Lnorth]=Lside_veg_v2(svfS,svfW,svfN,svfE,...
            svfEveg,svfSveg,svfWveg,svfNveg,svfEaveg,svfSaveg,svfWaveg,svfNaveg,...
            azimuth(i),altitude(i),Ta(i),Tw,SBC,ewall,Ldown,Lup,esky,t,F_sh);

        I0=0;%gvf=1;
        
    end
    
            %Calculation of radiant flux density and Tmrt during daytime
        Sstr=absK*(Kdirside+Kdown*Fup+Kup*Fup+Knorth*Fside+Keast*Fside+Ksouth*Fside+Kwest*Fside)...
            +absL*(Ldown*Fup+Lup*Fup+Lnorth*Fside+Least*Fside+Lsouth*Fside+Lwest*Fside);
        Tmrt=sqrt(sqrt((Sstr/(absL*SBC))))-273.2;
 
    petout=petcalculator3(Ta(i),RH(i),Tmrt,WsPET(i),pet);
    [UTCI_approx]=utci_calculator_solweigpoint(Ta(i),RH(i),Tmrt,WsUTCI(i));
    fprintf(fn,'%9.3f',YYYY(i),DOY(i),hour(i),dectime(i),altitude(i),azimuth(i),radI(i),radD(i)...
            ,radG(i),Kdirside, Kdown, Kup, Knorth, Keast, Ksouth,Kwest,...
            Ldown, Lup, Lnorth, Least, Lsouth, Lwest,Ta(i),Ta(i)+Tg,RH(i),...
            ea,esky,Sstr,Tmrt,I0,petout,WsPET(i),sh(i),UTCI_approx);
     fprintf(fn,'\r\n');
    data(i,:)=[Tmrt petout UTCI_approx radD(i) radI(i)];
end
fclose(fn);
% Close the progressbar
progressbar(1,0);


