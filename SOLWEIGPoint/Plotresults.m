%plotresult

data=importdata('OutputData_SOLWEIGpoint_Calculator_observations.txt');
dataval=data.data;
% figure
% plot(dataval(:,[9 28]))

figure
plot(dataval(:,27))
hold on

data=importdata('OutputData_SOLWEIGpoint_Calculator_modelled.txt');
datamod=data.data;

plot(datamod(:,27),'r')

figure
plot(dataval(:,22))
hold on
plot(datamod(:,22),'r')
