function [ h ] = SunpathdiagramOnPhoto_Solweigpoint( location, dates, timestep, data ,centerX,centerY,radie,jday)
%SUNPATHDIAGRAM plots sun path diagram for given latitude at various times
%of year.
%function [ h ] = sunpathdiagram( lat, dates, timestep )
%           Calls solarangles at location (degrees)
%           for dates (day of year)
%           at a timestep in hours (default=0.5)
%           data is the image
% lat=57;
% Setup
% UTC=input('UTC: ');
UTC=location.UTC;
dates=datenum(jday)+1-datenum(dates(1),1,1);

dates=floor(dates);
dates=reshape(dates,[],1);
dlab=strcat(datestr(dates,'mmm'),datestr(dates,'dd'));%datelabes
hour=(0:timestep:24)';
hlab=datestr(dates+hour/24,'HH:MM');%hourlabels
% hlab=num2str(hour,'%5.2f');

for i=1:length(dates)
    
    % Polarplotting
%     tdoy=dates(i)+hour/24.; %decimaltime
%     [ decl azim zen ] = solarangles( lat, tdoy );
    
    timestruct=datevec(jday+hour/24);
    for j=1:length(timestruct)
        time.year = timestruct(j,1);
        time.month = timestruct(j,2);
        time.day = timestruct(j,3);
        time.hour = timestruct(j,4);
        time.min = timestruct(j,5);
        time.sec = timestruct(j,6);
        time.UTC = UTC;
        sun = sun_position(time, location);
        azimuth(j,:)=sun.azimuth/(180/pi);
        zenith(j,:)=sun.zenith/(180/pi);
%         zenith(j,:)=sun.zenith;
    end
%      figure
% plot(zen)
% hold on
% plot(zenith,'r')
    day=find(zenith<=pi/2);
    
    % Setting out text
    maxday=max(day);
%     minday=min(day);
%     if dates(i)<365/2
        h=sunpathpolarWithPhoto_v3(azimuth(day),zenith(day)*180/pi,'g.-',data,centerX,centerY,radie);
        [tx ty]=pol2cart(azimuth(maxday),zenith(maxday)*180/pi);
        t1=text(centerX+ty*radie/90-radie/90,centerY-tx*radie/90,dlab(i,:),...
            'VerticalAlignment','middle',...
            'HorizontalAlignment','right','FontWeight','bold','Color','g');
%     else
%         h=sunpathpolarWithPhoto_v2(azim(day),zen(day)*180/pi,'b.-',data,centerX,centerY,radie);
        %         [tx ty]=pol2cart(azim(minday),zen(minday)*180/pi);
        %         text(tx,ty+5,dlab(i,:),'VerticalAlignment','middle',...
        %             'HorizontalAlignment','left','FontWeight','bold','Color','r');
%     end
    
%     if i==1||i==length(dates) %label hours for first and last dates only.
        for j=1:length(day)
            [tx ty]=pol2cart(azimuth(day(j)),zenith(day(j))*180/pi);
            t2=text(centerX+ty*radie/90,centerY-tx*radie/90-radie/90,hlab(day(j),:),...
                'VerticalAlignment','middle','FontSize',6,'FontWeight','bold','Color','g');
%             ,...
%                 'Rotation',90-azim(j)/pi);
        end
%     end
    
    hold on;
end

% title(['Solar Position Plot (Azimuth, Zenith) for Latitude ', num2str(lat)],...
%     'Position', [100 0],'FontWeight','bold')
% hold off;

