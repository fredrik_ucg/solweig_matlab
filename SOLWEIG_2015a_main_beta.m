%% SOLWEIG 2015a
% This m.file is the main script for the SOLWEIG model
% The desigh of this m-file originates from the interface using
% JA builder and Matlab Compiler
%
% This is a new version (2015a) for the SOLWEIG model
% This vesion includes land cover (grass, impervious and water)
%
% Fredrik Lindberg, G�teborg Urban Climate Group
% fredrikl@gvc.gu.se

%% Setup
clear
%Input DigitalSurfaceModel (DSM)
dsmfile=['m:' filesep 'repository' filesep 'solweig_matlab' filesep 'inputdata' filesep 'DSM_KRbig_v3.asc'];

% SkyViewFactor files
svfname='krbig_v3';
svffiles={['m:' filesep 'SOLWEIG' filesep 'SVFs' filesep 'def_', svfname ,'_svf.asc'];
          ['m:' filesep 'SOLWEIG' filesep 'SVFs' filesep 'def_',svfname,'_svfE.asc']; 
          ['m:' filesep 'SOLWEIG' filesep 'SVFs' filesep 'def_',svfname,'_svfS.asc'];
          ['m:' filesep 'SOLWEIG' filesep 'SVFs' filesep 'def_',svfname,'_svfW.asc']; 
          ['m:' filesep 'SOLWEIG' filesep 'SVFs' filesep 'def_',svfname,'_svfN.asc']};
svffilesveg={['m:' filesep 'SOLWEIG' filesep 'SVFs' filesep 'veg_',svfname,'_svf.asc'];
             ['m:' filesep 'SOLWEIG' filesep 'SVFs' filesep 'veg_',svfname,'_svfE.asc']; 
             ['m:' filesep 'SOLWEIG' filesep 'SVFs' filesep 'veg_',svfname,'_svfS.asc'];
             ['m:' filesep 'SOLWEIG' filesep 'SVFs' filesep 'veg_',svfname,'_svfW.asc']; 
             ['m:' filesep 'SOLWEIG' filesep 'SVFs' filesep 'veg_',svfname,'_svfN.asc'];
             ['m:' filesep 'SOLWEIG' filesep 'SVFs' filesep 'vega_',svfname,'_svf.asc'];
             ['m:' filesep 'SOLWEIG' filesep 'SVFs' filesep 'vega_',svfname,'_svfE.asc']; 
             ['m:' filesep 'SOLWEIG' filesep 'SVFs' filesep 'vega_',svfname,'_svfS.asc'];
             ['m:' filesep 'SOLWEIG' filesep 'SVFs' filesep 'vega_',svfname,'_svfW.asc']; 
             ['m:' filesep 'SOLWEIG' filesep 'SVFs' filesep 'vega_',svfname,'_svfN.asc']};

%Meteorological data
metdatafolder=['M:' filesep 'repository' filesep 'solweig_matlab' filesep 'inputdata' filesep ];
metdatafile='gbg20060726_2015a.txt';

% Outputfolder
coreoutputfolder=['m:' filesep 'SOLWEIG_2015a_runs' filesep 'SOLWEIGOutput_beta' filesep ];

% Location to be modelled
cityname='Goteborg';

% Vegetation file
vegetationfile=['m:' filesep 'repository' filesep 'solweig_matlab' filesep 'inputdata' filesep 'krbig_buildings.asc'];

%vegetation grids
cdsmfile=['m:' filesep 'repository' filesep 'solweig_matlab' filesep 'inputdata' filesep 'CDSM_KRbig_v3.asc'];
tdsmfile=['m:' filesep 'repository' filesep 'solweig_matlab' filesep 'inputdata' filesep 'filenamme.asc'];

%land cover grid
landcovergrid=['m:' filesep 'repository' filesep 'solweig_matlab' filesep 'inputdata' filesep 'landcover_KRbig_v3.asc'];
landcoverfile=['m:' filesep 'repository' filesep 'solweig_matlab' filesep 'inputdata' filesep 'landcoverclasses_2015a.txt'];

showimage=1; % Show hourly images of Tmrt during execution
landcover=1; % Use land cover grid for Lup and Kup
sensorheight=2.0; % Height of windsensor for PET/UTCI calc. at POI
cyl=0; % Consider standing man as cylinder
onlyglobal=0; % 1=diffuse and direct components unavailable and modelled with Reindl et al. 1990

usevegdem=1; % Use vegetation scheme, 1=use vegetation dem, 0=don't use vegetation dem
gridfiles=1;% 1=use allready gridded vegetation files, 0=use textfile
oldtextfile=0;% 1=textfile already exists, 0=no textfile exists
onlycdsm=1; % Only canopy vegetation dsm is available
trunkratio=0.23; % Height of trunk zone related to cdsm

%% Loading building and ground data (ESRI ASCIIGrid)
[a,scale,header,headernum,headername,sizey,sizex]=Solweig_10_loaddem(dsmfile);
a(a<0)=0.000000001;

%% Setting location
[location,UTC]=Solweig_10_loc(cityname);

%% Creating/loading vegetqtionDEM
[vegdem,vegdem2,buildings,walls]=Solweig_vegetation_create(usevegdem,gridfiles,oldtextfile,a,sizex,sizey,...
    vegetationfile,scale,cdsmfile,tdsmfile,onlycdsm,trunkratio);

%% Loading building and ground data (ESRI ASCIIGrid)
if landcover == 1
    lc_grid=Solweig_10_loaddem(landcovergrid);
    lc_grid(lc_grid==-9999)=2;

    buildfromlc=1;
    if buildfromlc==1
        buildings=double(lc_grid~=2);
    end
    % Input of landcover parameters
    formatSpec = '%*s%f%f%f%f%f%f%[^\n\r]';
    fileID = fopen(landcoverfile,'r');
    dataArray = textscan(fileID, formatSpec, 'Delimiter', ' ', 'MultipleDelimsAsOne', true, 'HeaderLines' ,1);
    fclose(fileID);
    lc_class = [dataArray{1:end-1}];
else
    lc_grid=[];
    lc_class=[];
end

%% Getting wall azimuth angels
dirwalls = filter1Goodwin_as_aspect_v3(walls,sizex,sizey,scale,a);

%% Setting point of interest 
point=input('Do you want to locate point of interest? (Y or N):  ','s');
if strcmpi(point,'y')
    figurehandler=show_figure([50 100 750 650],'none','Set Point of Interest','none');
    [row,col]=Solweig_20_poi(figurehandler,a,sizex,sizey,vegdem);
    close_figure(figurehandler);
    disp(['Point of interest is set to row= ',num2str(row) ,' col= ',num2str(col)]);
    [a]=set_point_height(a,row,col,1.1);
    height=1.1;
    disp('Heigth of point is set to 1,1 meter above ground level')
else
    row=[];col=[];height=[];
end

%% Loading or generating SVF grids 
param=input('Is SVF model already generated for current DEM? (Y or N):  ','s');
[svf,svfE,svfS,svfW,svfN,svfveg,svfEveg,svfSveg,svfWveg,svfNveg,svfaveg,...
    svfEaveg,svfSaveg,svfWaveg,svfNaveg]=Solweig_2013a_svf(svffiles,svffilesveg,a,scale,header,sizex,sizey,vegdem,vegdem2,param);

%% Parameters and constants 
param=input('Use standard parameterisation? (Y or N):  ','s');
[albedo_b,albedo_g,absK,absL,ewall,eground,Fside,Fup,PA,trans]=Solweig_2014a_param(param);

%% Input of metdata and calculation of sun positions 
[met,met_header,YYYY,altitude,azimuth,zen,jday,leafon,dectime,altmax]=Solweig_2015a_metdata(metdatafolder,metdatafile,location,UTC);

%% Outputfiles and fomats (1=output, 0=no output)
output.tmrthour=1;output.tmrtday=0;output.tmrtdiurn=0;
output.luphour=1;output.lupday=0;output.lupdiurn=0;
output.ldownhour=1;output.ldownday=0;output.ldowndiurn=0;
output.kuphour=1;output.kupday=0;
output.kdownhour=1;output.kdownday=0;
output.gvfhour=1;output.gvfday=0;
output.svf=0;
output.svfveg=0;
output.svfboth=0;
fileformat.asc=1;fileformat.tif=0;

%% SOLWEIG MAIN CORE 
Solweig_2015a_core(coreoutputfolder,a,scale,header,sizey,sizex,row,col,svf,svfN,svfW,svfE,svfS,svfveg,...
    svfNveg,svfEveg,svfSveg,svfWveg,svfaveg,svfEaveg,svfSaveg,svfWaveg,svfNaveg,vegdem,vegdem2,albedo_b,...
    albedo_g,absK,absL,ewall,eground,Fside,Fup,PA,met,YYYY,altitude,azimuth,zen,jday,showimage,usevegdem,...
    onlyglobal,buildings,location,height,trans,output,fileformat,landcover,sensorheight,leafon,lc_grid,...
    lc_class,dectime,altmax,dirwalls,walls,cyl);
