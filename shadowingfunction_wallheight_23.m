function [vegsh,sh,vbshvegsh,wallsh,wallsun,wallshve,facesh,facesun,walls]=shadowingfunction_wallheight_23(a,vegdem,vegdem2,azimuth,altitude,scale,amaxvalue,bush,walls,aspect)
%This m.file calculates shadows on a DSM and shadow height on building
%walls including both buildings and vegetion units.
%
%INPUTS:
%a = DSM
%vegdem = Vegetation canopy DSM (magl)
%vegdem2 = Trunkzone DSM (magl)
%azimuth and altitude = sun position
%scale= scale of DSM (1 meter pixels=1, 2 meter pixels=0.5)
%walls= pixel row 'outside' buildings. will be calculated if empty
%aspect=normal aspect of walls in radians
%
%OUTPUT:
%sh=ground and roof shadow
%vegsh
%vbshvegsh
%wallsh = height of wall that is in shadow
%wallsun = hieght of wall that is in sun
%wallshve,
%facesh,
%facesun,
%walls

%Fredrik Lindberg 2013-08-14
%fredrikl@gvc.gu.se

if isempty(walls)==1
    walls=ordfilt2(a,4,[0 1 0; 1 0 1; 0 1 0]);
    walls=walls-a;
    walls(walls<3)=0;
    sizex=size(a,2);%might be wrong
    sizey=size(a,1);
    dirwalls = filter1Goodwin_as_aspect_v3(walls,sizex,sizey,scale,a);
    aspect=dirwalls*pi/180;
end

% conversion
degrees=pi/180;
azimuth=azimuth*degrees;
altitude=altitude*degrees;

% measure the size of the image
sizex=size(a,1);
sizey=size(a,2);

% initialise parameters
dx=0;
dy=0;
dz=0;

sh=zeros(sizex,sizey);
vbshvegsh=sh;
vegsh=sh;
f=a;
shvoveg=vegdem;% for vegetation shadowvolume
g=sh;
bushplant=bush>1;
wallbol=double(walls>0);
wallbol(wallbol==0)=NaN;

pibyfour=pi/4;
threetimespibyfour=3*pibyfour;
fivetimespibyfour=5*pibyfour;
seventimespibyfour=7*pibyfour;
sinazimuth=sin(azimuth);
cosazimuth=cos(azimuth);
tanazimuth=tan(azimuth);
signsinazimuth=sign(sinazimuth);
signcosazimuth=sign(cosazimuth);
dssin=abs(1/sinazimuth);
dscos=abs(1/cosazimuth);
tanaltitudebyscale=tan(altitude)/scale;
index=1;

% main loop
while ((amaxvalue>=dz) && (abs(dx)<=sizex) && (abs(dy)<=sizey))
    
    if ((pibyfour<=azimuth)&&(azimuth<threetimespibyfour))||((fivetimespibyfour<=azimuth)&&(azimuth<seventimespibyfour))
        dy=signsinazimuth*index;
        dx=-1*signcosazimuth*abs(round(index/tanazimuth));
        ds=dssin;
    else
        dy=signsinazimuth*abs(round(index*tanazimuth));
        dx=-1*signcosazimuth*index;
        ds=dscos;
    end
    
    % note: dx and dy represent absolute values while ds is an incremental value
    dz=ds*index*tanaltitudebyscale;
    tempvegdem(1:sizex,1:sizey)=0;
    tempvegdem2(1:sizex,1:sizey)=0;
    temp(1:sizex,1:sizey)=0;
    
    absdx=abs(dx);
    absdy=abs(dy);
    
    xc1=((dx+absdx)/2)+1;
    xc2=(sizex+(dx-absdx)/2);
    yc1=((dy+absdy)/2)+1;
    yc2=(sizey+(dy-absdy)/2);
    
    xp1=-((dx-absdx)/2)+1;
    xp2=(sizex-(dx+absdx)/2);
    yp1=-((dy-absdy)/2)+1;
    yp2=(sizey-(dy+absdy)/2);
    
    
    tempvegdem(xp1:xp2,yp1:yp2)=vegdem(xc1:xc2,yc1:yc2)-dz;
    tempvegdem2(xp1:xp2,yp1:yp2)=vegdem2(xc1:xc2,yc1:yc2)-dz;
    temp(xp1:xp2,yp1:yp2)=a(xc1:xc2,yc1:yc2)-dz;
    
    f=max(f,temp);
    shvoveg=max(shvoveg,tempvegdem);
    sh(f>a)=1;sh(f<=a)=0; %Moving building shadow
    fabovea=tempvegdem>a; %vegdem above DEM
    gabovea=tempvegdem2>a; %vegdem2 above DEM
    vegsh2=fabovea-gabovea;
    vegsh=max(vegsh,vegsh2);
    vegsh(vegsh.*sh>0)=0;% removing shadows 'behind' buildings
    vbshvegsh=vegsh+vbshvegsh;
    
    % vegsh at high sun altitudes
    if index==1
        firstvegdem=tempvegdem-temp;
        firstvegdem(firstvegdem<=0)=1000;
        vegsh(firstvegdem<dz)=1;
        vegsh=vegsh.*(vegdem2>a);
        vbshvegsh=zeros(sizex,sizey);
    end
    
    % Bush shadow on bush plant
    if max(bush(:))>0 && max(max(fabovea.*bush))>0
        tempbush(1:sizex,1:sizey)=0;
        tempbush(xp1:xp2,yp1:yp2)=bush(xc1:xc2,yc1:yc2)-dz;
        g=max(g,tempbush);
        g=bushplant.*g;
    end
    
%     if index<3 %removing shadowed walls 1
%         tempfirst(1:sizex,1:sizey)=0;
%         tempfirst(xp1:xp2,yp1:yp2)= a(xc1:xc2,yc1:yc2);
%         if index==1 % removing shadowed walls 2
%             tempwalls(1:sizex,1:sizey)=0;
%             tempwalls(xp1:xp2,yp1:yp2)= wallbol(xc1:xc2,yc1:yc2);
%             wallfirst=((tempwalls+wallbol).*wallbol)==2;
%             wallfirstaspect=aspect.*wallbol.*wallfirst;
%             wallfirstaspect(wallfirstaspect==0)=NaN;
%             wallfirstsun=(wallfirstaspect>azimuth-pi/2 & wallfirstaspect<azimuth+pi/2);
%             wallfirstshade=wallfirst-wallfirstsun;
%         end
%     end
    
    index=index+1;
%     imagesc(h),axis image,colorbar
    % Stopping loop if all shadows reached the ground
    %     stopbuild=stopbuild==f;
    %      imagesc(stopbuild),axis image,pause(0.3)
    %     fin=find(stopbuild==0, 1);
    %     stopbuild=f;
    %     stopveg=stopveg==vegsh;
    %     finveg=find(stopveg==0, 1);
    %     stopveg=vegsh;
    %     if isempty(fin) && isempty(finveg)
    %         dz=amaxvalue+9999;
    %     end
    
end

% Removing walls in shadow due to selfshadowing
azilow=azimuth-pi/2;
azihigh=azimuth+pi/2;
if azilow>=0 && azihigh<2*pi % 90 to 270  (SHADOW)
    facesh=double((aspect<azilow | aspect>=azihigh)-wallbol+1);
    facesh(facesh==2)=0;
elseif azilow<0 && azihigh<=2*pi % 0 to 90
    azilow=azilow+2*pi;
    facesh=(aspect>azilow | aspect<=azihigh)*-1+1; %(SHADOW)
elseif azilow>0 && azihigh>=2*pi % 270 to 360
    azihigh=azihigh-2*pi;
    facesh=(aspect>azilow | aspect<=azihigh)*-1+1;%(SHADOW)
end

sh=1-sh;
vbshvegsh(vbshvegsh>0)=1;
vbshvegsh=vbshvegsh-vegsh;

if max(bush(:))>0
    g=g-bush;
    g(g>0)=1;
    g(g<0)=0;
    vegsh=vegsh-bushplant+g;
    vegsh(vegsh<0)=0;
end

vegsh(vegsh>0)=1;
shvoveg=(shvoveg-a).*vegsh; %Vegetation shadow volume
vegsh=1-vegsh;
vbshvegsh=1-vbshvegsh;

%removing walls in shadow
% tempfirst=tempfirst-a;
% tempfirst(tempfirst<2)=1;
% tempfirst(tempfirst>=2)=0;

shvo=f-a; %building shadow volume
facesun=double((facesh+double(walls>0))==1 & walls>0);
wallsun=walls-shvo;
wallsun(wallsun<0)=0;
wallsun(facesh==1)=0;% Removing walls in "self"-shadow
% wallsun(tempfirst==0)=0;% Removing walls in shadow 1
% wallsun(wallfirstshade==1)=0;% Removing walls in shadow 2
wallsh=walls-wallsun;
% wallsh(wallfirstshade==1)=0;
% wallsh=wallsh+(wallfirstshade.*walls);
wallbol=double(walls>0);
% facesun=double(walls>0);
% facesun(tempfirst==0)=0;% walls facing sun 1
% facesun(wallfirstshade==1)=0;% walls facing sun 2
% facesh=wallbol-facesun;
wallshve=shvoveg.*wallbol;
wallshve=wallshve-wallsh;
wallshve(wallshve<0)=0;
id=find(wallshve>walls);
wallshve(id)=walls(id);
wallsun=wallsun-wallshve;
id=find(wallsun<0);
wallshve(id)=0;
wallsun(id)=0;

% subplot(2,2,1),imagesc(facesh),axis image ,colorbar,title('facesh')%
% subplot(2,2,2),imagesc(wallsun,[0 20]),axis image, colorbar,title('Wallsun')%
% subplot(2,2,3),imagesc(sh-vegsh*0.8), colorbar,axis image,title('Groundsh')%
% subplot(2,2,4),imagesc(wallshve,[0 20]),axis image, colorbar,title('Wallshve')%
