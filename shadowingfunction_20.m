function [vegsh,sh,vbshvegsh]=shadowingfunction_20(a,vegdem,vegdem2,azimuth,altitude,scale,amaxvalue,bush)
% This function casts shadows on buildings and vegetation units

% conversion
degrees=pi/180;
azimuth=azimuth*degrees;
altitude=altitude*degrees;

% measure the size of the image
sizex=size(a,1);
sizey=size(a,2);

% initialise parameters
dx=0;
dy=0;
dz=0;

sh=zeros(sizex,sizey);
vbshvegsh=sh;
vegsh=sh;
stopbuild=sh;
stopveg=sh;
f=a;
g=sh;
bushplant=bush>1;

pibyfour=pi/4;
threetimespibyfour=3*pibyfour;
fivetimespibyfour=5*pibyfour;
seventimespibyfour=7*pibyfour;
sinazimuth=sin(azimuth);
cosazimuth=cos(azimuth);
tanazimuth=tan(azimuth);
signsinazimuth=sign(sinazimuth);
signcosazimuth=sign(cosazimuth);
dssin=abs(1/sinazimuth);
dscos=abs(1/cosazimuth);
tanaltitudebyscale=tan(altitude)/scale;
index=1;

% main loop
while ((amaxvalue>=dz) && (abs(dx)<=sizex) && (abs(dy)<=sizey))
    
    if ((pibyfour<=azimuth)&&(azimuth<threetimespibyfour))||((fivetimespibyfour<=azimuth)&&(azimuth<seventimespibyfour))
        dy=signsinazimuth*index;
        dx=-1*signcosazimuth*abs(round(index/tanazimuth));
        ds=dssin;
    else
        dy=signsinazimuth*abs(round(index*tanazimuth));
        dx=-1*signcosazimuth*index;
        ds=dscos;
    end
    
    % note: dx and dy represent absolute values while ds is an incremental value
    dz=ds*index*tanaltitudebyscale;
    tempvegdem(1:sizex,1:sizey)=0;
    tempvegdem2(1:sizex,1:sizey)=0;
    temp(1:sizex,1:sizey)=0;
    
    absdx=abs(dx);
    absdy=abs(dy);
    
    xc1=((dx+absdx)/2)+1;
    xc2=(sizex+(dx-absdx)/2);
    yc1=((dy+absdy)/2)+1;
    yc2=(sizey+(dy-absdy)/2);
    
    xp1=-((dx-absdx)/2)+1;
    xp2=(sizex-(dx+absdx)/2);
    yp1=-((dy-absdy)/2)+1;
    yp2=(sizey-(dy+absdy)/2);
    
    
    tempvegdem(xp1:xp2,yp1:yp2)=vegdem(xc1:xc2,yc1:yc2)-dz;
    tempvegdem2(xp1:xp2,yp1:yp2)=vegdem2(xc1:xc2,yc1:yc2)-dz;
    temp(xp1:xp2,yp1:yp2)=a(xc1:xc2,yc1:yc2)-dz;
    
    f=max(f,temp);
    sh(f>a)=1;sh(f<=a)=0; %Moving building shadow
    fabovea=tempvegdem>a; %vegdem above DEM
    gabovea=tempvegdem2>a; %vegdem2 above DEM
    vegsh2=fabovea-gabovea;
    vegsh=max(vegsh,vegsh2);
    vegsh(vegsh.*sh>0)=0;% removing shadows 'behind' buildings
    vbshvegsh=vegsh+vbshvegsh;

    % vegsh at high sun altitudes
    if index==1
        firstvegdem=tempvegdem-temp;
        firstvegdem(firstvegdem<=0)=1000;
        vegsh(firstvegdem<dz)=1;
        vegsh=vegsh.*(vegdem2>a);
        vbshvegsh=zeros(sizex,sizey);
    end
    
    % Bush shadow on bush plant
    if max(bush(:))>0 && max(max(fabovea.*bush))>0
        tempbush(1:sizex,1:sizey)=0;
        tempbush(xp1:xp2,yp1:yp2)=bush(xc1:xc2,yc1:yc2)-dz;
        g=max(g,tempbush);
        g=bushplant.*g;
    end
    
    index=index+1;
    
    % Stopping loop if all shadows reached the ground
%     stopbuild=stopbuild==f;
%      imagesc(stopbuild),axis image,pause(0.3)
%     fin=find(stopbuild==0, 1);
%     stopbuild=f;
%     stopveg=stopveg==vegsh;
%     finveg=find(stopveg==0, 1);
%     stopveg=vegsh;
%     if isempty(fin) && isempty(finveg)
%         dz=amaxvalue+9999;
%     end
    
end

sh=1-sh;
vbshvegsh(vbshvegsh>0)=1;
vbshvegsh=vbshvegsh-vegsh;

if max(bush(:))>0
    g=g-bush;
    g(g>0)=1;
    g(g<0)=0;
    vegsh=vegsh-bushplant+g;
    vegsh(vegsh<0)=0;
end

vegsh(vegsh>0)=1;
vegsh=1-vegsh;
vbshvegsh=1-vbshvegsh;
