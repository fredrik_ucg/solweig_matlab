%% SOLWEIG 2015a
% This m.file is the main script for the SOLWEIG model 
% The desigh of this m-file originates from the interface using
% JA builder and Matlab Compiler
%
% This is a new version (2015a) for the SOLWEIG model 
% This vesion includes land cover (grass, impervious and water)
%
% Fredrik Lindberg, G�teborg Urban Climate Group
% fredrikl@gvc.gu.se

%% Setup
clear
% wd = 'C:\Users\xlinfr\Documents';
wd = 'C:\Users\xlinfr\Documents\PythonScripts\SOLWEIG';
%Input DigitalSurfaceModel (DSM)
% dsmfile=['C:' filesep 'repository' filesep 'solweig_matlab' filesep 'inputdata' filesep 'DSM_KRbig_v3.asc'];
dsmfile=[wd filesep 'SOLWEIGdata' filesep 'DSM_KRbig.asc'];
% SkyViewFactor files
svfname='krbig';
svffiles={[wd filesep 'SOLWEIG' filesep 'SVFs' filesep 'def_', svfname ,'_svf.asc'];
          [wd filesep 'SOLWEIG' filesep 'SVFs' filesep 'def_',svfname,'_svfE.asc']; 
          [wd filesep 'SOLWEIG' filesep 'SVFs' filesep 'def_',svfname,'_svfS.asc'];
          [wd filesep 'SOLWEIG' filesep 'SVFs' filesep 'def_',svfname,'_svfW.asc']; 
          [wd filesep 'SOLWEIG' filesep 'SVFs' filesep 'def_',svfname,'_svfN.asc']};
svffilesveg={[wd filesep 'SOLWEIG' filesep 'SVFs' filesep 'veg_',svfname,'_svf.asc'];
             [wd filesep 'SOLWEIG' filesep 'SVFs' filesep 'veg_',svfname,'_svfE.asc']; 
             [wd filesep 'SOLWEIG' filesep 'SVFs' filesep 'veg_',svfname,'_svfS.asc'];
             [wd filesep 'SOLWEIG' filesep 'SVFs' filesep 'veg_',svfname,'_svfW.asc']; 
             [wd filesep 'SOLWEIG' filesep 'SVFs' filesep 'veg_',svfname,'_svfN.asc'];
             [wd filesep 'SOLWEIG' filesep 'SVFs' filesep 'vega_',svfname,'_svf.asc'];
             [wd filesep 'SOLWEIG' filesep 'SVFs' filesep 'vega_',svfname,'_svfE.asc']; 
             [wd filesep 'SOLWEIG' filesep 'SVFs' filesep 'vega_',svfname,'_svfS.asc'];
             [wd filesep 'SOLWEIG' filesep 'SVFs' filesep 'vega_',svfname,'_svfW.asc']; 
             [wd filesep 'SOLWEIG' filesep 'SVFs' filesep 'vega_',svfname,'_svfN.asc']};

%Meteorological data
metdatafolder='C:\Users\xlinfr\Documents\PythonScripts\SOLWEIG\SOLWEIGdata\';
metdatafile='gbg19970606_2015a.txt';
% metdatafolder=[wd filesep 'SOLWEIG' filesep 'Inputdata' filesep 'metdata' filesep '2015a' filesep ];
% metdatafile='gbg20051011_2015a.txt';

% Outputfolder
coreoutputfolder=[wd filesep 'SOLWEIG_2015a_runs' filesep 'SOLWEIGOutput_cylinderOFF' filesep 'pytest' filesep];

% Location to be modelled
cityname='Goteborg';

% Vegetation file
vegetationfile=['C:' filesep 'SOLWEIG' filesep 'Inputdata' filesep 'VegDEMs' filesep 'ga_buildings.asc'];

%vegetation grids
% cdsmfile=['C:' filesep 'repository' filesep 'solweig_matlab' filesep 'inputdata' filesep 'CDSM_KRbig_v3.asc'];
tdsmfile=[wd filesep 'repository' filesep 'solweig_matlab' filesep 'inputdata' filesep 'filenamme.asc'];
cdsmfile=[wd filesep 'SOLWEIGdata' filesep 'CDSM_KRbig.asc'];

%land cover grid
% landcovergrid=['C:' filesep 'repository' filesep 'solweig_matlab' filesep 'inputdata' filesep 'landcover_KRbig_v3.asc'];
landcoverfile=['C:' filesep 'repository' filesep 'solweig_matlab' filesep 'inputdata' filesep 'landcoverclasses_2015a.txt'];
landcovergrid=[wd filesep 'SOLWEIGdata' filesep 'landcover.asc'];

showimage=1; % Show hourly images of Tmrt during execution
landcover=1; % Use land cover grid for Lup and Kup
sensorheight=2.0; % Height of windsensor for PET/UTCI calc. at POI
cyl=0; % Consider standing man as cylinder
onlyglobal=0; % 1=diffuse and direct components unavailable and modelled with Reindl et al. 1990

usevegdem=1; % Use vegetation scheme, 1=use vegetation dem, 0=don't use vegetation dem
gridfiles=1;% 1=use allready gridded vegetation files, 0=use textfile
oldtextfile=1;% 1=textfile already exists, 0=no textfile exists
onlycdsm=1; % Only canopy vegetation dsm is available
trunkratio=0.25; % Height of trunk zone related to cdsm

%% Loading building and ground data (ESRI ASCIIGrid)
[a,scale,header,headernum,headername,sizey,sizex]=Solweig_10_loaddem(dsmfile);
a(a<0)=0;

%% Setting location 
[location,UTC]=Solweig_10_loc(cityname);

%% Creating/loading vegetqtionDEM (STEP 1c)
usevegdem=1;% 1=use vegetation dem, 0=don't use vegetation dem
gridfiles=0;% 1=use allready gridded files, 0=use textfile
oldvegdem=0;% 1=textfile exists, 0=no textfile exists
% [vegdem,vegdem2,buildings]=Solweig_vegetation_create(usevegdem,gridfiles,oldvegdem,a,sizex,sizey,vegetationfile,scale,cdsmfile,tdsmfile);
vegdem=Solweig_10_loaddem(cdsmfile);
vegdem2=vegdem*0.23;

%% Loading building and ground data (ESRI ASCIIGrid)
lc_grid=Solweig_10_loaddem(landcovergrid);
% lc_grid(lc_grid==-9999)=2;
walls=ordfilt2(a,4,[0 1 0; 1 0 1; 0 1 0]);%one pixel outside building footprints
walls=walls-a;
walls(walls<3)=0;
dirwalls = filter1Goodwin_as_aspect_v3(walls,sizex,sizey,scale,a);

buildfromlc=1;
if buildfromlc==1
    buildings=double(lc_grid~=2);
end

%% Shadowtesting
usevegdem = 1;
leafon = 1;
trans = 0.05;
trunkratio = 0.23;

if usevegdem==1
    % Vegetation transmittivity of shortwave radiation
    psi=leafon*trans;
    psi(leafon==0)=0.5;
    
    % amaxvalue (used in calculation of vegetation shadows)
    vegmax=max(vegdem(:));
    amaxvalue=range(a(:));
    amaxvalue=max(amaxvalue,vegmax);
    
    % Elevation vegdems if buildingDSM includes ground heights
    vegdem=vegdem+a;vegdem(vegdem==a)=0;
    vegdem2=vegdem2+a;vegdem2(vegdem2==a)=0;
    
    % Bush separation
    bush=not(vegdem2.*vegdem).*vegdem;
else
    psi=leafon*0+1;
end

azimuth = 135;
altitude = 30;

[sh wallsh wallsun facesh facesun]=shadowingfunction_wallheight_13(a,azimuth,altitude,scale,walls,dirwalls*pi/180);
% [vegsh,sh,vbshvegsh,wallsh,wallsun,wallshve,facesh,facesun,walls]=shadowingfunction_wallheight_23(a,vegdem,vegdem2,azimuth,altitude,scale,amaxvalue,bush,walls,dirwalls*pi/180);
clim=[0 35];
i1 = subplot(2,2,1);imagesc(walls,clim),axis image ,colorbar,title('walls');%
i2 = subplot(2,2,2);imagesc(wallsun,clim),axis image, colorbar,title('Wallsun');%
i3 = subplot(2,2,3);imagesc(sh), colorbar,axis image,title(['Groundsh, alt: ' num2str(altitude) ', azi:' num2str(azimuth)] );%
i4 = subplot(2,2,4);imagesc(wallsh,clim),axis image, colorbar,title('Wallsh');%
linkaxes([i1,i2,i3,i4])


grid = importdata('C:\Users\xlinfr\Documents\PythonScripts\ShadowTest\walls.txt');

figure
i1 = subplot(2,2,1);imagesc(grid,clim),axis image ,colorbar,title('walls py');%
i2 = subplot(2,2,2);imagesc(walls,clim),axis image ,colorbar,title('walls ml');%
i3 = subplot(2,2,3);imagesc(grid-walls,clim),axis image ,colorbar,title('py-ml');%
i4 = subplot(2,2,4);imagesc(a,clim),axis image ,colorbar,title('dsm');%
linkaxes([i1,i2,i3,i4])



%% Setting point of interest (STEP 1d)
% point=input('Do you want to locate point of interest? (Y or N):  ','s');
% point='n';
% if strcmpi(point,'y')
%     figurehandler=show_figure([50 100 750 650],'none','Set Point of Interest','none');
%     [row,col]=Solweig_20_poi(figurehandler,a,sizex,sizey,vegdem);
%     close_figure(figurehandler);
%     disp(['Point of interest is set to row= ',num2str(row) ,' col= ',num2str(col)]);
%     [a]=set_point_height(a,row,col,1.1);
%     height=1.1;
%     disp('Heigth of point is set to 1,1 meter above ground level')
% else
%     row=[];col=[];height=[];
% end
% row=184;col=155;height=1.1;
%% Loading or generating SVF model (STEP 2)
% param='y';
% param=input('Is SVF model already generated for current DEM? (Y or N):  ','s');
% [svf,svfE,svfS,svfW,svfN,svfveg,svfEveg,svfSveg,svfWveg,svfNveg,svfaveg,...
%     svfEaveg,svfSaveg,svfWaveg,svfNaveg]=Solweig_2013a_svf(svffiles,svffilesveg,a,scale,header,sizex,sizey,vegdem,vegdem2,param);

%% Parameters and constants (STEP 3)
% param=input('Use standard parameterisation? (Y or N):  ','s');
% [albedo_b,albedo_g,absK,absL,ewall,eground,Fside,Fup,PA,trans]=Solweig_2014a_param(param);

%% Input of metdata and calculation of sun positions [STEP 4]
% % [met,met_header,YYYY,altitude,azimuth,zen,jday,leafon,dectime,altmax]=Solweig_2015a_metdata(metdatafolder,metdatafile,location,UTC);
% % onlyglobal=1; % 1=diffuse and direct components unavailable and modelled with Reindl

%% Input of landcover parameters
% formatSpec = '%*s%f%f%f%f%f%[^\n\r]';
% fileID = fopen(landcoverfile,'r');
% dataArray = textscan(fileID, formatSpec, 'Delimiter', ' ', 'MultipleDelimsAsOne', true, 'HeaderLines' ,1);
% fclose(fileID);
% lc_class = [dataArray{1:end-1}];

%% Outputfiles and fomats (1=output, 0=no output)
% output.tmrthour=1;output.tmrtday=0;output.tmrtdiurn=0;
% output.luphour=1;output.lupday=0;output.lupdiurn=0;
% output.ldownhour=0;output.ldownday=0;output.ldowndiurn=0;
% output.kuphour=1;output.kupday=0;
% output.kdownhour=0;output.kdownday=0;
% output.gvfhour=0;output.gvfday=0;
% output.svf=0;
% output.svfveg=0;
% output.svfboth=0;
% fileformat.asc=0;fileformat.tif=0;

%% SOLWEIG MAIN CORE (STEP 5)
% showimage=0; % Show hourly images of Tmrt during execution 
% landcover=1; % Use land cover grid for Lup and (Kup) 
% sensorheight=2.0; % Height of wind sensor
% cyl=0; % Consider standing man as cylinder
% Solweig_2015a_core(coreoutputfolder,a,scale,header,sizey,sizex,row,col,svf,svfN,svfW,svfE,svfS,svfveg,...
%     svfNveg,svfEveg,svfSveg,svfWveg,svfaveg,svfEaveg,svfSaveg,svfWaveg,svfNaveg,vegdem,vegdem2,albedo_b,...
%     albedo_g,absK,absL,ewall,eground,Fside,Fup,PA,met,YYYY,altitude,azimuth,zen,jday,showimage,usevegdem,...
%     onlyglobal,buildings,location,height,trans,output,fileformat,landcover,sensorheight,leafon,lc_grid,...
%     lc_class,dectime,altmax,dirwalls,walls,cyl);
