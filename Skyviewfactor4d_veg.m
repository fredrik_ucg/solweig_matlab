function [svfveg,svfEveg,svfSveg,svfWveg,svfNveg,svfaveg,svfEaveg,svfSaveg,svfWaveg,svfNaveg]=Skyviewfactor4d_veg(outputfiles,a,scale,vegdem,vegdem2,header,sizex,sizey)
%This m.file calculates Skyview factors on a VegetationDEM for the four cardinal points.
%It also calculates separate SVFs for vegetation units shadowed by buildings

%Created by Fredrik Lindberg 2009-10-30
%20130208 - changed to use cell input
%% Set up
svfveg=sparse(zeros(sizey,sizex));
svfEveg=svfveg;svfSveg=svfveg;svfWveg=svfveg;svfNveg=svfveg;
svfaveg=svfveg;
svfEaveg=svfveg;svfSaveg=svfveg;svfWaveg=svfveg;svfNaveg=svfveg;

% amaxvalue 
vegmax=max(vegdem(:));
amaxvalue=range(a(:));
amaxvalue=max(amaxvalue,vegmax);

% Elevation vegdems if buildingDEM inclused ground heights
% vegdem3=vegdem;
vegdem=vegdem+a;vegdem(vegdem==a)=0;
vegdem2=vegdem2+a;vegdem2(vegdem2==a)=0;

% Bush separation
bush=not(vegdem2.*vegdem).*vegdem;
% vegdem=vegdem-bush;

noa=19;% No. of anglesteps minus 1
step=89/noa;
iangle=[((step/2):step:89) 90];
annulino=[round(0:step:89) 90];
[iazimuth aziinterval]=svf_angles_100121(iangle);
aziintervalaniso=ceil(aziinterval/2);
index=1;

%% Main core
for i=1:length(iangle)
    for j=1:aziinterval(i)
        progressbar(index/653,0);
        altitude=iangle(i);
        azimuth=iazimuth(index);
        [vegsh,sh,vbshvegsh]=shadowingfunction_20(a,vegdem,vegdem2,azimuth,altitude,scale,amaxvalue,bush);
%         [vegsh,sh,vbshvegsh]=veg_shadow(a,vegdem,vegdem2,azimuth,altitude,scale);
        
%         for k=annulino(i):annulino(i+1)-1
        for k=annulino(i)+1:annulino(i+1)% changed to include 90
            weight=annulus_weight(k,aziinterval(i));
            svfveg=svfveg+weight*vegsh;
            svfaveg=svfaveg+weight*vbshvegsh;
            weight=annulus_weight(k,aziintervalaniso(i));
            if azimuth >= 0  &&  azimuth < 180
                svfEveg=svfEveg+weight*vegsh;
                svfEaveg=svfEaveg+weight*vbshvegsh;
            end
            if azimuth >= 90  &&  azimuth < 270
                svfSveg=svfSveg+weight*vegsh;
                svfSaveg=svfSaveg+weight*vbshvegsh;
            end
            if azimuth >= 180  &&  azimuth < 360
                svfWveg=svfWveg+weight*vegsh;
                svfWaveg=svfWaveg+weight*vbshvegsh;
            end
            if azimuth >= 270  ||  azimuth < 90
                svfNveg=svfNveg+weight*vegsh;
                svfNaveg=svfNaveg+weight*vbshvegsh;
            end
        end
%         subplot(2,1,1),imagesc(a+vegdem3-(vegsh*-1+1)*20-(sh*-1+1)*10),axis image;
%         subplot(2,1,2),imagesc(svfveg),axis image;
%         pause(0.1)
        index=index+1;
    end
end

progressbar(1,0);
% close

% Last azimuth is 90. Hence, manual add of last annuli for svfS and SVFW
last=(zeros(sizey,sizex));
last(vegdem2==0)=3.0459e-004;
svfSveg=svfSveg+last;
svfWveg=svfWveg+last; 
svfSaveg=svfSaveg+last;
svfWaveg=svfWaveg+last;

%Forcing svf not be greater than 1 (some MATLAB crazyness)
svfveg(svfveg>1)=1;svfEveg(svfEveg>1)=1;svfSveg(svfSveg>1)=1;svfWveg(svfWveg>1)=1;svfNveg(svfNveg>1)=1;
svfaveg(svfaveg>1)=1;svfEaveg(svfEaveg>1)=1;svfSaveg(svfSaveg>1)=1;svfWaveg(svfWaveg>1)=1;svfNaveg(svfNaveg>1)=1;


% Save svfs image as ESRIGRID
out=[];
out(:,:,1)=svfveg(1:sizey,1:sizex);
out(:,:,2)=svfEveg(1:sizey,1:sizex);
out(:,:,3)=svfSveg(1:sizey,1:sizex);
out(:,:,4)=svfWveg(1:sizey,1:sizex);
out(:,:,5)=svfNveg(1:sizey,1:sizex);
out(:,:,6)=svfaveg(1:sizey,1:sizex);
out(:,:,7)=svfEaveg(1:sizey,1:sizex);
out(:,:,8)=svfSaveg(1:sizey,1:sizex);
out(:,:,9)=svfWaveg(1:sizey,1:sizex);
out(:,:,10)=svfNaveg(1:sizey,1:sizex);

for i=1:10
    fn2=fopen(outputfiles{i},'w');
    for p=1:6
        fprintf(fn2,'%6s', header{p});
        fprintf(fn2,'\r\n');
    end
    for p=1:size(out(:,:,1),1)
        fprintf(fn2,'%6.4f ',out(p,:,i));
        fprintf(fn2,'\r\n');
    end
    fclose(fn2);
end
