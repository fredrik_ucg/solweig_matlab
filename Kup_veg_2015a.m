function [Kup,KupE,KupS,KupW,KupN]=Kup_veg_2015a(radI,radD,radG,altitude,svfbuveg,albedo_b,F_sh,gvfalb,gvfalbE,gvfalbS,gvfalbW,gvfalbN,gvfalbnosh,gvfalbnoshE,gvfalbnoshS,gvfalbnoshW,gvfalbnoshN)

Kup=(gvfalb.*radI*sin(altitude*(pi/180)))+(radD*svfbuveg+...
    albedo_b*(1-svfbuveg).*(radG*(1-F_sh)+radD*F_sh)).*gvfalbnosh;

KupE=(gvfalbE.*radI*sin(altitude*(pi/180)))+(radD*svfbuveg+...
    albedo_b*(1-svfbuveg).*(radG*(1-F_sh)+radD*F_sh)).*gvfalbnoshE;

KupS=(gvfalbS.*radI*sin(altitude*(pi/180)))+(radD*svfbuveg+...
    albedo_b*(1-svfbuveg).*(radG*(1-F_sh)+radD*F_sh)).*gvfalbnoshS;

KupW=(gvfalbW.*radI*sin(altitude*(pi/180)))+(radD*svfbuveg+...
    albedo_b*(1-svfbuveg).*(radG*(1-F_sh)+radD*F_sh)).*gvfalbnoshW;

KupN=(gvfalbN.*radI*sin(altitude*(pi/180)))+(radD*svfbuveg+...
    albedo_b*(1-svfbuveg).*(radG*(1-F_sh)+radD*F_sh)).*gvfalbnoshN;

