function [met,met_header,YYYY,altitude,azimuth,zen,jday,leafon]=Solweig_2014a_metdata(inputmetfolder,inputmetfile,location,UTC)
% This function is used to process the input meteorological file. 
% It also calculates Sun position based on the time specified in the met-file
% New version from (2013b) alters hour to previous hour and change format
% to SUEWS 2013a format

% Read meteorological file
Newdata1=importdata([inputmetfolder inputmetfile],'\t',1);
met=Newdata1.data;
met_header=Newdata1.textdata;
% halftimestep=floor((met(2,3)-met(1,3))*1440/2);
halftimestepdec=(met(2,3)-met(1,3))/2;
% time.min=30;%min=30 (sunposition in the middle of the hour)!!!!!!!!!
% time.sec=0;
time.UTC=UTC;

% Read ModelledYears.txt (will change later)
Newdata=importdata([inputmetfolder 'ModelledYears.txt'],' ',1);
newyear=1;
YYYYstart=Newdata.data(newyear,1);
leafon1=Newdata.data(newyear,2);
leafoff1=Newdata.data(newyear,3);

for i=1:length(Newdata1.data(:,1))
    % Creating time structure
    [time.year,time.month,time.day,time.hour,time.min,time.sec]=datevec(datenum([YYYYstart,1,0])+met(i,3)-halftimestepdec);%
%     time.hour=met(i,2);%-1;!!!!!!!!!
%     if time.hour<0
%         time.hour=23;
%         [time.year,time.month,time.day]=datevec(datenum([YYYYstart,1,0])+met(i,1)-1);
%     end
    
    % Sun position
    sun=sun_position(time,location);
    altitude(1,i)=90-sun.zenith; azimuth(1,i)=sun.azimuth; zen(1,i)=sun.zenith*(pi/180);
    
    %day of year and check for leap year
    A=logical(mod(time.year,4));
    B=logical(mod(time.year,100));
    C=logical(mod(time.year,400));
    leapyear=ismember(time.year,time.year(~C | (~A & B)));
    if leapyear==1
        dayspermonth=[31 29 31 30 31 30 31 31 30 31 30 31];
    else
        dayspermonth=[31 28 31 30 31 30 31 31 30 31 30 31];
    end
    jday(1,i)=sum(dayspermonth(1:time.month-1))+time.day;
    YYYY(1,i)=YYYYstart;
    if met(i,3)>leafon1 || met(i,3)<leafoff1
        leafon(1,i)=1;
    else
        leafon(1,i)=0;
    end

    if i>24 && met(i,2)==0 && met(i,1)==0 % Multiple years
        newyear=newyear+1;
        YYYYstart=Newdata.data(newyear,1);
        leafon1=Newdata.data(newyear,2);
        leafoff1=Newdata.data(newyear,3);
    end
    
end

% if time.month<10, XM='0'; else XM='';end
% if time.day<10, XD='0'; else XD='';end
% time=timemat;
