function image_write(img,filename,ext,scalemin,scalemax)

% Convert image to double
img = double( img );

% Normalize image between zero and one
% img = img - min( img(:));
% img = img / max( img(:));
img=img-scalemin;
img=img/scalemax;

% Write out normalized image
if ~exist( 'ext', 'var' ) || isempty( ext )
  imwrite( img, filename )
else
  imwrite( img, filename, ext );
end

return;