function dirwalls = filter1Goodwin_as_aspect_v3(walls,sizex,sizey,scale,a)
% this function applies the filter processing presented in Goodwin et al
% 2010 but instead for removing linear fetures it calculates direction of
% wall
%
%Fredrik Lindberg, 2012-02-14
%fredrikl@gvc.gu.se

if isempty(walls)==1
    walls=ordfilt2(a,4,[0 1 0; 1 0 1; 0 1 0]);
    walls=walls-a;
    walls(walls<3)=0;
end

filtersize=floor(scale*9);
if scale~=1
    if mod(filtersize,scale)==0
        filtersize=filtersize-1;
    end
end
filthalveceil=ceil(filtersize/2);
filthalvefloor=floor(filtersize/2);

filtmatrix=zeros(filtersize,filtersize);
buildfilt=filtmatrix;

filtmatrix(:,filthalveceil)=1;
buildfilt(filthalveceil,1:filthalvefloor)=1;
buildfilt(filthalveceil,filthalveceil+1:filtersize)=2;

y=zeros(sizey,sizex);%final direction
z=y;%temporary direction
x=z;%building side
walls(walls>0)=1;

for h=0:1:180 %increased resolution to 1 deg 20140911
    filtmatrix1=imrotate(filtmatrix,h,'bilinear','crop');
    filtmatrixbuild=imrotate(buildfilt,h,'nearest','crop');
    index=270-h;
    if h==150
        filtmatrixbuild(:,9)=0;
    end
    if h==30
        filtmatrixbuild(:,9)=0;
    end
%     test2(h+1)=sum(filtmatrixbuild(filtmatrixbuild==2))/2;
%     test1(h+1)=sum(filtmatrixbuild(filtmatrixbuild==1));
    
%     if index<0
%         index=90-h+360;
%     end
    if index==225
        n=length(filtmatrix);
        filtmatrix1(1,1)=1;
        filtmatrix1(n,n)=1;
    end
    if index==135
        n=length(filtmatrix);
        filtmatrix1(1,n)=1;
        filtmatrix1(n,1)=1;
    end
    
    for i=filthalveceil:sizey-filthalveceil
        for j=filthalveceil:sizex-filthalveceil
            if walls(i,j)==1
                wallscut=walls(i-filthalvefloor:i+filthalvefloor,j-filthalvefloor:j+filthalvefloor).*filtmatrix1;
                dsmcut=a(i-filthalvefloor:i+filthalvefloor,j-filthalvefloor:j+filthalvefloor);
                if z(i,j)<sum(sum(wallscut))
                    z(i,j)=sum(sum(wallscut));
                    if sum(dsmcut(filtmatrixbuild==1))>sum(dsmcut(filtmatrixbuild==2))
                        x(i,j)=1;
                    else
                        x(i,j)=2;
                    end
                    y(i,j)=index;
                if i==92 && j==172
%                 subplot(2,3,1),imagesc(walls(i-filthalvefloor:i+filthalvefloor,j-filthalvefloor:j+filthalvefloor)),axis image
%                 subplot(2,3,2),imagesc(wallscut),axis image
%                 subplot(2,3,3),imagesc(dsmcut),axis image
%                 subplot(2,3,4),imagesc(y),axis image
%                 subplot(2,3,5),imagesc(filtmatrixbuild),axis image
%                 subplot(2,3,6),imagesc(walls(i-filthalvefloor:i+filthalvefloor,j-filthalvefloor:j+filthalvefloor)),axis image
%                 pause(0.01)    
                end
                end

            end
        end
    end    
end
y(x==1)=y(x==1)-180;
y(y<0)=y(y<0)+360;

[~,aspect] = get_ders(a,1/scale);% filling edges of model domain
y=y+((walls==1 & y==0).*(aspect/(pi/180)));

dirwalls=y;

