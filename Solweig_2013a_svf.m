function [svf,svfE,svfS,svfW,svfN,svfveg,svfEveg,svfSveg,svfWveg,svfNveg,svfaveg,svfEaveg,svfSaveg,svfWaveg,svfNaveg] = Solweig_2013a_svf(svffiles,svffilesveg,a,scale,header,sizex,sizey,vegdem,vegdem2,param)
% This function is used to load or generate SVF images based on the
% building and vegetation DSMs

if isempty(vegdem) %Using only default DSM
   if strcmpi(param,'n')
        [svf,svfE,svfS,svfW,svfN]=Skyviewfactor4d(svffiles,a,scale,header,sizex,sizey);
        svfveg=1;svfNveg=1;svfEveg=1;svfSveg=1;svfWveg=1;
        svfaveg=1;svfNaveg=1;svfEaveg=1;svfSaveg=1;svfWaveg=1;
    else
        [svf,svfE,svfS,svfW,svfN]=Solweig_10_loadsvf(svffiles);
        svfveg=1;svfNveg=1;svfEveg=1;svfSveg=1;svfWveg=1;
        svfaveg=1;svfNaveg=1;svfEaveg=1;svfSaveg=1;svfWaveg=1;
    end
else %Using default DSM and vegdem
    if strcmpi(param,'n')
        [svf,svfE,svfS,svfW,svfN]=Skyviewfactor4d(svffiles,a,scale,header,sizex,sizey);
        [svfveg,svfEveg,svfSveg,svfWveg,svfNveg,svfaveg,svfEaveg,...
            svfSaveg,svfWaveg,svfNaveg]=Skyviewfactor4d_veg(svffilesveg...
            ,a,scale,vegdem,vegdem2,header,sizex,sizey);
    else
        [svf,svfE,svfS,svfW,svfN]=Solweig_10_loadsvf(svffiles);
        [svfveg,svfEveg,svfSveg,svfWveg,svfNveg,svfaveg,svfEaveg,...
            svfSaveg,svfWaveg,svfNaveg]=Solweig_20_loadsvfveg(svffilesveg);
    end
end