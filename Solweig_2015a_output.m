function [] = Solweig_2015a_output(modva,Tmrtday,x,y,cyl)

screensize=get(0,'ScreenSize');
screenwidth=screensize(3);
screenheight=screensize(4);
% The figure takes the 70% of the screen
figure('Position',[(screenwidth-0.85*screenwidth) (screenheight-0.85*screenheight) (screenwidth*0.7) (screenheight*0.7)],'Name','Model output','Numbertitle','off');

if isempty(x)
    % Large Tmrt image as output
    set(clf,'color',[1 1 1])
    imagesc(Tmrtday),axis image; colorbar, colormap(jet), title('Daytime mean T_m_r_t (�C)','FontSize',12);
else
    set(clf,'color',[1 1 1])
    
    % Tmrt image
    subplot(2,2,1)
    imagesc(Tmrtday),axis image; colorbar, colormap(jet), title('Daytime T_m_r_t (�C)','FontSize',12);
    hold on
    plot(y,x,'b *');
    
    %Tmrt plot from poi
    subplot(2,2,2)
%     plot(modva(:,4),modva(:,28),'r*-'),hold on, ylim([-20 70])%, xlim([5 21])
    plot(modva(:,4),modva(:,28),'MarkerFaceColor',[1 1 1],'MarkerSize',4,'Marker','*','Color',[1 0 0])
    hold on
    title('T_{mrt} (�C) at point of interest','FontSize',12)
    ylabel('�C','FontSize',12)
    xlabel('Decimal time','FontSize',12)
%     tick=find(modva(:,3)==0 | modva(:,3)==6 |  modva(:,3)==12 | modva(:,3)==18);
%     set(gca,'XTick',tick),set(gca,'FontSize',8)
%     set(gca,'XTickLabel',modva(tick,3));
   
    % K fluxes plot at poi
    subplot(2,2,4)
    plot(modva(:,4),modva(:,10),'MarkerFaceColor',[1 1 1],'MarkerSize',5,'Marker','*','Color',[1 0 0])
    hold on %, ylim([0 1000])%,xlim([5 21])
    plot(modva(:,4),modva(:,11),'MarkerFaceColor',[1 1 1],'MarkerSize',5,'Marker','+','Color',[1 0 0])
    plot(modva(:,4),modva(:,13),'MarkerFaceColor',[1 1 1],'MarkerSize',5,'Marker','o','Color',[1 0 0])
    plot(modva(:,4),modva(:,14),'MarkerFaceColor',[1 1 1],'MarkerSize',5,'Marker','s','Color',[1 0 0])
    plot(modva(:,4),modva(:,15),'MarkerFaceColor',[1 1 1],'MarkerSize',5,'Marker','d','Color',[1 0 0])
    plot(modva(:,4),modva(:,16),'MarkerFaceColor',[1 1 1],'MarkerSize',5,'Marker','v','Color',[1 0 0])
    if cyl == 0
        legend('Kdown','Kup','Knorth','Keast','Ksouth','Kwest','Location','Best')
    else
        plot(modva(:,4),modva(:,12),'MarkerFaceColor',[1 1 1],'MarkerSize',5,'Marker','.','Color',[1 0 0])
        legend('Kdown','Kup','Knorth_D','Keast_D','Ksouth_D','Kwest_D','Kside_I','Location','Best')
    end
    title('Shortwave radiation fluxes at point of interest','FontSize',12)
    ylabel('W/m^2','FontSize',12)
    xlabel('Decimal time','FontSize',12)
%     set(gca,'XTick',tick),set(gca,'FontSize',8)
%     set(gca,'XTickLabel',modva(tick,3));
    
    % L fluxes plot at poi
    subplot(2,2,3)
    plot(modva(:,4),modva(:,17),'MarkerFaceColor',[1 1 1],'MarkerSize',5,'Marker','*','Color',[1 0 0])
    hold on %, ylim([200 600])%,xlim([5 21])
    plot(modva(:,4),modva(:,18),'MarkerFaceColor',[1 1 1],'MarkerSize',5,'Marker','+','Color',[1 0 0])
    plot(modva(:,4),modva(:,19),'MarkerFaceColor',[1 1 1],'MarkerSize',5,'Marker','o','Color',[1 0 0])
    plot(modva(:,4),modva(:,20),'MarkerFaceColor',[1 1 1],'MarkerSize',5,'Marker','s','Color',[1 0 0])
    plot(modva(:,4),modva(:,21),'MarkerFaceColor',[1 1 1],'MarkerSize',5,'Marker','d','Color',[1 0 0])
    plot(modva(:,4),modva(:,22),'MarkerFaceColor',[1 1 1],'MarkerSize',5,'Marker','v','Color',[1 0 0])
    legend('Ldown','Lup','Lnorth','Least','Lsouth','Lwest','Location', 'Best')
    title('Longwave radiation fluxes at point of interest','FontSize',12)
    ylabel('W/m^2','FontSize',12)
    xlabel('Decimal time','FontSize',12)
%     set(gca,'XTick',tick),set(gca,'FontSize',8)
%     set(gca,'XTickLabel',modva(tick,3));
    hold off
end


