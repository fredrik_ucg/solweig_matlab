function Solweig_outputfiles_hour_2013b(output,fileformat,Tmrt,Kup,Kdown,Lup,Ldown,gvf,outputfolder,YYYY,DOY,hour,PA,header,sizey)

if(exist([outputfolder 'ImagesAndAsciiGrids'],'dir')==0)
    mkdir([outputfolder 'ImagesAndAsciiGrids']);
end

% Output of Tmrtmap for each hour
if output.tmrthour==1
    if fileformat.asc==1
        fn2=fopen([outputfolder 'ImagesAndAsciiGrids' filesep 'Tmrt_' num2str(YYYY) '_' num2str(DOY) '_' num2str(hour) '_' PA '.asc'],'w');
        for p=1:6
            fprintf(fn2,'%6s', header{p});
            fprintf(fn2,'\r\n');
        end
        for p=1:sizey
            fprintf(fn2,'%6.4f ',Tmrt(p,:));
            fprintf(fn2,'\r\n');
        end
        fclose(fn2);
    end
    if fileformat.tif==1
        image_write(Tmrt,[outputfolder 'ImagesAndAsciiGrids' filesep 'Tmrt_' num2str(YYYY) '_' num2str(DOY) '_' num2str(hour) '_' PA '.tif'],'tif',-10,80);
    end
end

% Output of Kupmap for each hour
if output.kuphour==1
    if fileformat.asc==1
        fn2=fopen([outputfolder 'ImagesAndAsciiGrids' filesep 'Kup_at_' num2str(YYYY) '_' num2str(DOY) '_' num2str(hour) '_' PA '.asc'],'w');
        for p=1:6
            fprintf(fn2,'%6s', header{p});
            fprintf(fn2,'\r\n');
        end
        for p=1:sizey
            fprintf(fn2,'%6.4f ',Kup(p,:));
            fprintf(fn2,'\r\n');
        end
        fclose(fn2);
    end
    if fileformat.tif==1
        image_write(Kup,[outputfolder 'ImagesAndAsciiGrids' filesep 'Kup_at_' num2str(YYYY) '_' num2str(DOY) '_' num2str(hour) '_' PA '.tif'],'tif',0,300);
    end
end

% Output of Kdownmap for each hour
if output.kdownhour==1
    if fileformat.asc==1
        fn2=fopen([outputfolder 'ImagesAndAsciiGrids' filesep 'Kdown_at_' num2str(YYYY) '_' num2str(DOY) '_' num2str(hour) '_' PA '.asc'],'w');
        for p=1:6
            fprintf(fn2,'%6s', header{p});
            fprintf(fn2,'\r\n');
        end
        for p=1:sizey
            fprintf(fn2,'%6.4f ',Kdown(p,:));
            fprintf(fn2,'\r\n');
        end
        fclose(fn2);
    end
    if fileformat.tif==1
        image_write(Kdown,[outputfolder 'ImagesAndAsciiGrids' filesep 'Kdown_at_' num2str(YYYY) '_' num2str(DOY) '_' num2str(hour) '_' PA '.tif'],'tif',0,1000);
    end
end

% Output of Lupmap for each hour
if output.luphour==1
    if fileformat.asc==1
        fn2=fopen([outputfolder 'ImagesAndAsciiGrids' filesep 'Lup_at_' num2str(YYYY) '_' num2str(DOY) '_' num2str(hour) '_' PA '.asc'],'w');
        for p=1:6
            fprintf(fn2,'%6s', header{p});
            fprintf(fn2,'\r\n');
        end
        for p=1:sizey
            fprintf(fn2,'%6.4f ',Lup(p,:));
            fprintf(fn2,'\r\n');
        end
        fclose(fn2);
    end
    if fileformat.tif==1
        image_write( Lup, [outputfolder 'ImagesAndAsciiGrids' filesep 'Lup_at_' num2str(YYYY) '_' num2str(DOY) '_' num2str(hour) '_' PA '.tif'], 'tif',200,500);
    end
end

% Output of Ldownmap for each hour
if output.ldownhour==1
    if fileformat.asc==1
        fn2=fopen([outputfolder 'ImagesAndAsciiGrids' filesep 'Ldown_at_' num2str(YYYY) '_' num2str(DOY) '_' num2str(hour) '_' PA '.asc'],'w');
        for p=1:6
            fprintf(fn2,'%6s', header{p});
            fprintf(fn2,'\r\n');
        end
        for p=1:sizey
            fprintf(fn2,'%6.4f ',Ldown(p,:));
            fprintf(fn2,'\r\n');
        end
        fclose(fn2);
    end
    if fileformat.tif==1
        image_write(Ldown,[outputfolder 'ImagesAndAsciiGrids' filesep 'Ldown_at_' num2str(YYYY) '_' num2str(DOY) '_' num2str(hour) '_' PA '_' PA '.tif'],'tif',200,500);
    end
end

% Output of GVF for each hour
if output.gvfhour==1
    if fileformat.asc==1
        fn2=fopen([outputfolder 'ImagesAndAsciiGrids' filesep 'GVF_at_' num2str(YYYY) '_' num2str(DOY) '_' num2str(hour) '_' PA '.asc'],'w');
        for p=1:6
            fprintf(fn2,'%6s', header{p});
            fprintf(fn2,'\r\n');
        end
        for p=1:sizey
            fprintf(fn2,'%6.4f ',gvf(p,:));
            fprintf(fn2,'\r\n');
        end
        fclose(fn2);
    end
    if fileformat.tif==1
        image_write(gvf,[outputfolder 'ImagesAndAsciiGrids' filesep 'gvf_at_' num2str(YYYY) '_' num2str(DOY) '_' num2str(hour) '_' PA '.tif'],'tif',0,1);
    end
end