function Solweig_outputfiles_diurnal_2013b(output,fileformat,Tmrtdiurn,Lupdiurn,Ldowndiurn,svf,svfveg,outputfolder,YYYY,DOY,PA,header,sizey)

if(exist([outputfolder 'ImagesAndAsciiGrids'],'dir')==0)
    mkdir([outputfolder 'ImagesAndAsciiGrids']);
end

% Output of Tmrtmap, diurnal average
if output.tmrtdiurn==1
    if fileformat.asc==1
    fn2=fopen([outputfolder 'ImagesAndAsciiGrids' filesep 'AverageDiurnalTmrt_' num2str(YYYY) '_' num2str(DOY) '_' PA '.asc'],'w');
    for p=1:6
        fprintf(fn2,'%6s', header{p});
        fprintf(fn2,'\r\n');
    end
    for p=1:sizey
        fprintf(fn2,'%6.4f ',Tmrtdiurn(p,:));
        fprintf(fn2,'\r\n');
    end
    fclose(fn2);
    end
    if fileformat.tif==1
       image_write(Tmrtdiurn,[outputfolder 'ImagesAndAsciiGrids' filesep 'AverageDiurnalTmrt_' num2str(YYYY) '_' num2str(DOY) '_' PA '.tif'],'tif',-10,80); 
    end
end

% Output of Lupmap, diurnal average
if output.lupdiurn==1
    if fileformat.asc==1
    fn2=fopen([outputfolder 'ImagesAndAsciiGrids' filesep 'AverageDiurnalLup_' num2str(YYYY) '_' num2str(DOY) '_' PA '.asc'],'w');
    for p=1:6
        fprintf(fn2,'%6s', header{p});
        fprintf(fn2,'\r\n');
    end
    for p=1:sizey
        fprintf(fn2,'%6.4f ',Lupdiurn(p,:));
        fprintf(fn2,'\r\n');
    end
    fclose(fn2);
    end
    if fileformat.tif==1
       image_write(Lupdiurn,[outputfolder 'ImagesAndAsciiGrids' filesep 'AverageDiurnalLup_' num2str(YYYY) '_' num2str(DOY) '_' PA '.tif'],'tif',200,500); 
    end
end

% Output of Ldownmap, diurnal average
if output.ldowndiurn==1
    if fileformat.asc==1
    fn2=fopen([outputfolder 'ImagesAndAsciiGrids' filesep 'AverageDiurnalLdown_' num2str(YYYY) '_' num2str(DOY) '_' PA '.asc'],'w');
    for p=1:6
        fprintf(fn2,'%6s', header{p});
        fprintf(fn2,'\r\n');
    end
    for p=1:sizey
        fprintf(fn2,'%6.4f ',Ldowndiurn(p,:));
        fprintf(fn2,'\r\n');
    end
    fclose(fn2);
    end
    if fileformat.tif==1
       image_write(Ldowndiurn,[outputfolder 'ImagesAndAsciiGrids' filesep 'AverageDiurnalLdown_' num2str(YYYY) '_' num2str(DOY) '_' PA '.tif'],'tif',200,500); 
    end
end

% Output of BuildingSVF
if output.svf==1
    if fileformat.asc==1
    fn2=fopen([outputfolder 'ImagesAndAsciiGrids' filesep 'BuildingAndGroundSVF.asc'],'w');
    for p=1:6
        fprintf(fn2,'%6s', header{p});
        fprintf(fn2,'\r\n');
    end
    for p=1:sizey
        fprintf(fn2,'%6.4f ',svf(p,:));
        fprintf(fn2,'\r\n');
    end
    fclose(fn2);
    end
    if fileformat.tif==1
       image_write(svf,[outputfolder 'ImagesAndAsciiGrids' filesep 'BuildingAndGroundSVF.tif'],'tif',0,1); 
    end
end

% Output of VegetationSVF
if output.svfveg==1
    if fileformat.asc==1
    fn2=fopen([outputfolder 'ImagesAndAsciiGrids' filesep 'VegetationSVF.asc'],'w');
    for p=1:6
        fprintf(fn2,'%6s', header{p});
        fprintf(fn2,'\r\n');
    end
    for p=1:sizey
        fprintf(fn2,'%6.4f ',svfveg(p,:));
        fprintf(fn2,'\r\n');
    end
    fclose(fn2);
    end
    if fileformat.tif==1
       image_write(svfveg,[outputfolder 'ImagesAndAsciiGrids' filesep 'VegetationSVF.tif'],'tif',0,1); 
    end
end

% Output of SVFboth

if output.svfboth==1
    svfboth=(svf+svfveg)-1;
    if fileformat.asc==1
    fn2=fopen([outputfolder 'ImagesAndAsciiGrids' filesep 'VegetationBuildingSVF.asc'],'w');
    for p=1:6
        fprintf(fn2,'%6s', header{p});
        fprintf(fn2,'\r\n');
    end
    for p=1:sizey
        fprintf(fn2,'%6.4f ',svfboth(p,:));
        fprintf(fn2,'\r\n');
    end
    fclose(fn2);
    end
    if fileformat.tif==1
       image_write(svfboth,[outputfolder 'ImagesAndAsciiGrids' filesep 'VegetationBuildingSVF.tif'],'tif',0,2); 
    end
end