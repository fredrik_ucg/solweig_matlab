function petout=petcalculator3(ta,RH,tmrt,v,pet)
% This function calculates PET based on a fortran code created by P. H�ppe,
% modified by J. Holst and translated into matlab code by F. Lindberg and
% B. Holmer.


%% Temporary varibles
% ta=0;
% RH=93;
% tmrt=-1.494;
% pet.mbody=75;
% pet.age=35;
% pet.height=1.80;
% pet.activity=80;
% pet.sex=1;
% pet.clo=0.9;
% v=6;

if ta<=-99 || RH<=-99 || v<=-99 || tmrt<=-99
    petout=-999;
else
    
    %% Personal variables
    age=pet.age;
    mbody=pet.mbody;
    ht=pet.height;
    work=pet.activity;
    eta=0;
    icl=pet.clo;
    sex=pet.sex;
    
    %% constants from old code
    po=1013.25; % Pressure
    p=1013.25; % Pressure
    rob=1.06;
    cb=3.64*1000;
    food=0;
    emsk=0.99;
    emcl=0.95;
    evap=2.42*10^6;
    sigma=5.67*10^(-8);
    cair=1.01*1000;
    
    %% humidity
    vps=6.107*(10^(7.5*ta/(238+ta)));
    vpa=RH*vps/100;
    
    %% INBODY
    metbf=3.19*mbody^(3/4)*(1+0.004 *(30-age)+0.018*((ht*100/(mbody^(1/3)))-42.1));
    metbm=3.45*mbody^(3/4)*(1+0.004 *(30-age)+0.010*((ht*100/(mbody^(1/3)))-43.4));
    if sex==1, met=metbm+work; else met=metbf+work ;end
    h=met*(1-eta);
    rtv=1.44*10^(-6)*met ;
    
    % sensible respiration energy
    tex =0.47*ta+21.0;
    eres=cair*(ta-tex)*rtv;
    % latent respiration energy
    vpex =6.11*10^(7.45*tex/(235+tex));
    erel= 0.623*evap/p*(vpa-vpex)*rtv;
    % sum of the results
    ere=eres+erel;
    
    %% calcul
    % constants
    feff=0.725;
    adu=0.203*mbody^0.425*ht^0.725;
    facl=(-2.36+173.51*icl-100.76*icl*icl+19.28*(icl^3))/100;
    if facl>1, facl=1;end
    rcl=(icl/6.45)/facl;
    y=1;
    if icl<2, y=(ht-0.2)/ht;end
    if icl<=0.6, y=0.5;end
    if icl<=0.3, y=0.1;end
    fcl=1+0.15*icl;
    r2=adu*(fcl-1.+facl)/(2*3.14*ht*y);
    r1=facl*adu/(2*3.14*ht*y);
    di=r2-r1;
    acl=adu*facl+adu*(fcl-1);
    
    
    
    %% old calcul subroutine
    % Constants moved outside while loop
    wetsk=0;
    hc=2.67+6.5*v^0.67;
    hc=hc*(p/po)^0.55 ;
    c(1)=h+ere ;
    he=0.633*hc/(p*cair);
    fec=1/(1+0.92*hc*rcl);
    htcl=6.28*ht*y*di/(rcl*log(r2/r1)*acl);
    aeff=adu*feff;
    c(2)=adu*rob*cb;
    c(5)=0.0208*c(2);
    c(6)=0.76075*c(2);
    rdsk=0.79*10^7;
    rdcl=0;
    
    % wr=erel/evap*3600*(-1000);
    
    % skin temperatures
    count2=0;
    j=1;
    % for j=1:7
    while count2==0 && j<7
        
        tsk=34;
        count1=0;
        tcl=(ta+tmrt+tsk)/3;
        count3=1;
        enbal2=0;
        
        while count1<=3
            enbal=0;% placering os�ker
            while (enbal*enbal2)>=0 && count3<200
                enbal2=enbal; % Uppflyttad
                
                %20
                rclo2=emcl*sigma*((tcl+273.2)^4-(tmrt+273.2)^4)*feff;
                tsk=1/htcl*(hc*(tcl-ta)+rclo2)+tcl;
                
                % radiation balance
                rbare=aeff*(1-facl)*emsk*sigma*((tmrt+273.2)^4-(tsk+273.2)^4);
                rclo=feff*acl*emcl*sigma*((tmrt+273.2)^4-(tcl+273.2)^4);
                rsum=rbare+rclo;
                
                % convection
                cbare=hc*(ta-tsk)*adu*(1-facl);
                cclo=hc*(ta-tcl)*acl;
                csum=cbare+cclo;
                
                % core temperature
                c(3)=18-0.5*tsk;
                c(4)=5.28*adu*c(3);
                c(7)=c(4)-c(6)-tsk*c(5);
                c(8)=-c(1)*c(3)-tsk*c(4)+tsk*c(6);
                c(9)=c(7)*c(7)-4*c(5)*c(8);
                c(10)=5.28*adu-c(6)-c(5)*tsk;
                c(11)=c(10)*c(10)-4*c(5)*(c(6)*tsk-c(1)-5.28*adu*tsk);
                
                if tsk==36, tsk=36.01;end
                tcore(7)=c(1)/(5.28*adu+c(2)*6.3/3600)+tsk;
                tcore(3)=c(1)/(5.28*adu+(c(2)*6.3/3600)/(1+0.5*(34-tsk)))+tsk;
                if c(11)>=0, tcore(6)=(-c(10)-c(11)^0.5)/(2*c(5));end
                if c(11)>=0, tcore(1)=(-c(10)+c(11)^0.5)/(2*c(5));end
                if c(9)>=0, tcore(2)=(-c(7)+abs(c(9))^0.5)/(2*c(5));end
                if c(9)>=0, tcore(5)=(-c(7)-abs(c(9))^0.5)/(2*c(5));end
                tcore(4)=c(1)/(5.28*adu+c(2)*1/40)+tsk;
                
                % transpiration
                tbody=0.1*tsk+0.9*tcore(j);
                sw=304.94*(tbody-36.6)*adu/3600000;
                vpts=6.11*10^(7.45*tsk/(235.+tsk));
                if tbody<=36.6, sw=0;end
                if sex==2, sw=0.7*sw;end
                eswphy=-sw*evap;
                
                eswpot=he*(vpa-vpts)*adu*evap*fec;
                wetsk=eswphy/eswpot;
                if wetsk>1, wetsk=1;end
                eswdif=eswphy-eswpot;
                if eswdif<=0, esw=eswpot; else esw=eswphy;end
                if esw>0, esw=0;end
                
                % diffusion
                ed=evap/(rdsk+rdcl)*adu*(1-wetsk)*(vpa-vpts);
                
                % MAX VB
                vb1=34-tsk;
                vb2=tcore(j)-36.6;
                if vb2<0, vb2=0;end
                if vb1<0, vb1=0;end
                vb=(6.3+75*(vb2))/(1+0.5*vb1);
                
                % energy balance
                enbal=h+ed+ere+esw+csum+rsum+food;
                
                % clothing's temperature
                if count1==0, xx=1;end
                if count1==1, xx=0.1;end
                if count1==2, xx=0.01;end
                if count1==3, xx=0.001;end
                if enbal>0, tcl=tcl+xx;else tcl=tcl-xx; end
                
                count3=count3+1;
            end
            count1=count1+1;
            enbal2=0;
            
        end
        
        if j==2 || j==5
            if c(9)>=0
                if tcore(j)>=36.6 && tsk<=34.050
                    if (j~=4 && vb>=91) || (j==4 && vb<89)
                    else
                        if vb>90,vb=90;end
                        count2=1;
                    end
                end
            end
        end
        
        if j==6 || j==1
            if c(11)>0
                if tcore(j)>=36.6 && tsk>33.850
                    if (j~=4 && vb>=91) || (j==4 && vb<89)
                    else
                        if vb>90,vb=90;end
                        count2=1;
                    end
                end
            end
        end
        
        if j==3
            if tcore(j)< 36.6 && tsk<=34.000
                if (j~=4 && vb>=91) || (j==4 && vb<89)
                else
                    if vb>90,vb=90;end
                    count2=1;
                end
            end
        end
        
        if j==7
            if tcore(j)<36.6 && tsk>34.000
                if (j~=4 && vb>=91) || (j==4 && vb<89)
                else
                    if vb>90,vb=90;end
                    count2=1;
                end
            end
        end
        
        if j==4
            if (j~=4 && vb>=91) || (j==4 && vb<89)
            else
                if vb>90,vb=90;end
                count2=1;
            end
        end
        
        j=j+1;
        
        % !       water losts
        %         ws=sw*3600*1000
        %         if ws>2000 ws=2000
        %         wd=ed/evap*3600*(-1000)
        %         wsum=ws+wr+wd
        %         goto 100
        % 90      continue
        % 100     return
    end
    
    % PET_cal
    tx=ta;
    enbal2=0;
    count1=0;
    enbal=0;
    %150
    hc=2.67+6.5*0.1^0.67;
    hc=hc*(p/po)^0.55;
    while count1<=3
        while (enbal*enbal2)>=0
            enbal2=enbal;% Uppflyttad
            %       radiation balance
            rbare=aeff*(1-facl)*emsk*sigma*((tx+273.2)^4-(tsk+273.2)^4);
            rclo=feff*acl*emcl*sigma*((tx+273.2)^4-(tcl+273.2)^4);
            rsum=rbare+rclo;
            
            %       convection
            cbare=hc*(tx-tsk)*adu*(1-facl);
            cclo=hc*(tx-tcl)*acl;
            csum=cbare+cclo;
            
            %       diffusion
            ed=evap/(rdsk+rdcl)*adu*(1-wetsk)*(12-vpts);
            
            %       respiration
            tex=0.47*tx+21;
            eres=cair*(tx-tex)*rtv;
            vpex=6.11*10^(7.45*tex/(235+tex));
            erel=0.623*evap/p*(12-vpex)*rtv;
            ere=eres+erel;
            
            %       energy balance
            enbal=h+ed+ere+esw+csum+rsum;
            
            %       iteration concerning Tx
            if count1==0, xx=1;end
            if count1==1, xx=0.1;end
            if count1==2, xx=0.01;end
            if count1==3, xx=0.001;end
            if enbal>0, tx=tx-xx;end
            if enbal<0, tx=tx+xx;end
            
            % 	if enbal<=0 .and. enbal2>0 goto 160
            % 	if enbal>=0 .and. enbal2<0 goto 160
            
            
        end
        %            goto 150
        % 160
        count1=count1+1;
        enbal2=0;
    end
    % 	if count1=4 goto 170
    %            goto 150
    % 170     return
    %         end
    petout=tx;
end
