function [iazimuth aziinterval]=svf_angles_100121(iangle)

azi1=1:360/16:360;      %22.5
azi2=12:360/16:360;     %22.5
azi3=5:360/32:360;      %11.25
azi4=2:360/32:360;      %11.25
azi5=4:360/40:360;      %9
azi6=7:360/48:360;      %7.50
azi7=6:360/48:360;      %7.50
azi8=1:360/48:360;      %7.50
azi9=4:360/52:359;      %6.9231
azi10=5:360/52:360;     %6.9231
azi11=1:360/48:360;     %7.50
azi12=0:360/44:359;     %8.1818
azi13=3:360/44:360;     %8.1818
azi14=2:360/40:360;     %9
azi15=7:360/32:360;     %10
azi16=3:360/24:360;    %11.25
azi17=10:360/16:360;    %15
azi18=19:360/12:360;    %22.5
azi19=17:360/8:360;     %45
azi20=0;                %360
iazimuth=[azi1 azi2 azi3 azi4 azi5 azi6 azi7 azi8 azi9 azi10...
    azi11 azi12 azi13 azi14 azi15 azi16 azi17 azi18 azi19 azi20];
aziinterval=[16 16 32 32 40 48 48 48 52 52 48 44 44 40 32 24 16 12 8 1];
%% This cell is for polarplot
% altitude=[];
% for i=1:length(aziinterval)
%     altitude=[altitude iangle(i)*ones(1,aziinterval(i))];
% end
% subplot(2,2,1)
% polar2(iazimuth*(pi/180),(altitude*-1)+90,[0 90],'.k')
% set(gca,'YDir','reverse','FontSize',8,'View',[270 90],'XTickLabel','')
% x=0.5:1:359.5;
% p=histc(iazimuth,x);
% subplot(2,2,2),plot(p,'.'); figure(gcf)
% set(gca,'Xlim',[0 360],'Ylim',[0 6])
% subplot(2,2,3),hist(p,5)
% subplot(2,2,4),hist(iazimuth,16)

