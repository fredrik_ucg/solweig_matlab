function [a,scale,header,headernum,headername,sizey,sizex]=Solweig_10_loaddem(inputdata)
% This function loads an EsriasciiGrid
% It separates the file into header and grid. It also output spatial 
% information such as scale and domain size
%
% Input
% Inputdata = Path to EsriAsciiFile

[a, header]=Esriasciiimport(inputdata);

sizex=size(a,2);
sizey=size(a,1);

% put the headernumber and name in vectors
for i=1:6
    [cell,rest]=strtok(header{i},' ');
    headernum(i,:)=cellstr(rest);
    headername(i,:)=cellstr(cell);
    if i == 5
        scale=1/str2num(rest);
    end
end

