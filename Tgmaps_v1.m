function [TgK,Tstart,alb_grid,emis_grid,TgK_wall,Tstart_wall,TmaxLST,TmaxLST_wall] = Tgmaps_v1(lc_grid,lc_class)

%Tgmaps_v1 Populates grids with cooeficients for Tg wave
%   Detailed explanation goes here

id=unique(lc_grid);
TgK=lc_grid;
Tstart=lc_grid;
alb_grid=lc_grid;
emis_grid=lc_grid;
TmaxLST=lc_grid;

for i=1:size(id,1)
    row=lc_class(:,1)==id(i);
    TgK(TgK==id(i))=lc_class(row,4);
    Tstart(Tstart==id(i))=lc_class(row,5);
    alb_grid(alb_grid==id(i))=lc_class(row,2);
    emis_grid(emis_grid==id(i))=lc_class(row,3);
    TmaxLST(TmaxLST==id(i))=lc_class(row,6);
end

wall_pos=lc_class(:,1)==99;
TgK_wall=lc_class(wall_pos,4);
Tstart_wall=lc_class(wall_pos,5);
TmaxLST_wall=lc_class(wall_pos,6);
    
