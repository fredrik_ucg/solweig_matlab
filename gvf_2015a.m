function [gvfLup,gvfalb,gvfalbnosh,gvfLupE,gvfalbE,gvfalbnoshE,gvfLupS,gvfalbS,gvfalbnoshS,gvfLupW,gvfalbW,gvfalbnoshW,gvfLupN,gvfalbN,gvfalbnoshN]=gvf_2015a(wallsun,walls,buildings,scale,shadow,first,second,dirwalls,Tg,Tgwall,Ta,emis_grid,ewall,alb_grid,SBC,albedo_b,sizey,sizex,Twater,lc_grid,landcover)

azimuthA=5:20:359; %Search directions for Ground View Factors (GVF)

%%%% Ground View Factors %%%%
gvfLup=zeros(sizey,sizex);
gvfalb=zeros(sizey,sizex);
gvfalbnosh=zeros(sizey,sizex);
gvfLupE=gvfLup;gvfLupS=gvfLup;gvfLupW=gvfLup;gvfLupN=gvfLup;
gvfalbE=gvfalb;gvfalbS=gvfalb;gvfalbW=gvfalb;gvfalbN=gvfalb;
gvfalbnoshE=gvfalbnosh;gvfalbnoshS=gvfalbnosh;gvfalbnoshW=gvfalbnosh;gvfalbnoshN=gvfalbnosh;
%gvf=zeros(sizey,sizex);
%         sunwall=wallinsun_2015a(buildings,azimuth(i),shadow,psi(i),dirwalls,walls);
sunwall=(wallsun./walls.*buildings)==1; % new as from 2015a
for j=1:length(azimuthA)
    [~,gvfLupi,gvfalbi,gvfalbnoshi] = sunonsurface_2015a(azimuthA(j),scale,buildings,shadow,sunwall,first,second,...
        dirwalls*pi/180,walls,Tg,Tgwall,Ta,emis_grid,ewall,alb_grid,SBC,albedo_b,Twater,lc_grid,landcover);
    
    gvfLup=gvfLup+gvfLupi;
    gvfalb=gvfalb+gvfalbi;
    gvfalbnosh=gvfalbnosh+gvfalbnoshi;
    
     if azimuthA(j) >= 0  &&  azimuthA(j) < 180
        gvfLupE=gvfLupE+gvfLupi;
        gvfalbE=gvfalbE+gvfalbi;
        gvfalbnoshE=gvfalbnoshE+gvfalbnoshi;
    end
    if azimuthA(j) >= 90  &&  azimuthA(j) < 270
        gvfLupS=gvfLupS+gvfLupi;
        gvfalbS=gvfalbS+gvfalbi;
        gvfalbnoshS=gvfalbnoshS+gvfalbnoshi;
    end
    if azimuthA(j) >= 180  &&  azimuthA(j) < 360
        gvfLupW=gvfLupW+gvfLupi;
        gvfalbW=gvfalbW+gvfalbi;
        gvfalbnoshW=gvfalbnoshW+gvfalbnoshi;
    end
    if azimuthA(j) >= 270  ||  azimuthA(j) < 90
        gvfLupN=gvfLupN+gvfLupi;
        gvfalbN=gvfalbN+gvfalbi;
        gvfalbnoshN=gvfalbnoshN+gvfalbnoshi;
    end
end

gvfLup=gvfLup/length(azimuthA)+SBC*emis_grid.*(Ta+273.15).^4;
gvfalb=gvfalb/length(azimuthA);
gvfalbnosh=gvfalbnosh/length(azimuthA);

gvfLupE=gvfLupE/(length(azimuthA)/2)+SBC*emis_grid.*(Ta+273.15).^4;
gvfLupS=gvfLupS/(length(azimuthA)/2)+SBC*emis_grid.*(Ta+273.15).^4;
gvfLupW=gvfLupW/(length(azimuthA)/2)+SBC*emis_grid.*(Ta+273.15).^4;
gvfLupN=gvfLupN/(length(azimuthA)/2)+SBC*emis_grid.*(Ta+273.15).^4;

gvfalbE=gvfalbE/(length(azimuthA)/2);
gvfalbS=gvfalbS/(length(azimuthA)/2);
gvfalbW=gvfalbW/(length(azimuthA)/2);
gvfalbN=gvfalbN/(length(azimuthA)/2);

gvfalbnoshE=gvfalbnoshE/(length(azimuthA)/2);
gvfalbnoshS=gvfalbnoshS/(length(azimuthA)/2);
gvfalbnoshW=gvfalbnoshW/(length(azimuthA)/2);
gvfalbnoshN=gvfalbnoshN/(length(azimuthA)/2);
