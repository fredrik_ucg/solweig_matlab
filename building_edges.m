function [build,index,loctemp,walls]=building_edges(a,sizey,sizex)
%% Finding Building walls one pixel inside building footprints

wallbol=findbuildingedges(a);
g=wallbol;
g(1:sizey,1)=1;
g(1:sizey,sizex)=1;
g(1,1:sizex)=1;
g(sizey,1:sizex)=1;
g=g(1:sizey,1:sizex);
index=2;
build=[];
build(:,:,index)=g;
build(:,:,index-1)=g;
build=logical(build);
loctemp=[];
walls=double(wallbol);
