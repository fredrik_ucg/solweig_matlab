function [sos] = sunonsurface_veg(azimuthA,scale,buildings,sh,first,second,sunwall,vegsh,psi)
% This version of sunonsurfce goes with SOLWEIG 1.1

% conversion into radians
azimuth=azimuthA*(pi/180);

% loop parameters
index=0;
f=buildings;
sh=sh-(1-vegsh)*(1-psi);
% shadow=sh-(1-vegsh)*(1-psi);
dx=0;
dy=0;
ds=0; %#ok<NASGU>
sizex=size(sh,1);sizey=size(sh,2);
tempsh=zeros(sizex,sizey);
tempbu=tempsh;
tempbub=tempsh;
tempwallsun=tempsh;
weightsumsh=tempsh;
weightsumwall=tempsh;
first=first*scale;
second=second*scale;


% other loop parameters
pibyfour=pi/4;
threetimespibyfour=3*pibyfour;
fivetimespibyfour=5*pibyfour;
seventimespibyfour=7*pibyfour;
sinazimuth=sin(azimuth);
cosazimuth=cos(azimuth);
tanazimuth=tan(azimuth);
signsinazimuth=sign(sinazimuth);
signcosazimuth=sign(cosazimuth);
dssin=abs(1/sinazimuth);
dscos=abs(1/cosazimuth);

%% The Shadow casting algoritm
for n=1:second
    if ((pibyfour<=azimuth)&&(azimuth<threetimespibyfour))||((fivetimespibyfour<=azimuth)&&(azimuth<seventimespibyfour))
        dy=signsinazimuth*index;
        dx=-1*signcosazimuth*abs(round(index/tanazimuth));
        ds=dssin;
    else
        dy=signsinazimuth*abs(round(index*tanazimuth));
        dx=-1*signcosazimuth*index;
        ds=dscos;
    end

    % note: dx and dy represent absolute values while ds is an incremental value

    absdx=abs(dx);
    absdy=abs(dy);

    xc1=((dx+absdx)/2)+1;
    xc2=(sizex+(dx-absdx)/2);
    yc1=((dy+absdy)/2)+1;
    yc2=(sizey+(dy-absdy)/2);

    xp1=-((dx-absdx)/2)+1;
    xp2=(sizex-(dx+absdx)/2);
    yp1=-((dy-absdy)/2)+1;
    yp2=(sizey-(dy+absdy)/2);

    tempbu(xp1:xp2,yp1:yp2)=buildings(xc1:xc2,yc1:yc2);%moving building

    tempsh(xp1:xp2,yp1:yp2)=sh(xc1:xc2,yc1:yc2);%moving shadow image
    f=min(f,tempbu);%utsmetning av buildings

    shadow=tempsh.*f;
    weightsumsh=weightsumsh+shadow;

    tempwallsun(xp1:xp2,yp1:yp2)=sunwall(xc1:xc2,yc1:yc2);%moving buildingwall insun image
    tempb=tempwallsun.*f;
    tempbub=(tempb+tempbub)>0==1;
    weightsumwall=weightsumwall+tempbub;

    %     subplot(2,2,1)
    %     imagesc(shadow),colorbar,axis image, axis off
    %     subplot(2,2,2)
    %     imagesc(weightsumwall+weightsumsh),colorbar,axis image, axis off
    %     subplot(2,2,3)
    %     imagesc(weightsumsh),colorbar,axis image, axis off
    %     subplot(2,2,4)
    %     imagesc(weightsumwall),colorbar,axis image, axis off
    %     pause(0.001)

    if index*scale==first
        gvf1=(weightsumwall+weightsumsh)/first;
        gvf1(gvf1>1)=1;
    end
    index=index+1;

end
gvf2=(weightsumsh+weightsumwall)/second;
gvf2(gvf2>1)=1;

% Weighting
sos=(gvf1*0.5+gvf2*0.4)/0.9;

