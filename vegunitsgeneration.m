function [vegunit,vegdem,vegdem2]=vegunitsgeneration(a,sizex,sizey,buildings,vegdem,vegdem2,vegcoord_out,id,ttype,dia,height,trunk,auto,rowa,cola,scale,handles)
% This function creates the shape of each vegetation unit and locates it a
% grid. It also generates each trunk zone underneath each unit.

clim=[min(min(a+vegdem)) max(max(a+vegdem))];
if range(clim)==0, clim(2)=1; end
vegdemtemp=zeros(size(a,1),size(a,2));
vegdem2temp=vegdemtemp;
dia=dia*scale;
if ttype == 1 % conifer tree
    trees=conifertree(dia);
    trees=trees*(height-trunk);
    circle=imcircle(dia);
    trees=circle.*(trees+trunk);
    treetrunkunder=trunk*circle;
    
elseif ttype == 2 % desiduous tree
    trees=conifertree(dia);
    canopy=1-((1-trees).^2);
    trees=canopy*(height-trunk);
    circle=imcircle(dia);
    trees=circle.*(trees+trunk);
    treetrunkunder=circle*trunk;
    
elseif ttype == 3 % bush
    trees=conifertree(dia);
    canopy=1-((1-trees).^2);
    trees=canopy*(height-trunk);
    circle=imcircle(dia);
    trees=circle.*trees;
    treetrunkunder=circle*0.1;
end

if auto == 0
    b=a;
    imagesc(a(1:sizey,1:sizex)+vegdem(1:sizey,1:sizex),'parent',handles.axes2), axis image, hold on
    for i=1:length(vegcoord_out(:,7))
        if vegcoord_out(i,1)>0
            text(vegcoord_out(i,7),vegcoord_out(i,6),int2str(vegcoord_out(i,1)),'Color',[1 1 1],'FontSize',10);
        end
    end
    
    A=[-1 -1];
    while (A(1,1)<0) || (A(1,2)<0) || (A(1,1) > sizex) || (A(1,2) > sizey)
        A=ginput(1);
    end
    
    col=floor(A(1,1));
    row=floor(A(1,2));
    col1=col-(floor(dia/2));
    row1=row-(floor(dia/2));
else
    col1=cola-(floor(dia/2));
    row1=rowa-(floor(dia/2));
end

rowmin=abs(row1);
colmin=abs(col1);
rowmax=length(trees);
colmax=length(trees);

% cutting trees at dem egde H�R�RJAG
rowcutmax=length(trees);colcutmax=rowcutmax;rowcutmin=1;colcutmin=1;
if row1+length(trees)-1>size(vegdem,1)
    rowcutmax=length(trees)-abs(size(vegdem,1)-(row1+length(trees)-1));
    rowmax=size(vegdem,1);
end
if col1+length(trees)-1>size(vegdem,2)
    colcutmax=length(trees)-abs(size(vegdem,2)-(col1+length(trees)-1));
    colmax=size(vegdem,2);
end
if row1<1
    rowcutmin=abs(row1);
    rowmin=1;
    if rowcutmin==0,rowcutmin=1;end
end
if col1<1
    colcutmin=abs(col1);
    colmin=1;
    if colcutmin==0,colcutmin=1;end
end
if row1<1 || col1<1 || row1+length(trees)-1>size(vegdem,1) ||col1+length(trees)-1>size(vegdem,2)
    % cutting tree at dem edge
    trees=trees(rowcutmin:rowcutmax,colcutmin:colcutmax);
    treetrunkunder=treetrunkunder(rowcutmin:rowcutmax,colcutmin:colcutmax);
    vegdemtemp(rowmin:rowmin+size(trees,1)-1,colmin:colmin+size(trees,2)-1)=trees;
    vegdem2temp(rowmin:rowmin+size(trees,1)-1,colmin:colmin+size(trees,2)-1)=treetrunkunder;
else
    % no cutting of tree at dem edge
    vegdemtemp(rowmin:rowmin+rowmax-1,colmin:colmin+colmax-1)=trees;
    vegdem2temp(rowmin:rowmin+rowmax-1,colmin:colmin+colmax-1)=treetrunkunder;
end

vegdem=max(vegdem,vegdemtemp);
vegdem=vegdem(1:size(buildings,1),1:size(buildings,2)).*buildings; %remove vegetation from building pixels
vegdem2temp(vegdemtemp==0)=-1000;
vegdem2(vegdem2==0)=-1000;
vegdem2=max(vegdem2,vegdem2temp);
vegdem2=vegdem2(1:size(buildings,1),1:size(buildings,2)).*buildings; %remove vegetation from building pixels
vegdem2(vegdem2==-1000)=0;

if auto == 0
    e=b+vegdem;
    imagesc(e(1:sizey,1:sizex),clim), axis image, hold on
    text(col1+(floor(dia/2)),row1+(floor(dia/2)),int2str(id),'Color',[1 1 1],'FontSize',10);
    for i=1:length(vegcoord_out(:,7))
        if vegcoord_out(i,1)>0
            text(vegcoord_out(i,7),vegcoord_out(i,6),int2str(vegcoord_out(i,1)),'Color',[1 1 1],'FontSize',10);
        end
    end
    pause(0.5)
end

vegunit=[id,ttype,dia/scale,height,trunk,row1+(floor(dia/2)),col1+(floor(dia/2))];
