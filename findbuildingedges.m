function [wallbol]=findbuildingedges(a)
% This function identifies building walls 
% removal of -9999 included
% new version using maxfilter 2012-02-11

a(a==-9999)=0;

% g=ordfilt2(a,9,ones(3,3));
% g=g-a;
% g(g<2)=0;%walls identified when higher than 2m
% g(g>0)=1;
% g=logical(g);

% g=ordfilt2(a,4,[0 1 0; 1 0 1; 0 1 0]);
% g=g-a;
% g(g>2)=0;%walls identified when higher than 2m
% % g=ordfilt2(a,9,ones(3,3));
% % g=g-a;
% % g(g<2)=0;
% g(g>0)=1;
% g=logical(g);

g=ordfilt2(a,1,[0 1 0; 1 0 1; 0 1 0]); %inside buildings,yes?
g=a-g;
g(g<3)=0;%walls identified when higher than 2m (changed to 3m, 20141031)
g(g>0)=1;

g(1:size(g,1),1)=0;
g(1:size(g,1),size(g,2))=0;
g(1,1:size(g,2))=0;
g(size(g,1),1:size(g,2))=0;

wallbol=logical(g);

% g=g*-1+1;
% thres=2/((max(max(a)))-(min(min(a))));
% if thres==Inf
%     thres=0;
% end
% thres=[0 thres];
% if range(thres)>0
%     g=edge(a,'canny');
% %     g=edge(a,'canny',thres);
% else
%     g=a;
% end

% thres=2/((max(max(a)))-(min(min(a))));
% thres=[0 thres];
% g=(edge(a,'canny',thres));
% g = edge(a,'log',thres);
% [refvec]=GreatCircleDistance(location);
% [ASPECT,SLOPE]=gradientm(a,refvec);
% g=SLOPE>30;
% g=imfill(g,'holes');

% g=edge(a,'canny');
% h=edge(a,'log');
% g=h+g;
% g(g==2)=1;