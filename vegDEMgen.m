function [vegcoord_out,vegdem,vegdem2,id]=vegDEMgen(a,sizex,sizey,buildings,vegdem,vegdem2,id,ttype,dia,height,trunk,auto,vegcoord_out,scale,handles)
rowa=[];cola=[];
[vegunit,vegdem,vegdem2]=vegunitsgeneration(a,sizex,sizey,buildings,vegdem,vegdem2,vegcoord_out,id,ttype,dia,height,trunk,auto,rowa,cola,scale,handles);
vegcoord_out(id,1:7)=vegunit;
id=id+1;
