function [radI, radD]=diffusefraction(radG,altitude,Kt,Ta,RH)

% This function estimates diffuse and directbeam radiation according to
% Reindl et al (1990), Solar Energy 45:1

alfa=altitude*(pi/180);


if Ta<=-99 || RH<=-99 || isnan(Ta) || isnan(RH)
    if Kt<=0.3
        radD=radG*(1.020-0.248*Kt);
    elseif Kt>0.3 && Kt<0.78
        radD=radG*(1.45-1.67*Kt);
    elseif Kt>=0.78
        radD=radG*0.147;
    end
else
    RH=RH/100;
    if Kt<=0.3
        radD=radG*(1-0.232*Kt+0.0239*sin(alfa)-0.000682*Ta+0.0195*RH);
    elseif Kt>0.3 && Kt<0.78
        radD=radG*(1.329-1.716*Kt+0.267*sin(alfa)-0.00357*Ta+0.106*RH);
    elseif Kt>=0.78
        radD=radG*(0.426*Kt-0.256*sin(alfa)+0.00349*Ta+0.0734*RH);
    end
end
radI=(radG-radD)/(sin(alfa));

%% Corrections for low sun altitudes (20130307)
if radI<0
    radI=0;
end

if altitude<1 && radI>radG
    radI=radG;
end

if radD>radG
    radD=radG;
end