function [modva]=Solweig_2015a_core(outputfolder,a,scale,header,sizey,sizex,row,col,svf,svfN,svfW,svfE,svfS,svfveg,svfNveg,svfEveg,svfSveg,svfWveg,svfaveg,svfEaveg,svfSaveg,svfWaveg,svfNaveg,vegdem,vegdem2,albedo_b,albedo_g,absK,absL,ewall,eground,Fside,Fup,PA,met,YYYY,altitude,azimuth,zen,jday,showimage,usevegdem,onlyglobal,buildings,location,height,trans,output,fileformat,landcover,sensorheight,leafon,lc_grid,lc_class,dectime,altmax,dirwalls,walls,cyl,elvis)
% This is the core function of the SOLWEIG model
% 2015-05-12
% Fredrik Lindberg, fredrikl@gvc.gu.se
% G�teborg Urban Climate Group
% Gothenburg University
%
% Input variables:
% a = digital surface model
% scale = height to pixel size (2m pixel gives scale = 0.5)
% header = ESRI Ascii Grid header
% sizey,sizex = no. of pixels in x and y
% x,y = point of interest
% svf,svfN,svfW,svfE,svfS = SVFs for building and ground
% svfveg,svfNveg,svfEveg,svfSveg,svfWveg = Veg SVFs blocking sky
% svfaveg,svfEaveg,svfSaveg,svfWaveg,svfNaveg = Veg SVFs blocking buildings
% vegdem = Vegetation canopy DSM
% vegdem2 = Vegetation trunk zone DSM
% albedo_b = buildings
% albedo_g = ground (if landcover==0)
% absK = human absorption coefficient for shortwave radiation
% absL = human absorption coefficient for longwave radiation
% ewall = Emissivity of building walls
% eground = Emissivity of ground (if landcover==0)
% Fside = The angular factors between a person and the surrounding surfaces
% Fup = The angular factors between a person and the surrounding surfaces
% PA = Posture of a human
% met = meteorological inputdata
% YYYY = Year
% altitude = Sun altitude (degree)
% azimuth = Sun azimuth (degree)
% zen = Sun zenith angle (radians)
% jday = day of year
% showimage = show image during execuation
% usevegdem = use vegetation scheme
% onlyglobal = calculate dir and diff from global
% buildings = Boolena grid to identify building pixels
% location = geographic location
% height = height of measurments point
% trans Trensmissivity of shortwave theough vegetation
% output = output settings
% fileformat = fileformat of output grids
% landcover = use landcover scheme !!!NEW IN 2015a!!!
% sensorheight = Sensorheight of wind sensor
% leafon = foliated vegetation or not
% lc_grid = grid with landcoverclasses
% lc_class = table with landcover properties
% dectime = decimal time
% altmax = maximum sun altitude
% dirwalls = aspect of walls
% walls = one pixel row outside building footprints
% cyl = consider man as cyliner instead of cude

%% Core program start
%Initialization output textfile
if isempty(row)
else
    % This is settings for calulating PET at POI
    pet.mbody=75;
    pet.age=35;
    pet.height=1.80;
    pet.activity=80;
    pet.sex=1;
    pet.clo=0.9;
    if(exist(outputfolder,'dir')==0)
        mkdir(outputfolder);
    end
    fn=fopen([outputfolder 'Output_v2015a_POI_' PA '.txt'],'w');
    fprintf(fn,'%9s','year','DOY','hour','dectime','altitude','azimuth','Kdirect','Kdiffuse','Kglobal','Kdown','Kup',...
        'KsideI','Knorth','Keast','Ksouth','Kwest','Ldown','Lup','Lnorth','Least','Lsouth','Lwest','Ta','Tg',...
        'RH','Ea','Esky','Sstr','Tmrt','I0','CI','gvf','CITg','Shadow','SVF_b','SVF_b+v','PET','UTCI');
    fprintf(fn,'\r\n');
end

%Imagescaling and plotting vectors
clim=[-10 70];
modva=zeros(size(met,1),33);

%Instrument offset in degrees
t=0;

%Stefan Bolzmans Constant
SBC=5.67051e-8;

%Surface to air temperature difference at sunrise
% Tstart=0;%3.41; % dynamic as from 2015a

%Initialization of maps
Knight=zeros(sizey,sizex);
Tmrtday=zeros(sizey,sizex);Lupday=Tmrtday;Ldownday=Tmrtday;Kupday=Tmrtday;
Kdownday=Tmrtday;gvfday=Tmrtday;
Tmrtdiurn=Tmrtday;Lupdiurn=Tmrtday;Ldowndiurn=Tmrtday;
Tgmap1=Knight;Tgmap1E=Knight;Tgmap1S=Knight;Tgmap1W=Knight;Tgmap1N=Knight;

%Surface to air temperature difference at sunrise
% Tstart=3.41; % dynamic as from 2015a
% Ts parameterisation maps
if landcover==1
    [TgK,Tstart,alb_grid,emis_grid,TgK_wall,Tstart_wall,TmaxLST,TmaxLST_wall]=Tgmaps_v1(lc_grid,lc_class);
else
    TgK=Knight+0.37;
    Tstart=Knight-3.41;
    alb_grid=Knight+albedo_g;
    emis_grid=Knight+eground;
    TgK_wall=0.37;
    Tstart_wall=-3.41;
    TmaxLST=15;
    TmaxLST_wall=15;
end

tmp=(svf+svfveg-1);tmp(tmp<0)=0; %matlab crazyness around 0
svfalfa=asin(exp((log(1-(tmp)))/2));clear tmp

if showimage == 1
    screensize=get(0,'ScreenSize');
    screenwidth=screensize(3);
    screenheight=screensize(4);
    figure('Position',[(screenwidth-0.85*screenwidth) (screenheight-0.85*screenheight) (screenheight*0.7) (screenheight*0.6)],'color',[1 1 1],'Name',' ','Numbertitle','off','Toolbar','none','menubar','none');
end

%Creating vectors from meteorological input (2013b)
% year=time(:,1);month=time(:,2);day=time(:,3);%min=met(:,5);
DOY=met(:,2);hour=met(:,3);
Ta=met(:,12);RH=met(:,11);%dectime=met(:,3);
radG=met(:,15);radD=met(:,22);radI=met(:,23);P=met(:,13);
Ws=met(:,10);%Wd=met(:,13);
daytime=find(altitude>0);daytime=length(daytime); %Number of daytime hours
timestepdec=dectime(2)-dectime(1);
timeadd=0;timeaddE=0;timeaddS=0;timeaddW=0;timeaddN=0;
firstdaytime=1; %bugfix so that model can start during daytime

% If metfile starts at night
    CI=1;

%Parameterisarion for Lup
if isempty(height),height=1.1;else end
first=round(height); %Radiative surface influence, Rule of thumb by Schmid et al. (1990).
if first==0, first=1; end
second=round(height*20);
if usevegdem==1
    % Vegetation transmittivity of shortwave radiation
    psi=leafon*trans;
    psi(leafon==0)=0.5;
    
    % amaxvalue (used in calculation of vegetation shadows)
    vegmax=max(vegdem(:));
    amaxvalue=range(a(:));
    amaxvalue=max(amaxvalue,vegmax);
    
    % Elevation vegdems if buildingDSM includes ground heights
    vegdem=vegdem+a;vegdem(vegdem==a)=0;
    vegdem2=vegdem2+a;vegdem2(vegdem2==a)=0;
    
    % Bush separation
    bush=not(vegdem2.*vegdem).*vegdem;
else
    psi=leafon*0+1;
end

svfbuveg=(svf-(1-svfveg)*(1-trans)); % major bug fixed 20141203

% If Buildings is empty, building walls will be created using an edge function.
if isempty(buildings)
    buildings=findbuildingedges(a);
    buildings=buildings*(-1)+1;
    buildings=double(buildings);
end

% Main loop
for i=1:length(altitude)

    %Show the progressbar if the images are not shown
    if showimage == 0
        progressbar(i/length(altitude),0);
    end
    
    % Find sunrise decimal hour - new in 2014a
    [ ~, ~, ~, SNUP ] = daylen( jday(i), location.latitude );
    
    %Vapor pressure
    ea=6.107*10^((7.5*Ta(i))/(237.3+Ta(i)))*(RH(i)/100);
    
    %Determination of clear-sky emissivity from Prata (1996)
    msteg=46.5*(ea/(Ta(i)+273.15));
    esky=(1-(1+msteg)*exp(-((1.2+3.0*msteg)^0.5)))+elvis;%-0.04; old error from jonsson et al. 2006
    
    %Daily water body temperature
    if landcover == 1
        if (dectime(i)-floor(dectime(i)))==0 || i==1
            Twater=Twater_2015a(Ta,jday,i);
        end
    else
        Twater=[];
    end
    if altitude(i)>0 %%%%%% DAYTIME %%%%%%
        %Clearness Index on Earth's surface after Crawford and Dunchon (1999) with a correction
        %factor for low sun elevations after Lindberg et al. (2008)
        [I0, CI, Kt,~,~]=clearnessindex_2013b(zen(1,i),jday(1,i),Ta(i),RH(i)/100,radG(i),location,P(i));
        if CI>1 && CI==inf,CI=1;end
        
        %Estimation of radD and radI if not measured after Reindl et al. (1990)
        if onlyglobal == 1
            [radI(i), radD(i)]=diffusefraction(radG(i),altitude(i),Kt,Ta(i),RH(i));
        end
        
        %Shadow images
        if usevegdem==1
            [vegsh,sh,~,~,wallsun,~,~,~,walls]=shadowingfunction_wallheight_23(a,vegdem,vegdem2,azimuth(1,i),altitude(1,i),scale,amaxvalue,bush,walls,dirwalls*pi/180);
            shadow=sh-(1-vegsh)*(1-psi(i));
        else
            [sh , ~, wallsun , ~, ~]=shadowingfunction_wallheight_13(a,azimuth(1,i),altitude(1,i),scale,walls,dirwalls*pi/180);
            shadow=sh;
        end
        
        %%% Surface temperature parameterisation during daytime %%%%
        % new using max sun alt. instead of dfm
        Tgamp=(TgK*altmax(1,i)-Tstart)+Tstart;
%         Tgampwall=(0.67*altmax(1,i)-(-5.05))+(-5.05);%0.47%-3.41
        Tgampwall=(TgK_wall*altmax(1,i)-(Tstart_wall))+(Tstart_wall);
        Tg=Tgamp.*sin((((dectime(i)-floor(dectime(i)))-SNUP/24)./(TmaxLST./24-SNUP/24))*pi/2)+Tstart; % 2015a, based on max sun altitude
        Tgwall=Tgampwall.*sin((((dectime(i)-floor(dectime(i)))-SNUP/24)./(TmaxLST_wall./24-SNUP/24))*pi/2)+(Tstart_wall); % 2015a, based on max sun altitude
        
        if Tgwall<0 % temporary for removing low Tg during morning 20130205
%             Tg=0;
            Tgwall=0;
        end
        
        %         % old
        %         Tstartold=3.41;
        %         dfm=abs(172-jday(i)); %Day from midsommer
        %         Tgampold=0.000006*dfm^3-0.0017*dfm^2+0.0127*dfm+17.084+Tstartold; %sinus function for daily surface temperature wave
        %         Tg=Tgampold*sin((((dectime(i)-floor(dectime(i)))-SNUP/24)/(15/24-SNUP/24))*pi/2)-Tstartold; %new sunrise time 2014a
        %
        %         if Tg<0 % temporary for removing Tg<0 during morning 20140513, from SOLWEIG1D
        %             Tg=0;
        %         end
        
        %New estimation of Tg reduction for non-clear situation based on Reindl et al. 1990
        [radI0]=diffusefraction(I0,altitude(i),1,Ta(i),RH(i));
        corr=0.1473*log(90-(zen(i)/pi*180))+0.3454;% 20070329 temporary correction of latitude from Lindberg et al. 2008
        CI_Tg=(radI(i)/radI0)+(1-corr);
        if CI_Tg>1 || CI_Tg==inf ,CI_Tg=1;end
        Tg=Tg*CI_Tg; %new estimation
        Tgwall=Tgwall*CI_Tg;
        if landcover == 1
            Tg(Tg<0)=0; % temporary for removing low Tg during morning 20130205
        end
        %         Tw=Tg;
       
        %%%% Ground View Factors %%%%
        [gvfLup,gvfalb,gvfalbnosh,gvfLupE,gvfalbE,gvfalbnoshE,gvfLupS,...
            gvfalbS,gvfalbnoshS,gvfLupW,gvfalbW,gvfalbnoshW,gvfLupN,...
            gvfalbN,gvfalbnoshN]=gvf_2015a(wallsun,walls,buildings,...
            scale,shadow,first,second,dirwalls,Tg,Tgwall,Ta(i),...
            emis_grid,ewall,alb_grid,SBC,albedo_b,sizey,sizex,Twater,lc_grid,landcover);
        
        %         gvfLup=zeros(sizey,sizex);gvfalb=zeros(sizey,sizex);
        %         gvfalbnosh=zeros(sizey,sizex);%gvf=zeros(sizey,sizex);
        %         %         sunwall=wallinsun_2015a(buildings,azimuth(i),shadow,psi(i),dirwalls,walls);
        %         sunwall=(wallsun./walls.*buildings)==1; % new as from 2015a
        %         for j=1:length(azimuthA)
        %             [~,gvfLupi,gvfalbi,gvfalbnoshi] = sunonsurface_2015a(azimuthA(j),scale,buildings,shadow,sunwall,first,second,...
        %                 dirwalls*pi/180,walls,Tg,Tgwall,Ta(i),emis_grid,ewall,alb_grid,SBC,albedo_b,a);
        %             gvfLup=gvfLup+gvfLupi;
        %             gvfalb=gvfalb+gvfalbi;
        %             gvfalbnosh=gvfalbnosh+gvfalbnoshi;
        %             %             gvf=gvf+gvfi;
        %             %             imagesc(gvfalbnosh),colorbar, axis image,pause(0.05)
        %         end
        %         gvfLup=gvfLup/length(azimuthA)+SBC*emis_grid.*(Ta(i)+273.15).^4;
        %         gvfLup(lc_grid==3)=SBC*0.98.*(Twater+273.15).^4;%should change e?
        %         gvfalb=gvfalb/length(azimuthA);
        %         gvfalbnosh=gvfalbnosh/length(azimuthA);
        %         %         gvf=gvf/length(azimuthA);
        
        %%%% Lup, daytime %%%%
        %Surface temperature wave delay - new as from 2014a
        [Lup,timeadd,Tgmap1] = TsWaveDelay_2015a(gvfLup,firstdaytime,timeadd,timestepdec,Tgmap1);
        [LupE,timeaddE,Tgmap1E] = TsWaveDelay_2015a(gvfLupE,firstdaytime,timeaddE,timestepdec,Tgmap1E);
        [LupS,timeaddS,Tgmap1S] = TsWaveDelay_2015a(gvfLupS,firstdaytime,timeaddS,timestepdec,Tgmap1S);
        [LupW,timeaddW,Tgmap1W] = TsWaveDelay_2015a(gvfLupW,firstdaytime,timeaddW,timestepdec,Tgmap1W);
        [LupN,timeaddN,Tgmap1N] = TsWaveDelay_2015a(gvfLupN,firstdaytime,timeaddN,timestepdec,Tgmap1N);
        
        %         Tgmap0=gvfLup; % current timestep
        %         if firstdaytime==1 %"first in morning"
        %             Tgmap1=Tgmap0;
        %         end
        %         if timeadd>=(59/1440) %more or equal to 59 min
        %             weight1=exp(-33.27*timeadd); %surface temperature delay function - 1 step
        %             Tgmap1=Tgmap0*(1-weight1)+Tgmap1*weight1;
        %             Lup=Tgmap1;
        %             if timestepdec>(59/1440)
        %                 timeadd=timestepdec;
        %             else
        %                 timeadd=0;
        %             end
        %         else
        %             timeadd=timeadd+timestepdec;
        %             weight1=exp(-33.27*timeadd); %surface temperature delay function - 1 step
        %             Lup=(Tgmap0*(1-weight1)+Tgmap1*weight1);
        %         end
        
        %         %%%% Lup, daytime %%%%
        %         %Surface temperature wave delay - new as from 2014a
        %         Tgmap0=gvf.*Tg+Ta(i); % current timestep
        %         if firstdaytime==1 %"first in morning"
        %             Tgmap1=Tgmap0;
        %         end
        %         if timeadd>=(59/1440) %more or equal to 59 min
        %             weight1=exp(-33.27*timeadd); %surface temperature delay function - 1 step
        %             Tgmap1=Tgmap0*(1-weight1)+Tgmap1*weight1;
        %             Lup=SBC*eground*((Tgmap1+273.15).^4);
        %             if timestepdec>(59/1440)
        %                 timeadd=timestepdec;
        %             else
        %                 timeadd=0;
        %             end
        %         else
        %             timeadd=timeadd+timestepdec;
        %             weight1=exp(-33.27*timeadd); %surface temperature delay function - 1 step
        %             Lup=SBC*eground*((Tgmap0*(1-weight1)+Tgmap1*weight1+273.15).^4);
        %         end
        
        %Building height angle from svf
        F_sh=cylindric_wedge(zen(i),svfalfa);%Fraction shadow on building walls based on sun altitude and svf
        F_sh(isnan(F_sh))=0.5;
        
        %%%%%%% Calculation of shortwave daytime radiative fluxes %%%%%%%
        %         Kdown=radI(i)*shadow*sin(altitude(i)*(pi/180))+radD(i)*svfbuveg+...
        %             radG(i)*albedo_b*(1-svfbuveg).*(1-F_sh);%*sin(altitude(i)*(pi/180));
        Kdown=radI(i)*shadow*sin(altitude(i)*(pi/180))+radD(i)*svfbuveg+...
            albedo_b*(1-svfbuveg).*(radG(i)*(1-F_sh)+radD(i)*F_sh);%*sin(altitude(i)*(pi/180));
        
        [Kup,KupE,KupS,KupW,KupN]=Kup_veg_2015a(radI(i),radD(i),radG(i),altitude(i),...
            svfbuveg,albedo_b,F_sh,gvfalb,gvfalbE,gvfalbS,gvfalbW,gvfalbN,...
            gvfalbnosh,gvfalbnoshE,gvfalbnoshS,gvfalbnoshW,gvfalbnoshN);
        
        %Kup=(gvfalb.*radI(i)*sin(altitude(i)*(pi/180)))+(radD(i)*svfbuveg+...
        %    radG(i)*albedo_b*(1-svfbuveg).*(1-F_sh)).*gvfalbnosh;
        
        [Keast,Ksouth,Kwest,Knorth,KsideI]=Kside_veg_v2015a(radI(i),radD(i),radG(i),shadow,svfS,svfW,svfN,svfE,...
            svfEveg,svfSveg,svfWveg,svfNveg,azimuth(i),altitude(i),psi(i),t,albedo_b,F_sh,KupE,KupS,KupW,KupN,cyl);
        
        firstdaytime=0;
        
    else %%%%%%% NIGHTTIME %%%%%%%%
        
        %Nocturnal cloudfraction from Offerle et al. 2003
        if (dectime(i)-floor(dectime(i)))==0
            alt=altitude(1,i:size(altitude,2));
            alt=find(alt>1);
            rise=alt(1);
            [~, CI , ~, ~, ~]=clearnessindex_2013b(zen(1,i+rise),jday(1,i+rise),Ta(i+rise),RH(i+rise)/100,radG(i+rise),location,P(i+rise));
            if CI>1 || CI==inf,CI=1;end
        end
        
        Tgwall=0;CI_Tg=-999;%F_sh=[];Tg=0;
        
        %Nocturnal Kfluxes set to 0
        Kdown=Knight;Kwest=Knight;Kup=Knight;Keast=Knight;Ksouth=Knight;Knorth=Knight;shadow=Knight;KsideI=Knight;
        F_sh=Knight;Tg=Knight;
        
        %%%% Lup %%%%
        Lup=SBC*emis_grid.*((Knight+Ta(i)+Tg+273.15).^4);
        if landcover == 1
            Lup(lc_grid==3)=SBC*0.98.*(Twater+273.15).^4; % nocturnal Water temp
        end
        LupE=Lup;LupS=Lup;LupW=Lup;LupN=Lup;
        
        I0=0;gvfLup=zeros(sizey,sizex);timeadd=0;firstdaytime=1;
    end
    
    %%%% Ldown %%%%
    Ldown=(svf+svfveg-1)*esky*SBC*((Ta(i)+273.15)^4)+(2-svfveg-svfaveg)*ewall*SBC*((Ta(i)+273.15)^4)+...
        (svfaveg-svf)*ewall*SBC*((Ta(i)+273.15+Tgwall)^4)+(2-svf-svfveg)*(1-ewall)*esky*SBC*((Ta(i)+273.15)^4); %Jonsson et al. (2006)
    %Ldown=Ldown-25;% Shown by Jonsson et al. (2006) and Duarte et al. (2006)
    
    if CI < 0.95  %non-clear conditions
        c=1-CI;
        %         Ldown=Ldown*(1-c)+c*SBC*((Ta(i)+273.15)^4);
        Ldown=Ldown*(1-c)+c*((svf+svfveg-1)*SBC*((Ta(i)+273.15)^4)+(2-svfveg-svfaveg)*ewall*SBC*((Ta(i)+273.15)^4)+...
            (svfaveg-svf)*ewall*SBC*((Ta(i)+273.15+Tgwall)^4)+(2-svf-svfveg)*(1-ewall)*SBC*((Ta(i)+273.15)^4));%NOT REALLY TESTED!!! BUT MORE CORRECT?
    end
    
    %%%% Lside %%%%
    [Least,Lsouth,Lwest,Lnorth]=Lside_veg_v2015a(svfS,svfW,svfN,svfE,svfEveg,svfSveg,svfWveg,svfNveg,...
        svfEaveg,svfSaveg,svfWaveg,svfNaveg,azimuth(i),altitude(i),Ta(i),Tgwall,SBC,ewall,Ldown,esky,...
        t,F_sh,CI,LupE,LupS,LupW,LupN);
    
    %%%% Calculation of radiant flux density and Tmrt %%%%
    if cyl == 1 % Human body considered as an cyliner
        Sstr=absK*(KsideI+(Kdown+Kup)*Fup+(Knorth+Keast+Ksouth+Kwest)*Fside)...
            +absL*(Ldown*Fup+Lup*Fup+Lnorth*Fside+Least*Fside+Lsouth*Fside+Lwest*Fside);
%         Knorth=nan;Ksouth=nan;Kwest=nan;Keast=nan;
    else  % Human body considered as a standing cube
%         Knorth=KnorthI+KnorthDG;Keast=KeastI+KeastDG;Ksouth=KsouthI+KsouthDG;Kwest=KwestI+KwestDG;
        Sstr=absK*((Kdown+Kup)*Fup+(Knorth+Keast+Ksouth+Kwest)*Fside)...
            +absL*(Ldown*Fup+Lup*Fup+Lnorth*Fside+Least*Fside+Lsouth*Fside+Lwest*Fside);
%         Sstr=absK*(Kdown*Fup+Kup*Fup+Knorth*Fside+Keast*Fside+Ksouth*Fside+Kwest*Fside)...
%         +absL*(Ldown*Fup+Lup*Fup+Lnorth*Fside+Least*Fside+Lsouth*Fside+Lwest*Fside);
%         KsideI=nan;
    end
    Tmrt=sqrt(sqrt((Sstr/(absL*SBC))))-273.2;
%     Sstr=absK*(Kdown*Fup+Kup*Fup+Knorth*Fside+Keast*Fside+Ksouth*Fside+Kwest*Fside)...
%         +absL*(Ldown*Fup+Lup*Fup+Lnorth*Fside+Least*Fside+Lsouth*Fside+Lwest*Fside);
%     Tmrt=sqrt(sqrt((Sstr/(absL*SBC))))-273.2;

%     subplot(3,4,1),imagesc(absL*(Ldown*Fup+Lup*Fup+Lnorth*Fside+Least*Fside+Lsouth*Fside+Lwest*Fside)),axis image,title('Ltot')
%     date=datevec(datenum([YYYY(i) 0 0 0 0 0])+DOY(i));
%     dec=dectime(i)-floor(dectime(i));
%     date(1,4)=floor(dec*24);
%     date(1,5)=round((dec-date(1,4)/24)*1440);
%     subplot(2,3,1),imagesc(Tmrt),axis image, colorbar,title(['Tmrt, G=' num2str(radG(i)) ' I=' num2str(radI(i)) ' D=' num2str(radD(i))],'FontSize',10)
%     subplot(2,3,2),imagesc(Kup),axis image,colorbar,title('Kup','FontSize',10)
%     subplot(2,3,3),imagesc((Lsouth+Least+Lnorth+Lwest)/4),axis image,colorbar,title('Lside','FontSize',10)
%     subplot(2,3,4),imagesc((Ksouth+Keast+Knorth+Kwest)/4),axis image,colorbar,title(['Kside, Time=' datestr(date,'yyyy/mm/dd HH:MM')],'FontSize',10)
%     subplot(2,3,5),imagesc(Lup),axis image,colorbar,title('Lup','FontSize',10)
%     subplot(2,3,6),imagesc(Kdown),axis image,colorbar,title(['Kdown, Alt=' num2str(altitude(i)) ' Az=' num2str(azimuth(i))],'FontSize',10)
%     pause(0.2)
%     
    %%%% Save hourly images as ESRIGRID and/or TIF %%%%
    if altitude(i)>0
        Tmrtday=Tmrtday+Tmrt;Lupday=Lupday+Lup;Ldownday=Ldownday+Ldown;
        Kupday=Kupday+Kup;Kdownday=Kdownday+Kdown;gvfday=gvfday+gvfLup;
    end
    Tmrtdiurn=Tmrtdiurn+Tmrt;Lupdiurn=Lupdiurn+Lup;Ldowndiurn=Ldowndiurn+Ldown;
    Solweig_outputfiles_hour_2014a(output,fileformat,Tmrt,Kup,Kdown,Lup,Ldown,gvfLup,outputfolder,YYYY(i),DOY(i),hour(i),PA,header,sizey,dectime(i));
    
    %%%% Saving to textfile for Point Of Interest %%%%
    if isempty(row)
    else  
        %Recalculating wind speed based on powerlaw
        WsPET=(1.1/sensorheight)^0.14*Ws(i);
        WsUTCI=(10/sensorheight)^0.14*Ws(i);
        petout=petcalculator3(Ta(i),RH(i),Tmrt(row,col),WsPET,pet);
        UTCI_approx=utci_calculator_solweigpoint(Ta(i),RH(i),Tmrt(row,col),WsUTCI);
        
        fprintf(fn,'%9.3f',YYYY(i),DOY(i),hour(i),dectime(i),altitude(i),azimuth(i),radI(i),radD(i)...
            ,radG(i), Kdown(row,col), Kup(row,col), KsideI(row,col),Knorth(row,col), Keast(row,col), Ksouth(row,col),Kwest(row,col),...
            Ldown(row,col), Lup(row,col), Lnorth(row,col), Least(row,col), Lsouth(row,col), Lwest(row,col),Ta(i),Ta(i)+Tg(row,col),RH(i),...
            ea,esky,Sstr(row,col),Tmrt(row,col),I0,CI,gvfLup(row,col),CI_Tg,shadow(row,col),svf(row,col),svfbuveg(row,col),petout,UTCI_approx);
        fprintf(fn,'\r\n');
        modva(i,:)=[YYYY(i) DOY(i) hour(i) dectime(i) altitude(i) azimuth(i) radI(i) radD(i) ...
            radG(i) Kdown(row,col) Kup(row,col) KsideI(row,col) Knorth(row,col) Keast(row,col) Ksouth(row,col) Kwest(row,col) ...
            Ldown(row,col) Lup(row,col) Lnorth(row,col) Least(row,col) Lsouth(row,col) Lwest(row,col) Ta(i) ...
            RH(i) ea esky Sstr(row,col) Tmrt(row,col) I0 CI Ta(i)+Tg(row,col) CI_Tg shadow(row,col)];
    end
    
    %%%% Image display during execution%%%%
    if showimage == 1
        imagesc(Tmrt,clim),axis image; colorbar, colormap(jet)%
        date=datevec(datenum([YYYY(i) 0 0 0 0 0])+DOY(i));
        dec=dectime(i)-floor(dectime(i));
        date(1,4)=floor(dec*24);
        date(1,5)=round((dec-date(1,4)/24)*1440);
        title(['T_{mrt} (�C) on ' datestr(date,'yyyy/mm/dd HH:MM')],'FontSize',14)
        title(['T_{mrt} (�C) on ' datestr(date,'yyyy/mm/dd HH:MM') ' alt=' num2str(altitude(i)) ' alt=' num2str(azimuth(i))],'FontSize',14)
        hold on
        plot(col,row,'r *');
        pause(0.1);
    end
    
%         limL=[250 500];
%         limK=[0 900];
%         date=datevec(datenum([YYYY(i) 0 0 0 0 0])+DOY(i));
%         dec=dectime(i)-floor(dectime(i));
%         date(1,4)=floor(dec*24);
%         date(1,5)=round((dec-date(1,4)/24)*1440);
%         subplot(2,3,1),imagesc(Lup,limL),axis image, colorbar,title('Lup','FontSize',10)
%         subplot(2,3,2),imagesc(Kup,limK),axis image,colorbar,title('Kup','FontSize',10)
%         subplot(2,3,3),imagesc(Kdown,limK),axis image,colorbar,title('Kdown','FontSize',10)
%         subplot(2,3,4),imagesc(Ldown,limL),axis image,colorbar,title(['Ldown, Time=' datestr(date,'yyyy/mm/dd HH:MM')],'FontSize',10)
%         subplot(2,3,5),imagesc(Tmrt,clim),axis image,colorbar,title('Tmrt','FontSize',10)
%         subplot(2,3,6),imagesc(a-shadow*10),axis image,colorbar,title('shadow','FontSize',10)
%         if i==41
%             4
%         end
%         pause(0.2)
    
end

%Closing the progressbar
if showimage == 0
    progressbar(1,0);
else
    close
end

%Closing text file
if isempty(row)
else
    fclose(fn);
end

%%%% Plotting and saving the results %%%%
Tmrtday=Tmrtday/daytime;Lupday=Lupday/daytime;Ldownday=Ldownday/daytime;
Kupday=Kupday/daytime;Kdownday=Kdownday/daytime;gvfday=gvfday/daytime;
Tmrtdiurn=Tmrtdiurn/length(altitude);Lupdiurn=Lupdiurn/length(altitude);
Ldowndiurn=Ldowndiurn/length(altitude);
Solweig_2015a_output(modva,Tmrtday,row,col,cyl)
Solweig_outputfiles_day_2013b(output,fileformat,Tmrtday,Kupday,Kdownday,Lupday,Ldownday,gvfday,outputfolder,YYYY(1),DOY(1),PA,header,sizey);
Solweig_outputfiles_diurnal_2013b(output,fileformat,Tmrtdiurn,Lupdiurn,Ldowndiurn,svf,svfveg,outputfolder,YYYY(1),DOY(1),PA,header,sizey);



