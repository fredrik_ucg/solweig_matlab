function varargout = NewLocation(varargin)
% NEWLOCATION MATLAB code for NewLocation.fig

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @NewLocation_OpeningFcn, ...
                   'gui_OutputFcn',  @NewLocation_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before NewLocation is made visible.
function NewLocation_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to NewLocation (see VARARGIN)

% Choose default command line output for NewLocation
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes NewLocation wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = NewLocation_OutputFcn(hObject, ~, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

function edit_city_Callback(hObject, ~, handles)
get(hObject,'String');
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function edit_city_CreateFcn(hObject, ~, handles)
% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit_country_Callback(hObject, ~, handles)
get(hObject,'String');
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function edit_country_CreateFcn(hObject, ~, handles)


% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function edit_lon_Callback(hObject, ~, handles)
input = str2double(get(hObject,'String'));
if input>180 || input<-180
    msgboxText{1} =  'Choose a decimal number between -180 and 180';
    msgbox(msgboxText,'Number has to be between -180 and 180', 'error');
end
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function edit_lon_CreateFcn(hObject, ~, handles)
% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit_lat_Callback(hObject, ~, handles)
input = str2double(get(hObject,'String'));
if input>90 || input<-90
    msgboxText{1} =  'Choose a decimal number between -90 and 90';
    msgbox(msgboxText,'Number has to be between -90 and 90', 'error');
end
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function edit_lat_CreateFcn(hObject, ~, handles)
% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit_alt_Callback(hObject, ~, handles)
str2double(get(hObject,'String'));
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function edit_alt_CreateFcn(hObject, ~, handles)
% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit_utc_Callback(hObject, ~, handles)
input = str2double(get(hObject,'String'));
if input>12 || input<-12
    msgboxText{1} =  'Choose a decimal number between -12 and 12';
    msgbox(msgboxText,'Number has to be between -12 and 12', 'error');
end
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function edit_utc_CreateFcn(hObject, ~, handles)
% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on button press in pushbutton_close.
function pushbutton_close_Callback(hObject, ~, handles)
% hObject    handle to pushbutton_closesvf (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% get the main_gui handle (access to the gui)
mainGUIhandle = SOLWEIG;       
% get the data from the gui (all handles inside gui_main)
mainGUIdata  = guidata(mainGUIhandle);

city=get(handles.edit_city,'String');
country=get(handles.edit_country,'String');
lon=str2num(get(handles.edit_lon,'String'));
lat=str2num(get(handles.edit_lat,'String'));
alt=str2num(get(handles.edit_alt,'String'));
utc=str2num(get(handles.edit_utc,'String'));

data=importdata([pwd filesep 'locations.txt'],'\t',1);
textdata=data.textdata;
data=data.data;

data=[data;[lon lat alt utc;]];
textdata=[textdata;{country} {city}];
for i=2:size(textdata,1)
    textdata2{i-1,1}=strcat(textdata{i,1},', ',textdata{i,2});
end
set(mainGUIdata.popupmenu_cityname,'String',textdata2)
set(mainGUIdata.popupmenu_cityname,'Value',size(textdata2,1))
set(mainGUIdata.text_lon,'String',num2str(lon))
set(mainGUIdata.text_lat,'String',num2str(lat))
set(mainGUIdata.text_alt,'String',num2str(alt))
set(mainGUIdata.text_utc,'String',num2str(utc))
set(mainGUIdata.text_lon,'Value',(lon))
set(mainGUIdata.text_lat,'Value',(lat))
set(mainGUIdata.text_alt,'Value',(alt))
set(mainGUIdata.text_utc,'Value',(utc))

guidata(hObject, handles);

fn=fopen([pwd filesep 'locations.txt'],'a+');
fprintf(fn,'\r\n');
fprintf(fn,'%s\t',country,city,num2str(lon),num2str(lat),num2str(alt),num2str(utc));
fclose(fn);

% save changed data back into main_gui
%this line updates the data of the Main Gui
guidata(SOLWEIG, mainGUIdata);
 
% close this gui
close(NewLocation)