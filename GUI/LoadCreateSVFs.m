function varargout = LoadCreateSVFs(varargin)
% LOADCREATESVFS MATLAB code for LoadCreateSVFs.fig

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
    'gui_Singleton',  gui_Singleton, ...
    'gui_OpeningFcn', @LoadCreateSVFs_OpeningFcn, ...
    'gui_OutputFcn',  @LoadCreateSVFs_OutputFcn, ...
    'gui_LayoutFcn',  [] , ...
    'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before LoadCreateSVFs is made visible.
function LoadCreateSVFs_OpeningFcn(hObject, ~, handles, varargin)

% get the main_gui handle (access to the gui)
mainGUIhandle = SOLWEIG;
% get the data from the gui (all handles inside gui_main)
mainGUIdata  = guidata(mainGUIhandle);
%
dsm=get(mainGUIdata.pushbutton_dsm, 'UserData');
veg=get(mainGUIdata.text_step1, 'UserData');
set(handles.uipanel_dsm,'UserData',dsm)
set(handles.uipanel_cdsm,'UserData',veg)

% guidata(hObject, handles);
% oldvegdem=get(mainGUIdata.text_vegdsm,'Value');
% if oldvegdem==0
%     veg=[];
%     set(handles.pushbutton_remove,'Enable','off')
% else
%     veg=get(mainGUIdata.pushbutton_continue, 'UserData');
%     set(handles.pushbutton_save,'UserData',veg);
%     set(handles.popupmenu_id,'Userdata',veg.id)
%     textdata2=veg.vegcoord_out(:,1);
%     set(handles.popupmenu_id,'String',textdata2)
% end
% if isempty(veg)==1
%     veg.vegdem=0;
% end

% Choose default command line output for LoadCreateSVFs
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes LoadCreateSVFs wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = LoadCreateSVFs_OutputFcn(hObject, ~, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in pushbutton_loadsvf.
function pushbutton_loadsvf_Callback(hObject, ~, handles)
[filename, directory] = uigetfile('*.zip','Name of zip file containing buildings SVFs');
unzip([directory,filename],directory)
svffiles{1}=[directory 'svf.asc'];
svffiles{2}=[directory 'svfE.asc'];
svffiles{3}=[directory 'svfS.asc'];
svffiles{4}=[directory 'svfW.asc'];
svffiles{5}=[directory 'svfN.asc'];
[svfA,svfE,svfS,svfW,svfN]=Solweig_10_loadsvf(svffiles);
delete(svffiles{1}, svffiles{2}, svffiles{3}, svffiles{4}, svffiles{5})
svf.svf=svfA;
svf.svfE=svfE;
svf.svfS=svfS;
svf.svfW=svfW;
svf.svfN=svfN;
set(handles.pushbutton_loadsvf,'UserData',svf)
set(handles.text_svf,'String','Building SVF availabe','ForegroundColor',[0 0 1])
% save changed data back into main_gui
%this line updates the data of the Main Gui
guidata(hObject, handles);

% --- Executes on button press in pushbutton_createsvf.
function pushbutton_createsvf_Callback(hObject, ~, handles)
dsm=get(handles.uipanel_dsm,'UserData');

% % get the main_gui handle (access to the gui)
% mainGUIhandle = SOLWEIG;
% % get the data from the gui (all handles inside gui_main)
% mainGUIdata  = guidata(mainGUIhandle);
%
% dsm=get(mainGUIdata.pushbutton_dsm, 'UserData');
% veg=get(mainGUIdata.text_step1, 'UserData');

% veg=get(handles.uipanel_sdsm,'UserData');
% load('dsm.mat')
[filename, directory] = uiputfile('*.zip','Name of zip file containing buildings SVFs');
% filename='kr.zip';
% directory='M:\SOLWEIG\SOLWEIG_mfiles\SOLWIEG_GUI\SVFs\';
if filename==0
else
    svffiles{1}=[directory 'svf.asc'];
    svffiles{2}=[directory 'svfE.asc'];
    svffiles{3}=[directory 'svfS.asc'];
    svffiles{4}=[directory 'svfW.asc'];
    svffiles{5}=[directory 'svfN.asc'];
    [svfA,svfE,svfS,svfW,svfN]=Skyviewfactor4d(svffiles,dsm.a,dsm.scale,dsm.header,dsm.sizex,dsm.sizey);
    zip([directory filename],svffiles)
    delete(svffiles{1}, svffiles{2}, svffiles{3}, svffiles{4}, svffiles{5})
    svf.svf=svfA;
    svf.svfE=svfE;
    svf.svfS=svfS;
    svf.svfW=svfW;
    svf.svfN=svfN;
    %     set(mainGUIdata.pushbutton_svf,'UserData',svf);
    set(handles.pushbutton_loadsvf,'UserData',svf);
    %     set(handles.pushbutton_closesvf,'UserData',dsm);
    set(handles.text_svf,'String','Building SVF availabe','ForegroundColor',[0 0 1])
end

% save changed data back into main_gui
%this line updates the data of the Main Gui
% guidata(SOLWEIG, mainGUIdata);
guidata(hObject, handles);
% getframe(LoadCreateSVFs)

% --- Executes on button press in pushbutton_loadvegsvf.
function pushbutton_loadvegsvf_Callback(hObject, ~, handles)
[filename, directory] = uigetfile('*.zip','Name of zip file containing buildings and vegetation SVFs');
if filename==0
else
    unzip([directory,filename],directory)
    svffiles{1}=[directory 'svfveg.asc'];
    svffiles{2}=[directory 'svfEveg.asc'];
    svffiles{3}=[directory 'svfSveg.asc'];
    svffiles{4}=[directory 'svfWveg.asc'];
    svffiles{5}=[directory 'svfNveg.asc'];
    svffiles{6}=[directory 'svfaveg.asc'];
    svffiles{7}=[directory 'svfEaveg.asc'];
    svffiles{8}=[directory 'svfSaveg.asc'];
    svffiles{9}=[directory 'svfWaveg.asc'];
    svffiles{10}=[directory 'svfNaveg.asc'];
    [svfveg,svfEveg,svfSveg,svfWveg,svfNveg,svfaveg,svfEaveg,...
        svfSaveg,svfWaveg,svfNaveg]=Solweig_20_loadsvfveg(svffiles);
    delete(svffiles{1}, svffiles{2}, svffiles{3}, svffiles{4}, svffiles{5})
    delete(svffiles{6}, svffiles{7}, svffiles{8}, svffiles{9}, svffiles{10})
    svf.svfveg=svfveg;
    svf.svfEveg=svfEveg;
    svf.svfSveg=svfSveg;
    svf.svfWveg=svfWveg;
    svf.svfNveg=svfNveg;
    svf.svfaveg=svfaveg;
    svf.svfEaveg=svfEaveg;
    svf.svfSaveg=svfSaveg;
    svf.svfWaveg=svfWaveg;
    svf.svfNaveg=svfNaveg;
    set(handles.pushbutton_loadvegsvf,'UserData',svf);
    set(handles.text_svfveg,'String','Vegetation SVF availabe','ForegroundColor',[0 0 1])
end

guidata(hObject, handles);

% --- Executes on button press in pushbutton_createvegsvf.
function pushbutton_createvegsvf_Callback(hObject, ~, handles)
% % get the main_gui handle (access to the gui)
% mainGUIhandle = SOLWEIG;
% % get the data from the gui (all handles inside gui_main)
% mainGUIdata  = guidata(mainGUIhandle);
%
% dsm=get(mainGUIdata.pushbutton_dsm, 'UserData');
% veg=get(mainGUIdata.text_step1, 'UserData');

dsm=get(handles.uipanel_dsm,'UserData');
veg=get(handles.uipanel_cdsm,'UserData');

[filename, directory] = uiputfile('*.zip','Name of zip file containing building and vegetation SVFs');
if filename==0
else
    svffiles{1}=[directory 'svfveg.asc'];
    svffiles{2}=[directory 'svfEveg.asc'];
    svffiles{3}=[directory 'svfSveg.asc'];
    svffiles{4}=[directory 'svfWveg.asc'];
    svffiles{5}=[directory 'svfNveg.asc'];
    svffiles{6}=[directory 'svfaveg.asc'];
    svffiles{7}=[directory 'svfEaveg.asc'];
    svffiles{8}=[directory 'svfSaveg.asc'];
    svffiles{9}=[directory 'svfWaveg.asc'];
    svffiles{10}=[directory 'svfNaveg.asc'];
    
    [svfveg,svfEveg,svfSveg,svfWveg,svfNveg,svfaveg,svfEaveg,...
        svfSaveg,svfWaveg,svfNaveg]=Skyviewfactor4d_veg(svffiles...
        ,dsm.a,dsm.scale,veg.vegdem,veg.vegdem2,dsm.header,dsm.sizex,dsm.sizey);
    
    zip([directory filename],svffiles)
    delete(svffiles{1}, svffiles{2}, svffiles{3}, svffiles{4}, svffiles{5})
    delete(svffiles{6}, svffiles{7}, svffiles{8}, svffiles{9}, svffiles{10})
    svf.svfveg=svfveg;
    svf.svfEveg=svfEveg;
    svf.svfSveg=svfSveg;
    svf.svfWveg=svfWveg;
    svf.svfNveg=svfNveg;
    svf.svfaveg=svfaveg;
    svf.svfEaveg=svfEaveg;
    svf.svfSaveg=svfSaveg;
    svf.svfWaveg=svfWaveg;
    svf.svfNaveg=svfNaveg;
    set(handles.pushbutton_loadvegsvf,'UserData',svf);
    set(handles.text_svfveg,'String','Vegetation SVF availabe','ForegroundColor',[0 0 1])
end
% set(mainGUIdata.pushbutton_dsm, 'UserData',dsm);
% set(mainGUIdata.text_step1, 'UserData',veg);
% % save changed data back into main_gui
% %this line updates the data of the Main Gui
% guidata(SOLWEIG, mainGUIdata);
guidata(hObject, handles);
% getframe(LoadCreateSVFs)

% --- Executes on button press in pushbutton_closesvf.
function pushbutton_closesvf_Callback(hObject, ~, handles)
% get the main_gui handle (access to the gui)
mainGUIhandle = SOLWEIG;
% get the data from the gui (all handles inside gui_main)
mainGUIdata  = guidata(mainGUIhandle);

dsm=get(mainGUIdata.pushbutton_dsm, 'UserData');
svf=get(handles.pushbutton_loadsvf, 'UserData');
svfveg=get(handles.pushbutton_loadvegsvf, 'UserData');

if dsm.sizex==size(svf.svf,2) && dsm.sizey==size(svf.svf,1)
    if isempty(svfveg)==1
        imagesc(svf.svf,'parent',mainGUIdata.axes2);
    else
        imagesc(svf.svf+svfveg.svfveg,'parent',mainGUIdata.axes2);
    end
    colormap(mainGUIdata.axes2,copper)
    set(mainGUIdata.axes2,'Visible','off')
    set(mainGUIdata.text2,'Visible','off')
    axis(mainGUIdata.axes2,'image')
    % set(mainGUIdata.axes2,'PlotBoxAspectRatio',[dsm.sizex dsm.sizey 1])
    set(mainGUIdata.pushbutton_metdata, 'Enable','on');
    set(mainGUIdata.pushbutton_svf, 'UserData',svf);
    set(mainGUIdata.text_step2, 'UserData',svfveg);
    % save changed data back into main_gui
    %this line updates the data of the Main Gui
    guidata(SOLWEIG, mainGUIdata);
    
    % close this gui
    close(LoadCreateSVFs)
else
    msgboxText{1} =  'Load another svf file or create new';
    h=msgbox(msgboxText,'Svf grids do not match dsm', 'error');
    set(handles.text_svf,'String','Building SVF not availabe','ForegroundColor',[1 0 0])
    set(handles.text_svfveg,'String','Vegetation SVF not availabe','ForegroundColor',[1 0 0])
    getframe(h)
end
