function varargout = LoadMeteorology(varargin)
% LOADMETEOROLOGY MATLAB code for LoadMeteorology.fig

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @LoadMeteorology_OpeningFcn, ...
                   'gui_OutputFcn',  @LoadMeteorology_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before LoadMeteorology is made visible.
function LoadMeteorology_OpeningFcn(hObject, ~, handles, varargin)

% Choose default command line output for LoadMeteorology
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes LoadMeteorology wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = LoadMeteorology_OutputFcn(hObject, eventdata, handles) 
% Get default command line output from handles structure
varargout{1} = handles.output;

% --- Executes on button press in checkbox_reindl.
function checkbox_reindl_Callback(hObject, ~, handles)
get(hObject,'Value');
guidata(hObject, handles);

% --- Executes on button press in pushbutton_loadmetdata.
function pushbutton_loadmetdata_Callback(hObject, ~, handles)
[inputfile,directory]=uigetfile({'*.txt'},'Select text file');
% get the main_gui handle (access to the gui)
mainGUIhandle = SOLWEIG;       
% get the data from the gui (all handles inside gui_main)
mainGUIdata  = guidata(mainGUIhandle);
location.longitude=str2num(get(mainGUIdata.text_lon,'String'));
location.latitude=str2num(get(mainGUIdata.text_lat,'String'));
location.altitude=str2num(get(mainGUIdata.text_alt,'String'));
UTC=str2num(get(mainGUIdata.text_utc,'String'));
% [met1,met_header,time,altitude,azimuth,zen,jday,XM,XD]=Solweig_10_metdata([directory inputfile],location,UTC);
[met1,met_header,YYYY,altitude,azimuth,zen,jday,leafon]=Solweig_2014a_metdata(directory,inputfile,location,UTC);
met.met=met1;
met.met_header=met_header;
% met.time=time;
met.altitude=altitude;
met.azimuth=azimuth;
met.zen=zen;
met.jday=jday;
met.YYYY=YYYY;
met.leafon=leafon;
% met.UTC=UTC;
set(handles.pushbutton_loadmetdata,'UserData',met)
set(handles.text1,'String','Meteorological data loaded','ForegroundColor',[0 0 1])
getframe(LoadMeteorology);
guidata(hObject, handles);

% --- Executes on button press in pushbutton_close.
function pushbutton_close_Callback(hObject, ~, handles)
% get the main_gui handle (access to the gui)
mainGUIhandle = SOLWEIG;       
% get the data from the gui (all handles inside gui_main)
mainGUIdata  = guidata(mainGUIhandle);

met=get(handles.pushbutton_loadmetdata,'UserData');
reindl=get(handles.checkbox_reindl,'Value');
set(mainGUIdata.pushbutton_metdata,'UserData',met)
set(mainGUIdata.text_step3,'UserData',reindl)
set(mainGUIdata.pushbutton_execute,'Enable','on');
% save changed data back into main_gui
%this line updates the data of the Main Gui
guidata(SOLWEIG, mainGUIdata);
 
% close this gui
close(LoadMeteorology)
