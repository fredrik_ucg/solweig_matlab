name_country	name_city	longitude	latitude	altitude	utc		
Sweden	G�teborg	11.97	57.7	3	1
Egypt	Cairo	31.25	30.05	95	2
Andorra	Andorra	1.5	42.5	0	1
Argentina	Buenos Aires	-58.5	-34.67	25	-3
Australia	Darwin	130.9	-12.38	0	9.5
Australia	Sydney	151.17	-33.54	42	10
Belgium	Brussels	4.35	50.83	398	1
Bolivia	La Paz	-68.17	-16.5	3632	-4
Brazil	Brasilia	-47.97	-15.77	0	-3
Brazil	Rio de Janeiro	-43.27	-22.9	31	-3
Chile	Santiago	-70.67	-33.5	520	-4
China	Nanking	118.8	32.08	0	8
China	Peking	116.32	39.95	52	8
Denmark	Copenhagen	12.57	55.72	9	1
Germany	Berlin	13.43	52.5	35	1
Germany	Bochum	7.21	51.49	0	1
Germany	Bremen	8.8	53.08	4	1
Germany	Dresden	13.68	51.02	246	1
Germany	Frankfurt am Main	8.68	50.1	103	1
Germany	Freiburg	7.85	48	278	1
Germany	Hamburg	10	53.55	14	1
Germany	Hannover	9.73	52.38	53	1
Germany	Heidelberg	8.7	49.42	112	1
Germany	Leipzig	12.33	51.33	0	1
Germany	Munich	11.58	48.13	530	1
Germany	Stuttgart	9.2	48.78	260	1
Ecuador	Guayaquil	-79.88	-2.26	0	-5
Ecuador	Quito	-78.5	-0.22	2818	-5
El Salvador	San Salvador	-89.17	13.67	700	-6
Finland	Helsinki	25	60.13	45	2
France	Bordeaux	-0.57	44.83	47	1
France	Lyon	4.83	45.77	200	1
France	Montpellier	4	43.6	0	1
France	Nice	7.2	43.6	5	1
France	Paris	2.33	48.87	52	1
Greece	Athens	23.73	38	107	2
Russia	Moscow	37.58	55.75	156	3
Russia	St. Petersburg	30.42	59.92	4	3
Hong Kong	Hong Kong	114.18	22.25	33	8
India	Calcutta	88.35	22.58	6	5.5
India	New Delhi	77.22	28.37	218	5.5
Ireland	Cork	-8.47	51.9	0	0
Ireland	Dublin	-6.25	53.33	68	0
Iceland	Reykjavik	-21.97	61.15	18	-1
Israel	Tel Aviv	34.77	32.08	0	2
Italy	Florins	11.25	43.78	76	1
Italy	Genoa	8.93	44.4	54	1
Italy	Nepal	14.25	40.83	25	1
Italy	Rom	12.5	41.88	46	1
Italy	Venice	12.33	45.43	1	1
Japan	Tokyo	139.77	35.7	0	9
Serbia	Belgrade	20.47	44.8	131	1
Canada	Inuvik	-133.5	68.3	0	-8
Canada	Quebec	-71.25	47.83	23	-5
Canada	Saint John	-66	45.3	0	-4
Canada	Vancouver	-123.07	49.15	2	-8
Kenya	Mombasa	39.67	-4.07	55	3
Colombia	Bogota	-74.08	4.63	2556	-5
Cuba	Havana	-82.42	23.12	24	-5
Luxemburg	Luxemburg	6.13	49.62	343	1
Malaysia	Kuala Lumpur	101.7	3.13	0	7.5
Morocco	Marrakech	-8	31.82	460	0
Mexico	Mexico City	-99.17	19.42	2485	-6
Monaco	Monte Carlo	7.42	43.73	0	1
Nepal	Kathmandu	85.32	27.7	1337	5.5
New Zeeland	Auckland	174.75	-36.92	49	12
New Zeeland	Dunedin	170.5	-45.87	0	12
Holland	Amsterdam	4.9	52.35	0	1
Holland	Maastricht	5.7	50.85	0	1
Holland	Rotterdam	4.48	51.92	0	1
Norway	Bergen	5.33	60.38	45	1
Norway	Oslo	10.75	59.93	96	1
Norway	Trondheim	10.38	63.6	0	1
Austria	Salzburg	13.05	47.9	435	1
Austria	Wien	16.37	48.22	203	1
Peru	Lima	-77.05	-12.1	11	-5
Philippines	Manila	120.98	14.6	16	8
Poland	Breslau	16.98	51.13	119	1
Poland	Krakow	20.02	50.08	213	1
Poland	Warsaw	21	52.25	107	1
Portugal	Lisbon	-9.13	38.73	77	0
Portugal	Porto	-8.62	41.15	95	0
Rumania	Bucharest	44.42	26.1	82	2
Sweden	Malm�	13	55.58	8	1
Switzerland	Basel	7.6	47.55	0	1
Switzerland	Gent	6.15	46.2	405	1
Switzerland	Z�rich	8.55	47.38	569	1
Slovakia	Bratislava	17.2	48.2	133	1
Spain	Barcelona	2.17	41.42	95	1
Spain	Cadiz	-6.3	36.4	0	1
Spain	Madrid	-3.72	40.42	667	1
Spain	Malaga	-4.42	36.72	34	1
South Africa	Johannesburg	28	-26.25	0	2
Tibet	Lhasa	91.17	29.68	0	6
Tunisia	Tunis	10.22	36.83	3	1
Turkey	Istanbul	28.98	41.03	0	2
UK	Belfast	-5.92	54.58	67	0
UK	Birmingham	1.93	52.48	136	0
UK	Cambridge	0.13	52.2	12	0
UK	Glasgow	4.4	55.87	6	0
UK	London	-0.17	51.5	0	0
Hungary	Budapest	19	47.5	0	1
USA	El Dorado	-92.67	33.21	0	-5
USA	Eureka	-124.17	40.82	0	-8
USA	Los Angeles	-118.23	34.05	103	-8
USA	San Francisco	-122.43	37.77	16	-8
USA	Denver	-104.99	39.75	0	-7
USA	Washington	-77.02	38.9	4	-5
USA	Miami	-80.22	25.78	2	-5
USA	Honolulu	-157.86	21.31	10	-10
USA	Clinton	-90.19	41.84	0	-6
USA	Moscow	-117	46.73	0	-7
USA	Detroit	-83.05	42.33	189	-5
USA	Minneapolis	-93.26	44.98	254	-6
USA	Ocean Grove	-74	40.17	0	-5
USA	New York City	-74	40.75	96	-5
USA	Valhalla	-73.77	41.81	0	-5
USA	Seattle	-122.33	47.6	4	-8
USA	Milwaukee	-87.9	43.04	0	-6
USA	Sacramento	-121.5	38.65	8	-8
Venezuela	Caracas	-66.93	10.58	1035	-4.5
Sweden	Lule�	22.15	65.58	2	1
Sweden	Stockholm	18	59	3	1
Antartica	SouthPole	0	-90	2000	0	