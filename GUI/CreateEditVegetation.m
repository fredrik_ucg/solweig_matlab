function varargout = CreateEditVegetation(varargin)
% CREATEEDITVEGETATION MATLAB code for CreateEditVegetation.fig

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @CreateEditVegetation_OpeningFcn, ...
                   'gui_OutputFcn',  @CreateEditVegetation_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before CreateEditVegetation is made visible.
function CreateEditVegetation_OpeningFcn(hObject, ~, handles, varargin)

% get the main_gui handle (access to the gui)
mainGUIhandle = DigitalSurfaceModels;       
% get the data from the gui (all handles inside gui_main)
mainGUIdata  = guidata(mainGUIhandle);

dsm=get(mainGUIdata.pushbutton_loaddsm, 'UserData');
removeopt=get(mainGUIdata.uipanel_vegtext,'UserData');
oldvegdem=get(mainGUIdata.text_vegdsm,'Value');

if oldvegdem==0
    if sum(removeopt(:,1))>0
        set(handles.pushbutton_remove,'Enable','on')
    else
        veg=[];
        set(handles.pushbutton_remove,'Enable','off')
    end
else
    veg=get(mainGUIdata.pushbutton_continue, 'UserData');
    set(handles.pushbutton_save,'UserData',veg);
    set(handles.popupmenu_id,'Userdata',veg.id)
    textdata2=veg.vegcoord_out(:,1);
    set(handles.popupmenu_id,'String',textdata2)
end
if isempty(veg)==1
    veg.vegdem=0;
end
getframe(handles.axes2);
imagesc(dsm.a+veg.vegdem);%,'parent',handles.axes2);
colormap(handles.axes2,copper);
if oldvegdem==1
    for i=1:length(veg.vegcoord_out(:,7))
        if veg.vegcoord_out(i,1)>0
            text(veg.vegcoord_out(i,7),veg.vegcoord_out(i,6),int2str(veg.vegcoord_out(i,1)),'Color',[1 1 1],'FontSize',10);
        end
    end
end
set(handles.axes2,'Visible','off')
set(handles.axes2,'PlotBoxAspectRatio',[dsm.sizex dsm.sizey 1])
set(handles.uipanel2,'UserData',dsm)
% Choose default command line output for CreateEditVegetation
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes CreateEditVegetation wait for user response (see UIRESUME)
% uiwait(handles.figure1);

% --- Outputs from this function are returned to the command line.
function varargout = CreateEditVegetation_OutputFcn(hObject, eventdata, handles) 

% Get default command line output from handles structure
varargout{1} = handles.output;

% --- Executes on selection change in popupmenu_vegtype.
function popupmenu_vegtype_Callback(hObject, ~, handles)
ttype=get(hObject,'Value');
if ttype==3
    set(handles.edit_trunkheight,'Enable','off')
    set(handles.edit_trunkheight,'String','0')
else
    set(handles.edit_trunkheight,'Enable','on')
    set(handles.edit_trunkheight,'String','')    
end

guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function popupmenu_vegtype_CreateFcn(hObject, ~, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit_canopyheight_Callback(hObject, ~, handles)
input = str2double(get(hObject,'String'));
if input>150 || input<0
    msgboxText{1} =  'Choose a decimal number between 0 and 150';
    msgbox(msgboxText,'Number has to be between 0 and 150', 'error');
end
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function edit_canopyheight_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit_dia_Callback(hObject, ~, handles)
input = str2double(get(hObject,'String'));
if input>100 || input<0
    msgboxText{1} =  'Choose a decimal number between 0 and 100';
    msgbox(msgboxText,'Number has to be between 0 and 100', 'error');
end
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function edit_dia_CreateFcn(hObject, ~, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit_trunkheight_Callback(hObject, ~, handles)
input = str2double(get(hObject,'String'));
if input>100 || input<0
    msgboxText{1} =  'Choose a decimal number between 0 and 100';
    msgbox(msgboxText,'Number has to be between 0 and 100', 'error');
end
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function edit_trunkheight_CreateFcn(hObject, ~, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on button press in pushbutton_locate.
function pushbutton_locate_Callback(hObject, eventdata, handles)

% get the main_gui handle (access to the gui)
mainGUIhandle = DigitalSurfaceModels;       
% get the data from the gui (all handles inside gui_main)
mainGUIdata  = guidata(mainGUIhandle);
oldvegdem=get(mainGUIdata.text_vegdsm,'Value');
if oldvegdem==0
    veg=get(mainGUIdata.pushbutton_continue,'UserData');
else
    veg=get(handles.pushbutton_save,'UserData');
end
dsm=get(handles.uipanel2,'UserData');
a=dsm.a;
sizex=dsm.sizex;
sizey=dsm.sizey;
scale=dsm.scale;
auto=0;
if isempty(veg)
    vegdem=zeros(size(dsm.a,1),size(dsm.a,2));
    vegdem2=zeros(size(dsm.a,1),size(dsm.a,2));
    id=1;
else
    vegdem=veg.vegdem;
    vegdem2=veg.vegdem2;
    id=get(handles.popupmenu_id,'Userdata');
end

% veg=get(mainGUIdata.pushbutton_continue,'Userdata');
if oldvegdem==0
    vegcoord_out=get(mainGUIdata.uipanel_vegtext,'UserData');
else
    vegco=get(handles.pushbutton_save,'UserData');
    vegcoord_out=vegco.vegcoord_out;
end
buildings=get(mainGUIdata.pushbutton_createveg,'UserData');
ttype=get(handles.popupmenu_vegtype,'Value');
height=str2num(get(handles.edit_canopyheight,'String'));
trunk=str2num(get(handles.edit_trunkheight,'String'));
dia=str2num(get(handles.edit_dia,'String'));

rowa=[];cola=[];

% This function creates the shape of each vegetation unit and locates it a
% grid. It also generates each trunk zone underneath each unit.
clim=[min(min(a+vegdem)) max(max(a+vegdem))];
if range(clim)==0, clim(2)=1; end
vegdemtemp=zeros(size(a,1),size(a,2));
vegdem2temp=vegdemtemp;
dia=dia*scale;
if ttype == 1 % conifer tree
    trees=conifertree(dia);
    trees=trees*(height-trunk);
    circle=imcircle(dia);
    trees=circle.*(trees+trunk);
    treetrunkunder=trunk*circle;
    
elseif ttype == 2 % desiduous tree
    trees=conifertree(dia);
    canopy=1-((1-trees).^2);
    trees=canopy*(height-trunk);
    circle=imcircle(dia);
    trees=circle.*(trees+trunk);
    treetrunkunder=circle*trunk;
    
elseif ttype == 3 % bush
    trees=conifertree(dia);
    canopy=1-((1-trees).^2);
    trees=canopy*(height-trunk);
    circle=imcircle(dia);
    trees=circle.*trees;
    treetrunkunder=circle*0.1;
end

if auto == 0
    b=a;
    getframe(handles.axes2);
    imagesc(a+vegdem), axis image, hold on
    set(handles.axes2,'Visible','off')
    for i=1:length(vegcoord_out(:,7))
        if vegcoord_out(i,1)>0
            text(vegcoord_out(i,7),vegcoord_out(i,6),int2str(vegcoord_out(i,1)),'Color',[1 1 1],'FontSize',10);
        end
    end
    
    A=[-1 -1];
    while (A(1,1)<0) || (A(1,2)<0) || (A(1,1) > sizex) || (A(1,2) > sizey)
        A=ginput(1);
    end
    
    col=floor(A(1,1));
    row=floor(A(1,2));
    col1=col-(floor(dia/2));
    row1=row-(floor(dia/2));
else
    col1=cola-(floor(dia/2));
    row1=rowa-(floor(dia/2));
end

rowmin=abs(row1);
colmin=abs(col1);
rowmax=length(trees);
colmax=length(trees);

% cutting trees at dem egde H�R�RJAG
rowcutmax=length(trees);colcutmax=rowcutmax;rowcutmin=1;colcutmin=1;
if row1+length(trees)-1>size(vegdem,1)
    rowcutmax=length(trees)-abs(size(vegdem,1)-(row1+length(trees)-1));
    rowmax=size(vegdem,1);
end
if col1+length(trees)-1>size(vegdem,2)
    colcutmax=length(trees)-abs(size(vegdem,2)-(col1+length(trees)-1));
    colmax=size(vegdem,2);
end
if row1<1
    rowcutmin=abs(row1);
    rowmin=1;
    if rowcutmin==0,rowcutmin=1;end
end
if col1<1
    colcutmin=abs(col1);
    colmin=1;
    if colcutmin==0,colcutmin=1;end
end
if row1<1 || col1<1 || row1+length(trees)-1>size(vegdem,1) ||col1+length(trees)-1>size(vegdem,2)
    % cutting tree at dem edge
    trees=trees(rowcutmin:rowcutmax,colcutmin:colcutmax);
    treetrunkunder=treetrunkunder(rowcutmin:rowcutmax,colcutmin:colcutmax);
    vegdemtemp(rowmin:rowmin+size(trees,1)-1,colmin:colmin+size(trees,2)-1)=trees;
    vegdem2temp(rowmin:rowmin+size(trees,1)-1,colmin:colmin+size(trees,2)-1)=treetrunkunder;
else
    % no cutting of tree at dem edge
    vegdemtemp(rowmin:rowmin+rowmax-1,colmin:colmin+colmax-1)=trees;
    vegdem2temp(rowmin:rowmin+rowmax-1,colmin:colmin+colmax-1)=treetrunkunder;
end

vegdem=max(vegdem,vegdemtemp);
vegdem=vegdem(1:size(buildings,1),1:size(buildings,2)).*buildings; %remove vegetation from building pixels
vegdem2temp(vegdemtemp==0)=-1000;
vegdem2(vegdem2==0)=-1000;
vegdem2=max(vegdem2,vegdem2temp);
vegdem2=vegdem2(1:size(buildings,1),1:size(buildings,2)).*buildings; %remove vegetation from building pixels
vegdem2(vegdem2==-1000)=0;

% if auto == 0
    e=b+vegdem;
    imagesc(e(1:sizey,1:sizex),clim), axis image, hold on
    text(col1+(floor(dia/2)),row1+(floor(dia/2)),int2str(id),'Color',[1 1 1],'FontSize',10);
    for i=1:length(vegcoord_out(:,7))
        if vegcoord_out(i,1)>0
            text(vegcoord_out(i,7),vegcoord_out(i,6),int2str(vegcoord_out(i,1)),'Color',[1 1 1],'FontSize',10);
        end
    end
    pause(0.1)
% end

vegunit=[id,ttype,dia/scale,height,trunk,row1+(floor(dia/2)),col1+(floor(dia/2))];
vegcoord_out(id,1:7)=vegunit;
id=id+1;
veg.vegcoord_out=vegcoord_out;
% veg=get(mainGUIdata.pushbutton_continue,'Userdata',veg);
% set(mainGUIdata.uipanel_vegtext,'Userdata',vegcoord_out);
veg.vegdem=vegdem;
veg.vegdem2=vegdem2;
veg.id=id;
set(handles.pushbutton_save,'UserData',veg);
set(handles.popupmenu_id,'Userdata',id)
set(mainGUIdata.text_vegdsm,'Value',1);
set(mainGUIdata.pushbutton_continue,'UserData',veg)
guidata(DigitalSurfaceModels, mainGUIdata);
guidata(hObject, handles);
getframe(CreateEditVegetation);
% save changed data back into main_gui
%this line updates the data of the Main Gui
% guidata(DigitalSurfaceModels, mainGUIdata);
% getframe(CreateEditVegetation)

% --- Executes on selection change in popupmenu_id.
function popupmenu_id_Callback(hObject, ~, handles)
get(hObject,'Value');
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function popupmenu_id_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on button press in pushbutton_remove.
function pushbutton_remove_Callback(hObject, eventdata, handles)
% get the main_gui handle (access to the gui)
mainGUIhandle = DigitalSurfaceModels;       
% get the data from the gui (all handles inside gui_main)
mainGUIdata  = guidata(mainGUIhandle);
idline=get(handles.popupmenu_id,'Value');
veg=get(handles.pushbutton_save,'UserData');
vegcoord_out=veg.vegcoord_out;
idremove=vegcoord_out(idline);
dsm=get(handles.uipanel2,'UserData');
a=dsm.a;
sizex=dsm.sizex;
sizey=dsm.sizey;
scale=dsm.scale;
buildings=get(mainGUIdata.pushbutton_createveg,'UserData');
[vegcoord_out,vegdem,vegdem2]=Removetree_GUI(a,sizex,sizey,buildings,veg.vegdem,veg.vegdem2,idremove,vegcoord_out,scale);
clim=[min(a(:)) max(max(a+vegdem))];
getframe(handles.axes2);
imagesc(vegdem(1:sizey,1:sizex)+a(1:sizey,1:sizex),clim), axis image, hold on
for i=1:length(vegcoord_out(:,7))
    if vegcoord_out(i,1)>0
        text(vegcoord_out(i,7),vegcoord_out(i,6),int2str(vegcoord_out(i,1)),'Color',[1 1 1],'FontSize',10);
    end
end
veg.vegcoord_out=vegcoord_out;
veg.vegdem=vegdem;
veg.vegdem2=vegdem2;
set(handles.pushbutton_save,'UserData',veg);
guidata(hObject, handles);

% --- Executes on button press in pushbutton_cancel.
function pushbutton_cancel_Callback(~, ~, handles)
% get the main_gui handle (access to the gui)
mainGUIhandle = DigitalSurfaceModels;       
% get the data from the gui (all handles inside gui_main)
mainGUIdata  = guidata(mainGUIhandle);
veg=get(handles.pushbutton_save, 'UserData');
set(mainGUIdata.pushbutton_edit,'Enable','off')
set(mainGUIdata.pushbutton_continue,'UserData',[]);
% set(handles.popupmenu_id,'Userdata',veg.id)
set(mainGUIdata.text_vegdsm,'String','Vegetation DSMs not loaded','ForegroundColor',[1 0 0])
guidata(DigitalSurfaceModels, mainGUIdata);
close(CreateEditVegetation)

% --- Executes on button press in pushbutton_save.
function pushbutton_save_Callback(hObject, ~, handles)
% get the main_gui handle (access to the gui)
mainGUIhandle = DigitalSurfaceModels;       
% get the data from the gui (all handles inside gui_main)
mainGUIdata  = guidata(mainGUIhandle);

veg=get(handles.pushbutton_save,'UserData');
if isempty(veg)
    veg.vegcoord_out=get(mainGUIdata.uipanel_vegtext,'UserData');
end
set(mainGUIdata.pushbutton_continue,'UserData',veg);
[inputfile directory]=uiputfile({'*.txt'},'Create text file');
save_vegcoord(veg.vegcoord_out,[directory inputfile]);
% save([directory inputfile],veg.vegcoord_out,'-ASCII','-tabs')
set(mainGUIdata.text_vegdsm,'String','Vegetation DSMs loaded','ForegroundColor',[0 0 1]);
guidata(DigitalSurfaceModels, mainGUIdata);
guidata(hObject, handles);
close(CreateEditVegetation)
