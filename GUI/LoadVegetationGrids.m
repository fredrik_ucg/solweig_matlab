function varargout = LoadVegetationGrids(varargin)
% LOADVEGETATIONGRIDS MATLAB code for LoadVegetationGrids.fig

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @LoadVegetationGrids_OpeningFcn, ...
                   'gui_OutputFcn',  @LoadVegetationGrids_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before LoadVegetationGrids is made visible.
function LoadVegetationGrids_OpeningFcn(hObject, eventdata, handles, varargin)

% % get the main_gui handle (access to the gui)
% mainGUIhandle2 = SOLWEIG;       
% % get the data from the gui (all handles inside gui_main)
% mainGUIdata2  = guidata(mainGUIhandle2);
% 
% dsm=get(mainGUIdata2.pushbutton_dsm, 'UserData');

% Choose default command line output for LoadVegetationGrids
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes LoadVegetationGrids wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = LoadVegetationGrids_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in pushbutton_loadcanopy.
function pushbutton_loadcanopy_Callback(hObject, eventdata, handles)
% get the main_gui handle (access to the gui)
mainGUIhandle = DigitalSurfaceModels;       
% get the data from the gui (all handles inside gui_main)
mainGUIdata  = guidata(mainGUIhandle);

dsm=get(mainGUIdata.pushbutton_loaddsm, 'UserData');

[inputfile, directory]=uigetfile({'*.asc;*.txt'},'Select ESRI ASCIIgrid file');

if inputfile==0
else
    [a,scale,header,headernum,headername,sizey,sizex]=Solweig_10_loaddem([directory inputfile]);
    cdsm.vegdem=a;
    cdsm.scale=scale;
    cdsm.header=header;
    cdsm.headernum=headernum;
    cdsm.headername=headername;
    cdsm.sizey=sizey;
    cdsm.sizex=sizex;
    if dsm.sizex==cdsm.sizex && dsm.sizey==cdsm.sizey && dsm.scale==cdsm.scale
        set(handles.pushbutton_loadcanopy,'UserData',cdsm);
        set(handles.text_dsm,'String','Canopy DSM loaded','ForegroundColor',[0 0 1])
        set(handles.pushbutton_loadtrunk,'Enable','on')
        set(handles.pushbutton_fraction,'Enable','on')
        set(handles.pushbutton_constant,'Enable','on')
        set(handles.pushbutton_save,'Enable','off')
    else
        msgboxText{1} =  'Choose another grid or method';
        msgbox(msgboxText,'The size of two grids is not the same', 'error');
    end
    
    getframe(LoadVegetationGrids);
    guidata(hObject, handles);
    
end

% --- Executes on button press in pushbutton_loadtrunk.
function pushbutton_loadtrunk_Callback(hObject, eventdata, handles)
[inputfile directory]=uigetfile({'*.asc;*.txt'},'Select ESRI ASCIIgrid file');
cdsm=get(handles.pushbutton_loadcanopy,'UserData');

if inputfile==0
else
    [a,scale,header,headernum,headername,sizey,sizex]=Solweig_10_loaddem([directory inputfile]);
    tdsm.vegdem2=a;
    tdsm.scale=scale;
    tdsm.header=header;
    tdsm.headernum=headernum;
    tdsm.headername=headername;
    tdsm.sizey=sizey;
    tdsm.sizex=sizex;
    if cdsm.sizex==tdsm.sizex && cdsm.sizey==tdsm.sizey && cdsm.scale==tdsm.scale
        set(handles.pushbutton_loadtrunk,'UserData',tdsm);
        set(handles.text_trunk,'String','Trunkzone DSM loaded/generated','ForegroundColor',[0 0 1])
        set(handles.pushbutton_save,'Enable','on')
    else
        msgboxText{1} =  'Choose another grid or method';
        msgbox(msgboxText,'The size of the grids is not the same', 'error');
    end
    
    guidata(hObject, handles);
 
end

% --- Executes during object creation, after setting all properties.
function edit_fraction_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit_fraction_Callback(hObject, eventdata, handles)
input = str2double(get(hObject,'String'));
if input>1 || input<0
    msgboxText{1} =  'Choose a decimal number between 0 and 1';
    msgbox(msgboxText,'Number has to be between 0 and 1', 'error');
end
guidata(hObject, handles);

% --- Executes on button press in pushbutton_fraction.
function pushbutton_fraction_Callback(hObject, eventdata, handles)
frac=str2double(get(handles.edit_fraction,'String'));
cdsm=get(handles.pushbutton_loadcanopy,'UserData');
tdsm.vegdem2=cdsm.vegdem*frac;
set(handles.pushbutton_loadtrunk,'UserData',tdsm);
set(handles.text_trunk,'String','Trunkzone DSM loaded/generated','ForegroundColor',[0 0 1])
set(handles.pushbutton_save,'Enable','on')
guidata(hObject, handles);

function edit_height_Callback(hObject, eventdata, handles)
input = str2double(get(hObject,'String'));
if input>100 || input<0
    msgboxText{1} =  'Choose a decimal number between 0 and 100';
    msgbox(msgboxText,'Number has to be between 0 and 100', 'error');
end
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function edit_height_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on button press in pushbutton_constant.
function pushbutton_constant_Callback(hObject, eventdata, handles)
h=str2num(get(handles.edit_height,'String'));
cdsm=get(handles.pushbutton_loadcanopy,'UserData');
tdsm.vegdem2=cdsm.vegdem;
tdsm.vegdem2(tdsm.vegdem2>0)=h;
tdsm.vegdem2(tdsm.vegdem2>cdsm.vegdem)=0;
set(handles.pushbutton_loadtrunk,'UserData',tdsm);
set(handles.text_trunk,'String','Trunkzone DSM loaded/generated','ForegroundColor',[0 0 1])
set(handles.pushbutton_save,'Enable','on')
guidata(hObject, handles);

% --- Executes on button press in pushbutton_save.
function pushbutton_save_Callback(hObject, ~, handles)
% get the main_gui handle (access to the gui)
mainGUIhandle = DigitalSurfaceModels;       
% get the data from the gui (all handles inside gui_main)
mainGUIdata  = guidata(mainGUIhandle);

tdsm=get(handles.pushbutton_loadtrunk,'UserData');
cdsm=get(handles.pushbutton_loadcanopy,'UserData');

veg.vegdem=cdsm.vegdem;
veg.vegdem2=tdsm.vegdem2;
set(mainGUIdata.pushbutton_continue,'UserData',veg);
set(mainGUIdata.text_vegdsm,'String','Vegetation DSMs loaded','ForegroundColor',[0 0 1]);
guidata(DigitalSurfaceModels, mainGUIdata);
close(LoadVegetationGrids)

% --- Executes on button press in pushbutton_cancel.
function pushbutton_cancel_Callback(hObject, ~, handles)
close(LoadVegetationGrids)
