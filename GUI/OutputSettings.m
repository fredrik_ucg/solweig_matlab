function varargout = OutputSettings(varargin)
% OUTPUTSETTINGS MATLAB code for OutputSettings.fig
%      OUTPUTSETTINGS, by itself, creates a new OUTPUTSETTINGS or raises the existing
%      singleton*.
%
%      H = OUTPUTSETTINGS returns the handle to a new OUTPUTSETTINGS or the handle to
%      the existing singleton*.
%
%      OUTPUTSETTINGS('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in OUTPUTSETTINGS.M with the given input arguments.
%
%      OUTPUTSETTINGS('Property','Value',...) creates a new OUTPUTSETTINGS or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before OutputSettings_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to OutputSettings_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help OutputSettings

% Last Modified by GUIDE v2.5 14-Feb-2013 16:09:51

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @OutputSettings_OpeningFcn, ...
                   'gui_OutputFcn',  @OutputSettings_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before OutputSettings is made visible.
function OutputSettings_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to OutputSettings (see VARARGIN)

% Choose default command line output for OutputSettings
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes OutputSettings wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = OutputSettings_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in checkbox_tmrt_h.
function checkbox_tmrt_h_Callback(hObject, eventdata, handles)
get(hObject,'Value');
guidata(hObject, handles);

% --- Executes on button press in checkbox_tmrt_day.
function checkbox_tmrt_day_Callback(hObject, eventdata, handles)
get(hObject,'Value');
guidata(hObject, handles);

% --- Executes on button press in checkbox_tmrt_diurn.
function checkbox_tmrt_diurn_Callback(hObject, eventdata, handles)
get(hObject,'Value');
guidata(hObject, handles);

% --- Executes on button press in checkbox_lup_h.
function checkbox_lup_h_Callback(hObject, eventdata, handles)
get(hObject,'Value');
guidata(hObject, handles);

% --- Executes on button press in checkbox_lup_day.
function checkbox_lup_day_Callback(hObject, eventdata, handles)
get(hObject,'Value');
guidata(hObject, handles);

% --- Executes on button press in checkbox_lup_diurn.
function checkbox_lup_diurn_Callback(hObject, eventdata, handles)
get(hObject,'Value');
guidata(hObject, handles);

% --- Executes on button press in checkbox_ldown_h.
function checkbox_ldown_h_Callback(hObject, eventdata, handles)
get(hObject,'Value');
guidata(hObject, handles);

% --- Executes on button press in checkbox_ldown_day.
function checkbox_ldown_day_Callback(hObject, eventdata, handles)
get(hObject,'Value');
guidata(hObject, handles);

% --- Executes on button press in checkbox_ldown_diurn.
function checkbox_ldown_diurn_Callback(hObject, eventdata, handles)
get(hObject,'Value');
guidata(hObject, handles);

% --- Executes on button press in checkbox_kup_h.
function checkbox_kup_h_Callback(hObject, eventdata, handles)
get(hObject,'Value');
guidata(hObject, handles);

% --- Executes on button press in checkbox_kup_day.
function checkbox_kup_day_Callback(hObject, eventdata, handles)
get(hObject,'Value');
guidata(hObject, handles);

% --- Executes on button press in checkbox_kdown_h.
function checkbox_kdown_h_Callback(hObject, eventdata, handles)
get(hObject,'Value');
guidata(hObject, handles);

% --- Executes on button press in checkbox_kdown_day.
function checkbox_kdown_day_Callback(hObject, eventdata, handles)
get(hObject,'Value');
guidata(hObject, handles);

% --- Executes on button press in checkbox_gvf_h.
function checkbox_gvf_h_Callback(hObject, eventdata, handles)
get(hObject,'Value');
guidata(hObject, handles);

% --- Executes on button press in checkbox_gvf_day.
function checkbox_gvf_day_Callback(hObject, eventdata, handles)
get(hObject,'Value');
guidata(hObject, handles);

% --- Executes on button press in checkbox_svfbuveg.
function checkbox_svfbuveg_Callback(hObject, eventdata, handles)
get(hObject,'Value');
guidata(hObject, handles);

% --- Executes on button press in checkbox_svfveg.
function checkbox_svfveg_Callback(hObject, eventdata, handles)
get(hObject,'Value');
guidata(hObject, handles);

% --- Executes on button press in checkbox_svfbu.
function checkbox_svfbu_Callback(hObject, eventdata, handles)
get(hObject,'Value');
guidata(hObject, handles);


% --- Executes on button press in checkbox_sh_day.
function checkbox_sh_day_Callback(hObject, eventdata, handles)
get(hObject,'Value');
guidata(hObject, handles);

% --- Executes on button press in checkbox_sh_hour.
function checkbox_sh_hour_Callback(hObject, eventdata, handles)
get(hObject,'Value');
guidata(hObject, handles);

% --- Executes on button press in checkbox_ascii.
function checkbox_ascii_Callback(hObject, eventdata, handles)
get(hObject,'Value');
guidata(hObject, handles);

% --- Executes on button press in checkbox_tif.
function checkbox_tif_Callback(hObject, eventdata, handles)
get(hObject,'Value');
guidata(hObject, handles);

% --- Executes on button press in pushbutton_outputfolder.
function pushbutton_outputfolder_Callback(hObject, ~, handles)
directory=uigetdir(pwd,'Specify outputfolder');
directory=[directory filesep];
set(handles.pushbutton_outputfolder,'Userdata',directory)
set(handles.pushbutton_close,'Enable','on');
guidata(hObject, handles);

% --- Executes on button press in pushbutton_close.
function pushbutton_close_Callback(hObject, ~, handles)
% get the main_gui handle (access to the gui)
mainGUIhandle = SOLWEIG;       
% get the data from the gui (all handles inside gui_main)
mainGUIdata  = guidata(mainGUIhandle);

output(1)=get(handles.checkbox_tmrt_h,'Value');
output(2)=get(handles.checkbox_tmrt_day,'Value');
output(3)=get(handles.checkbox_tmrt_diurn,'Value');
output(4)=get(handles.checkbox_lup_h,'Value');
output(5)=get(handles.checkbox_lup_day,'Value');
output(6)=get(handles.checkbox_lup_diurn,'Value');
output(7)=get(handles.checkbox_ldown_h,'Value');
output(8)=get(handles.checkbox_ldown_day,'Value');
output(9)=get(handles.checkbox_ldown_diurn,'Value');
output(10)=get(handles.checkbox_kup_h,'Value');
output(11)=get(handles.checkbox_kup_day,'Value');
output(12)=get(handles.checkbox_kdown_h,'Value');
output(13)=get(handles.checkbox_kdown_day,'Value');
output(14)=get(handles.checkbox_gvf_h,'Value');
output(15)=get(handles.checkbox_gvf_day,'Value');
output(16)=get(handles.checkbox_svfbu,'Value');
output(17)=get(handles.checkbox_svfveg,'Value');
output(18)=get(handles.checkbox_svfbuveg,'Value');
output(19)=get(handles.checkbox_ascii,'Value');
output(20)=get(handles.checkbox_tif,'Value');
output(21)=get(handles.checkbox_sh_hour,'Value');
output(22)=get(handles.checkbox_sh_day,'Value');
outputfolder=get(handles.pushbutton_outputfolder,'Userdata');

if sum(output(19:20))==0 && sum(output(1:18))>0
    msgboxText{1} =  'Choose at least one of the file formats';
    msgbox(msgboxText,'An output format much be chosen', 'error');
else
    set(mainGUIdata.pushbutton_output,'UserData',output)
    set(mainGUIdata.text_step4,'UserData',outputfolder)
    set(mainGUIdata.pushbutton_svf,'Enable','on');
    set(mainGUIdata.pushbutton_dailyshading, 'Enable','on');
    
    % save changed data back into main_gui
    %this line updates the data of the Main Gui
    guidata(SOLWEIG, mainGUIdata);
    
    % close this gui
    close(OutputSettings)
end
