function [vegcoord_out,vegdem,vegdem2]=Removetree_GUI(a,sizex,sizey,buildings,vegdem,vegdem2,idremove,vegcoord_out,scale)
auto=1;
id=idremove;
vegcoord_out(id,1:7)=0;

vegdem=zeros(size(a,1),size(a,2));
vegdem2=zeros(size(a,1),size(a,2));

% Putting out vegetation units
for i=1:length(vegcoord_out(:,1))
    id=vegcoord_out(i,1);
    ttype=vegcoord_out(i,2);

    dia=vegcoord_out(i,3);
    height=vegcoord_out(i,4);
    trunk=vegcoord_out(i,5);
    rowa=vegcoord_out(i,6);
    cola=vegcoord_out(i,7);
    if ttype == 0
        ttype=3;rowa=5;cola=5;dia=2;
    end
    [vegunit,vegdem,vegdem2]=vegunitsgeneration(a,sizex,sizey,buildings,vegdem,vegdem2,vegcoord_out,id,ttype,dia,height,trunk,auto,rowa,cola,scale);
end


