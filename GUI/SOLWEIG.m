function varargout = SOLWEIG(varargin)
% SOLWEIG MATLAB code for SOLWEIG.fig
%
% This is a main file for the SOLWEIG GUI
% Created by Fredrik Lindberg
% 2013-02-15, 
% Urban Climate Group, Gothenburg University
% fredrikl@gvc.gu.se
% mcc('-e','-R','-logfile,"errorlog.txt"','-v','SOLWEIG.m')


% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @SOLWEIG_OpeningFcn, ...
                   'gui_OutputFcn',  @SOLWEIG_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before SOLWEIG is made visible.
function SOLWEIG_OpeningFcn(hObject, ~, handles, varargin)
% This function has no output args, see OutputFcn.

% rdate=datevec(now);
% selectedDate = uical([date(1) date(2) date(3)],'en');
selectedDate = floor(now);
set(handles.text_date,'Value',selectedDate)
set(handles.text_date, 'String', datestr(selectedDate, 24));

data=importdata([pwd filesep 'locations.txt'],'\t',1);
textdata=data.textdata;
% data=data.data;

for i=2:size(textdata,1)
    textdata2{i-1,1}=strcat(textdata{i,1},', ',textdata{i,2});
end
set(handles.popupmenu_cityname,'String',textdata2)
% set(mainGUIdata.popupmenu_cityname,'Value',size(textdata2,1))

% Choose default command line output for SOLWEIG
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes SOLWEIG wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = SOLWEIG_OutputFcn(~, ~, handles) 

% Get default command line output from handles structure
varargout{1} = handles.output;

% --- Executes on button press in pushbutton_dsm.
function pushbutton_dsm_Callback(hObject, ~, handles)
DigitalSurfaceModels()

% --- Executes on button press in pushbutton_metdata.
function pushbutton_metdata_Callback(hObject, ~, handles)
LoadMeteorology()

% --- Executes on button press in pushbutton_svf.
function pushbutton_svf_Callback(hObject, ~, handles)
LoadCreateSVFs()

% --- Executes on selection change in popupmenu_cityname.
function popupmenu_cityname_Callback(hObject, ~, handles)
row=get(hObject,'Value');
data=importdata([pwd filesep 'locations.txt'],'\t',1);
data=data.data;
set(handles.text_lon,'String',num2str(data((row),1)))
set(handles.text_lat,'String',num2str(data((row),2)))
set(handles.text_alt,'String',num2str(data((row),3)))
set(handles.text_utc,'String',num2str(data((row),4)))
set(handles.text_lon,'Value',(data((row),1)))
set(handles.text_lat,'Value',(data((row),2)))
set(handles.text_alt,'Value',(data((row),3)))
set(handles.text_utc,'Value',(data((row),4)))
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function popupmenu_cityname_CreateFcn(hObject, ~, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on button press in pushbutton_addlocation.
function pushbutton_addlocation_Callback(hObject, ~, handles)
NewLocation()

% --- Executes on button press in pushbutton_output.
function pushbutton_output_Callback(hObject, ~, handles)
OutputSettings()

function edit_absK_Callback(hObject, ~, handles)
input = str2double(get(hObject,'String'));
if input>1 || input<0
    msgboxText{1} =  'Choose a decimal number between 0 and 1';
    msgbox(msgboxText,'Number has to be between 0 and 1', 'error');
end
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function edit_absK_CreateFcn(hObject, ~, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit_absL_Callback(hObject, ~, handles)
input = str2double(get(hObject,'String'));
if input>1 || input<0
    msgboxText{1} =  'Choose a decimal number between 0 and 1';
    msgbox(msgboxText,'Number has to be between 0 and 1', 'error');
end
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function edit_absL_CreateFcn(hObject, ~, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on selection change in popupmenu_posture.
function popupmenu_posture_Callback(hObject, ~, handles)
get(hObject,'String');
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function popupmenu_posture_CreateFcn(hObject, ~, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit_albedo_b_Callback(hObject, ~, handles)
input = str2double(get(hObject,'String'));
if input>1 || input<0
    msgboxText{1} =  'Choose a decimal number between 0 and 1';
    msgbox(msgboxText,'Number has to be between 0 and 1', 'error');
end
guidata(hObject, handles);

function edit_albedo_b_CreateFcn(hObject, ~, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit_albedo_g_Callback(hObject, eventdata, handles)
input = str2double(get(hObject,'String'));
if input>1 || input<0
    msgboxText{1} =  'Choose a decimal number between 0 and 1';
    msgbox(msgboxText,'Number has to be between 0 and 1', 'error');
end
guidata(hObject, handles);

function edit_albedo_g_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit_ewalls_Callback(hObject, ~, handles)
input = str2double(get(hObject,'String'));
if input>1 || input<0
    msgboxText{1} =  'Choose a decimal number between 0 and 1';
    msgbox(msgboxText,'Number has to be between 0 and 1', 'error');
end
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function edit_ewalls_CreateFcn(hObject, ~, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit_eground_Callback(hObject, ~, handles)
input = str2double(get(hObject,'String'));
if input>1 || input<0
    msgboxText{1} =  'Choose a decimal number between 0 and 1';
    msgbox(msgboxText,'Number has to be between 0 and 1', 'error');
end
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function edit_eground_CreateFcn(hObject, ~, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit_trans_Callback(hObject, ~, handles)
input = str2double(get(hObject,'String'));
if input>1 || input<0
    msgboxText{1} =  'Choose a decimal number between 0 and 1';
    msgbox(msgboxText,'Number has to be between 0 and 1', 'error');
end
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function edit_trans_CreateFcn(hObject, ~, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on button press in pushbutton_execute.
function pushbutton_execute_Callback(~, ~, handles)

% Get building and ground DSM
dsm=get(handles.pushbutton_dsm,'UserData');
a=dsm.a;
scale=dsm.scale;
header=dsm.header;
headernum=dsm.headernum;
headername=dsm.headername;
sizey=dsm.sizey;
sizex=dsm.sizex;
clear dsm

% Get Vegetation DSMs
veg=get(handles.text_step1,'UserData');
if isempty(veg)
    vegdem=[];vegdem2=[];buildings=[]; 
else
    vegdem=veg.vegdem;
    vegdem2=veg.vegdem2;
    buildings=get(handles.text_step5,'UserData');
end

% Get Building SVFs
svfs=get(handles.pushbutton_svf,'UserData');
svf=svfs.svf;
svfE=svfs.svfE;
svfS=svfs.svfS;
svfW=svfs.svfW;
svfN=svfs.svfN;
clear svfs

% Get Vegetation SVFs
vegsvfs=get(handles.text_step2,'UserData');
if isempty(vegsvfs)
    svfveg=1;svfNveg=1;svfEveg=1;svfSveg=1;svfWveg=1;
    svfaveg=1;svfNaveg=1;svfEaveg=1;svfSaveg=1;svfWaveg=1;
else
    svfveg=vegsvfs.svfveg;
    svfEveg=vegsvfs.svfEveg;
    svfSveg=vegsvfs.svfSveg;
    svfWveg=vegsvfs.svfWveg;
    svfNveg=vegsvfs.svfNveg;
    svfaveg=vegsvfs.svfaveg;
    svfEaveg=vegsvfs.svfEaveg;
    svfSaveg=vegsvfs.svfSaveg;
    svfWaveg=vegsvfs.svfWaveg;
    svfNaveg=vegsvfs.svfNaveg;
end
clear vegsvfs

% Location and meteorology
location.longitude=str2num(get(handles.text_lon,'String'));
location.latitude=str2num(get(handles.text_lat,'String'));
location.altitude=str2num(get(handles.text_alt,'String'));
mets=get(handles.pushbutton_metdata,'UserData');
met=mets.met;
% met_header=mets.met_header;
% time=mets.time;
altitude=mets.altitude;
azimuth=mets.azimuth;
zen=mets.zen;
jday=mets.jday;
leafon=mets.leafon;
YYYY=mets.YYYY;
% time.UTC=str2num(get(handles.text_utc,'String'));
clear mets

% Load outputfolder and output structure
out=get(handles.pushbutton_output,'UserData');
coreoutputfolder=get(handles.text_step4,'UserData');
output.tmrthour=out(1);output.tmrtday=out(2);output.tmrtdiurn=out(3);
output.luphour=out(4);output.lupday=out(5);output.lupdiurn=out(6);
output.ldownhour=out(7);output.ldownday=out(8);output.ldowndiurn=out(9);
output.kuphour=out(10);output.kupday=out(11);
output.kdownhour=out(12);output.kdownday=out(13);
output.gvfhour=out(14);output.gvfday=out(15);
output.svf=out(16);output.svfveg=out(17);output.svfboth=out(18);
fileformat.asc=out(19);fileformat.tif=out(20);
clear out

% load other parameters
albedo_b=str2double(get(handles.edit_albedo_b,'String'));
albedo_g=str2double(get(handles.edit_albedo_g,'String'));
sensorheight=str2double(get(handles.edit_sensorheight,'String'));
ewall=str2double(get(handles.edit_ewalls,'String'));
eground=str2double(get(handles.edit_eground,'String'));
absK=str2double(get(handles.edit_absK,'String'));
absL=str2double(get(handles.edit_absL,'String'));
paste=get(handles.popupmenu_posture,'Value');
trans=str2double(get(handles.edit_trans,'String'));
if paste==1,Fside=0.22;Fup=0.06;PA='Standing';end
if paste==2,Fside=0.166;Fup=Fside;PA='Sitting';end
height=1.1;
onlyglobal=get(handles.text_step3,'UserData');
if isempty(vegdem)
    usevegdem=0;
else
    usevegdem=get(handles.checkbox_useveg,'Value');
    if usevegdem==0, usevegdem=1;else usevegdem=0;end
end
showimage=get(handles.checkbox_showimage,'Value');

poi=get(handles.pushbutton_poi,'Userdata');
if isempty(poi)
    row=[];col=[];
else
    row=poi(1);
    col=poi(2);
end

landuse=[]; % Not active
% Solweig_2013a_core(coreoutputfolder,a,scale,header,sizey,sizex,row,col,svf,svfN,svfW,svfE,svfS,svfveg,...
%     svfNveg,svfEveg,svfSveg,svfWveg,svfaveg,svfEaveg,svfSaveg,svfWaveg,svfNaveg,vegdem,vegdem2,albedo,...
%     absK,absL,ewall,eground,Fside,Fup,PA,met,time,altitude,azimuth,zen,jday,XM,XD,showimage,usevegdem,...
%     onlyglobal,buildings,location,height,trans,output,fileformat,landuse,pet);
Solweig_2014a_core(coreoutputfolder,a,scale,header,sizey,sizex,row,col,svf,svfN,svfW,svfE,svfS,svfveg,...
    svfNveg,svfEveg,svfSveg,svfWveg,svfaveg,svfEaveg,svfSaveg,svfWaveg,svfNaveg,vegdem,vegdem2,albedo_b,...
    albedo_g,absK,absL,ewall,eground,Fside,Fup,PA,met,YYYY,altitude,azimuth,zen,jday,showimage,usevegdem,...
    onlyglobal,buildings,location,height,trans,output,fileformat,landuse,sensorheight,leafon);

% --- Executes on button press in pushbutton_poi.
function pushbutton_poi_Callback(hObject, ~, handles)
figurehandler=show_figure([50 100 750 650],'none','Set Point of Interest','none');
% Get building and ground DSM
dsm=get(handles.pushbutton_dsm,'UserData');
a=dsm.a;
sizey=dsm.sizey;
sizex=dsm.sizex;
clear dsm

% Get Vegetation DSMs
veg=get(handles.text_step1,'UserData');
if isempty(veg)
    vegdem=[];
    vegdem2=0;
else
    vegdem=veg.vegdem;
end
[row,col]=Solweig_20_poi(figurehandler,a,sizex,sizey,vegdem);
close_figure(figurehandler);
set(handles.pushbutton_poi,'Userdata',[row;col]);
% getframe(handles.axes1)
% imagesc(a+vegdem2);
% hold on
% plot(handles.axes1,col,row,'r *');
% pause(0.1);
guidata(hObject, handles);

function edit_sensorheight_Callback(hObject, eventdata, handles)
input = str2double(get(hObject,'String'));
if input>100 || input<0
    msgboxText{1} =  'Choose a decimal number between 0 and 100';
    msgbox(msgboxText,'Number has to be between 0 and 100', 'error');
end
guidata(hObject, handles);

function edit_sensorheight_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on button press in pushbutton_dailyshading.
function pushbutton_dailyshading_Callback(hObject, ~, handles)
% time.month=input('Month(MM):  ');
% time.day=input('Day(DD):  ');
jday=get(handles.text_date,'Value');
jday=datevec(jday);
time.year=jday(1);
time.month=jday(2);
time.day=jday(3);
dtime=str2num(get(handles.edit_interval,'String'));
out=get(handles.pushbutton_output,'UserData');
coreoutputfolder=get(handles.text_step4,'UserData');
output.shadowhour=out(21);output.shadowday=out(22);
fileformat.asc=out(19);fileformat.tif=out(20);

UTC=str2num(get(handles.text_utc,'String'));
location.longitude=str2num(get(handles.text_lon,'String'));
location.latitude=str2num(get(handles.text_lat,'String'));
location.altitude=str2num(get(handles.text_alt,'String'));
showimage=get(handles.checkbox_showimage,'Value');
[altitude,azimuth] = Dailyshading_param(location, UTC, time, dtime);

% Get building and ground DSM
dsm=get(handles.pushbutton_dsm,'UserData');
a=dsm.a;
scale=dsm.scale;
header=dsm.header;
headernum=dsm.headernum;
headername=dsm.headername;
sizey=dsm.sizey;
sizex=dsm.sizex;
clear dsm

% Get Vegetation DSMs
veg=get(handles.text_step1,'UserData');
if isempty(veg)
    vegdem=[];vegdem2=[];%buildings=[]; 
else
    vegdem=veg.vegdem;
    vegdem2=veg.vegdem2;
    %buildings=veg.buildings;
end
if isempty(vegdem)
    usevegdem=0;
else
    usevegdem=get(handles.checkbox_useveg,'Value');
    if usevegdem==0, usevegdem=1;else usevegdem=0;end
end

poi=get(handles.pushbutton_poi,'Userdata');
if isempty(poi)
    row=[];col=[];
else
    row=poi(1);
    col=poi(2);
end
Dailyshading_core(coreoutputfolder,a,vegdem,vegdem2,scale,sizey,sizex,altitude,azimuth,showimage,usevegdem,time,row,col,dtime,header,output,fileformat);

% --- Executes on button press in pushbutton_setdate.
function pushbutton_setdate_Callback(hObject, ~, handles)
date=datevec(now);
selectedDate = uical([date(1) date(2) date(3)],'en');
set(handles.text_date,'Value',selectedDate)
guidata(hObject, handles);
set(handles.text_date, 'String', datestr(selectedDate, 24));

function edit_interval_Callback(hObject, ~, handles)
input = str2double(get(hObject,'String'));
if input>720 || input<1
    msgboxText{1} =  'Choose a decimal number between 0 and 720';
    msgbox(msgboxText,'Number has to be between 0 and 720', 'error');
end
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function edit_interval_CreateFcn(hObject, ~, handles)
% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on button press in checkbox_showimage.
function checkbox_showimage_Callback(hObject, ~, handles)
get(hObject,'Value');
guidata(hObject, handles);

% --- Executes on button press in checkbox_useveg.
function checkbox_useveg_Callback(hObject, ~, handles)
get(hObject,'Value');
guidata(hObject, handles);

% MENUES--------------------------------------------------------------------
function menu_file_Callback(hObject, ~, handles)

% --------------------------------------------------------------------
function menu_exit_Callback(hObject, ~, handles)
close(SOLWEIG)

% --------------------------------------------------------------------
function help_Callback(hObject, eventdata, handles)


% --------------------------------------------------------------------
function about_Callback(hObject, eventdata, handles)
h=figure;
set(h,'Toolbar','none','menubar','none','Name',' ','Numbertitle','off');
p=imread('logo.jpg');
imshow(p);
annotation(h,'textbox',...
    [0.22 0.043 0.56 0.089],...
    'String',{'Contact: Fredrik Lindberg (fredrikl@gvc.gu.se)','Build 1 - 2014-5-13'},...
    'HorizontalAlignment','center','FontName','Calibri','FitBoxToText','off');
title('SOLWIEG, version 2014a','FontWeight','Bold','FontName','Calibri','FontSize',14)


% --------------------------------------------------------------------
function manual_Callback(hObject, eventdata, handles)
open([pwd '\manual\SOLWEIG_manual.pdf'])
