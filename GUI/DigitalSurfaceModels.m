function varargout = DigitalSurfaceModels(varargin)
% DIGITALSURFACEMODELS MATLAB code for DigitalSurfaceModels.fig

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
    'gui_Singleton',  gui_Singleton, ...
    'gui_OpeningFcn', @DigitalSurfaceModels_OpeningFcn, ...
    'gui_OutputFcn',  @DigitalSurfaceModels_OutputFcn, ...
    'gui_LayoutFcn',  [] , ...
    'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

% --- Executes just before DigitalSurfaceModels is made visible.
function DigitalSurfaceModels_OpeningFcn(hObject, ~, handles, varargin)
% get the main_gui handle (access to the gui)
mainGUIhandle = SOLWEIG;       
% get the data from the gui (all handles inside gui_main)
mainGUIdata  = guidata(mainGUIhandle);

dsm=get(mainGUIdata.pushbutton_dsm, 'UserData');
if isempty(dsm)
else
    set(handles.text_dsm,'String','Building DSM loaded','ForegroundColor',[0 0 1])
    veg=get(mainGUIdata.text_step1, 'UserData');
    if isempty(veg)
        set(handles.pushbutton_createveg,'Enable','on')
        set(handles.pushbutton_loadvegtext,'Enable','on')
        set(handles.pushbutton_loadvegdsm,'Enable','on')
    else
        set(handles.pushbutton_createveg,'Enable','on')
        set(handles.pushbutton_loadvegtext,'Enable','on')
        set(handles.pushbutton_edit,'Enable','on')
        set(handles.text_vegdsm,'String','Vegetation DSMs loaded','ForegroundColor',[0 0 1])
        set(handles.pushbutton_loadvegdsm,'Enable','on')
    end
end
    
    

% Choose default command line output for DigitalSurfaceModels
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes DigitalSurfaceModels wait for user response (see UIRESUME)
% uiwait(handles.figure1);

% --- Outputs from this function are returned to the command line.
function varargout = DigitalSurfaceModels_OutputFcn(hObject, ~, handles)

varargout{1} = handles.output;

% --- Executes on button press in pushbutton_loaddsm.
function pushbutton_loaddsm_Callback(hObject, ~, handles)
[inputfile directory]=uigetfile({'*.asc;*.txt'},'Select ESRI ASCIIgrid file');

if inputfile==0
else
    [a,scale,header,headernum,headername,sizey,sizex]=Solweig_10_loaddem([directory inputfile]);
    set(handles.text_dsm,'String','Building DSM loaded','ForegroundColor',[0 0 1])
    set(handles.text_vegdsm,'String','Vegetation DSMs not loaded','ForegroundColor',[1 0 0])
    dsm.a=a;
    dsm.scale=scale;
    dsm.header=header;
    dsm.headernum=headernum;
    dsm.headername=headername;
    dsm.sizey=sizey;
    dsm.sizex=sizex;
    set(handles.pushbutton_loaddsm,'UserData',dsm);
    set(handles.pushbutton_continue, 'UserData',[]);
    set(handles.pushbutton_loadvegtext,'Enable','on')
    set(handles.pushbutton_loadvegdsm,'Enable','on')
    set(handles.pushbutton_createveg,'Enable','on')
    set(handles.pushbutton_continue,'Enable','on')
    guidata(hObject, handles);
    
end

% --- Executes on button press in pushbutton_createveg.
function pushbutton_createveg_Callback(hObject, ~, handles)


% % get the main_gui handle (access to the gui)
% mainGUIhandle = SOLWEIG;       
% % get the data from the gui (all handles inside gui_main)
% mainGUIdata  = guidata(mainGUIhandle);

h=figure;
set(h,'Toolbar','none','menubar','none','Name','Vegetation DEM generation','Numbertitle','off');

% Marking Buildings
% dsm=get(mainGUIdata.pushbutton_dsm, 'UserData');
% if isempty(dsm)
dsm=get(handles.pushbutton_loaddsm,'UserData');
sizey=dsm.sizey;
sizex=dsm.sizex;
[build,index,loctemp]=building_edges(dsm.a,sizey,sizex);
k=1;
 while k ~= 3
     k = menuMarkBuildings('Options','Mark buildings','Undo','Done');
     if k == 1
         [build,index,loctemp]=mark_buildings(build,index,loctemp,sizex,sizey);
     elseif k == 2
         [build,index,loctemp]=undo_mark_building(build,index,loctemp,sizex,sizey);
     elseif k == 3
         [buildings,vegcoord_out]=final_mark_buildings(build,index,loctemp,sizex,sizey);
     end
 end
 close_figure(1)
 set(handles.uipanel_vegtext,'UserData',vegcoord_out)
 set(handles.pushbutton_createveg,'UserData',buildings)
 set(handles.text_vegdsm,'Value',0)
 CreateEditVegetation()
 set(handles.pushbutton_edit,'Enable','on')
 guidata(hObject, handles);

% --- Executes on button press in pushbutton_loadvegtext.
function pushbutton_loadvegtext_Callback(hObject, ~, handles)
[inputfile directory]=uigetfile({'*.txt'},'Select text file');
dsm=get(handles.pushbutton_loaddsm,'UserData');
a=dsm.a;
sizex=dsm.sizex;
sizey=dsm.sizey;
scale=dsm.scale;
[vegdem,vegdem2,vegcoord_out,buildings,id,auto]=vegDEMautogeneration(a,sizex,sizey,[directory inputfile],scale);
if isempty(vegdem)==0
    set(handles.text_vegdsm,'String','Vegetation DSMs loaded','ForegroundColor',[0 0 1])
end
veg.vegdem=vegdem;
veg.vegdem2=vegdem2;
veg.vegcoord_out=vegcoord_out;
veg.id=id;
veg.auto=auto;
set(handles.pushbutton_createveg,'UserData',buildings);
set(handles.pushbutton_continue,'UserData',veg);
set(handles.text_vegdsm,'Value',1)
set(handles.pushbutton_edit,'Enable','on')
guidata(hObject, handles);

% --- Executes on button press in pushbutton_edit.
function pushbutton_edit_Callback(hObject, ~, handles)
% set(handles.uipanel_vegtext,'UserData',vegcoord_out)
% set(handles.pushbutton_createveg,'UserData',buildings)
set(handles.text_vegdsm,'Value',1)
CreateEditVegetation()
guidata(hObject, handles);
 
 % --- Executes on button press in pushbutton_loadvegdsm.
function pushbutton_loadvegdsm_Callback(hObject, ~, handles)
LoadVegetationGrids()

% --- Executes on button press in pushbutton_continue.
function pushbutton_continue_Callback(~, ~, handles)
% get the main_gui handle (access to the gui)
mainGUIhandle = SOLWEIG;       
% get the data from the gui (all handles inside gui_main)
mainGUIdata  = guidata(mainGUIhandle);
 
dsm=get(handles.pushbutton_loaddsm, 'UserData');
veg=get(handles.pushbutton_continue, 'UserData');
% buidlings=get(handles.pushbutton_createveg,'UserData');
% set(mainGUIdata.pushbutton_dsm, 'UserData',[]);
set(mainGUIdata.pushbutton_dsm, 'UserData',dsm);
set(mainGUIdata.text_step1, 'UserData',veg);
set(handles.pushbutton_loaddsm,'UserData',dsm);
% set(mainGUIdata.text_step5,'UserData')
% save('dsm.mat','dsm')
if not(isempty(veg))
    if isfield(veg,'vegcoord_out')
        if sum(veg.vegcoord_out(:,1))==0
            veg.vegdem=0;
        end
    end
else
    veg.vegdem=0;
end

imagesc(dsm.a+veg.vegdem,'parent',mainGUIdata.axes1);
colormap(mainGUIdata.axes1,copper)
set(mainGUIdata.axes1,'Visible','off')
set(mainGUIdata.text1,'Visible','off')
% axis(mainGUIdata.axes1, 'image')
set(mainGUIdata.axes1,'PlotBoxAspectRatio',[dsm.sizex dsm.sizey 1])
% set(mainGUIdata.axes2,'PlotBoxAspectRatio',[dsm.sizex dsm.sizey 1])
set(mainGUIdata.pushbutton_output, 'Enable','on');
set(mainGUIdata.checkbox_useveg, 'Enable','on');
set(mainGUIdata.pushbutton_poi, 'Enable','on');

% save changed data back into main_gui
%this line updates the data of the Main Gui
guidata(SOLWEIG, mainGUIdata);
 
% close this gui
close(DigitalSurfaceModels)
