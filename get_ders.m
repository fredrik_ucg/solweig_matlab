function [grad,asp] = get_ders(dem,cs)
% calculate slope and aspect (deg) using GRADIENT function
[fx,fy] = gradient(dem,cs,cs); % uses simple, unweighted gradient of immediate neighbours
[asp,grad]=cart2pol(fy,fx); % convert to carthesian coordinates
grad=atan(grad); %steepest slope
% asp=asp.*-1+pi; % convert asp 0 facing south
asp=asp.*-1; % convert asp to increase going counterclockwise

% Converting to 0 - 2*pi
temp=asp<0;
temp=temp*(pi*2);
asp=asp+temp;