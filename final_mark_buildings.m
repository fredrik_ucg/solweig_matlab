function [buildings,vegcoord_out]=final_mark_buildings(build,index,loctemp,sizex,sizey)
% New version where minfilter is used to compensate maxfilter in
% findbuildingedges.m
buildings=build(:,:,index-1);
buildings=(buildings-1)*-1;

% new code 2011-02-12
g=ordfilt2(buildings,5,ones(3,3));% Median filter to fill small holes
g=buildings-g;
g(g<0)=0;
buildings=buildings-g;
% buildings=ordfilt2(buildings,9,ones(3,3)); % (min) filter to shrink buildings

imagesc(buildings), axis image,colormap(1-gray)
locations=[];
if isempty(loctemp)
    vegcoord_out(:,8)=0;
else
    for i=1:length(loctemp(:,1))
        locations=[locations loctemp(i,:)];
    end
    takeout= locations>0;
    locations=locations(takeout)';
    vegcoord_out=[];
    vegcoord_out(:,8)=locations;
end

buildings(1:sizey,1)=1;
buildings(1:sizey,sizex)=1;
buildings(1,1:sizex)=1;
buildings(sizey,1:sizex)=1;

% if sizex<sizey
% %     disp('1')
%     pad=(zeros(sizey,(sizey-sizex)));
%     buildings=horzcat(buildings,pad);
% end
% if sizex>sizey
% %     disp('2')
%     pad=zeros((sizex-sizey),sizex);
%     buildings=vertcat(buildings,pad);
% end
