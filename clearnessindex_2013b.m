function [I0,CI,Kt,I0et,CIuncorr]=clearnessindex_2013b(zen,jday,Ta,RH,radG,location,P)

% Clearness Index at the Earth's surface calculated from Crawford and Duchon 1999

if P==-999
    p=1013; %Pressure in millibars
else
    p=P*10; %COnvert from hPa to millibars
end
Itoa=1370; %Effective solar constant
D=sun_distance(jday); %irradiance differences due to Sun-Earth distances
m=35*cos(zen)*((1224*(cos(zen)^2)+1)^(-1/2));     %optical air mass at p=1013
Trpg=1.021-0.084*(m*(0.000949*p+0.051))^0.5; %Transmission coefficient for Rayliegh scattering and permanent gases

% empirical constant depending on latitude
if location.latitude<10
G=[3.37 2.85 2.80 2.64];
elseif location.latitude>=10 && location.latitude<20
G=[2.99 3.02 2.70 2.93];
elseif location.latitude>=20 && location.latitude<30
G=[3.60 3.00 2.98 2.93];
elseif location.latitude>=30 && location.latitude<40
G=[3.04 3.11 2.92 2.94];
elseif location.latitude>=40 && location.latitude<50
G=[2.70 2.95 2.77 2.71];
elseif location.latitude>=50 && location.latitude<60
G=[2.52 3.07 2.67 2.93];
elseif location.latitude>=60 && location.latitude<70
G=[1.76 2.69 2.61 2.61];
elseif location.latitude>=70 && location.latitude<80
G=[1.60 1.67 2.24 2.63];
elseif location.latitude>=80 && location.latitude<90
G=[1.11 1.44 1.94 2.02];
end
if jday > 335 || jday <= 60, G=G(1); elseif jday > 60 && jday <= 152, G=G(2);...
        elseif jday > 152 && jday <= 244, G=G(3); elseif jday > 244 && jday <= 335, G=G(4); end
%dewpoint calculation
a2=17.27;b2=237.7;
Td=(b2*(((a2*Ta)/(b2+Ta))+log(RH)))/(a2-(((a2*Ta)/(b2+Ta))+log(RH)));
Td=(Td*1.8)+32; %Dewpoint (�F)
u=exp(0.1133-log(G+1)+0.0393*Td); %Precipitable water
Tw=1-0.077*((u*m)^0.3); %Transmission coefficient for water vapor
Tar=0.935^m; %Transmission coefficient for aerosols

I0=Itoa*cos(zen)*Trpg*Tw*D*Tar;
b=I0==abs(zen)>pi/2;I0(b==1)=0;clear b;
if not(isreal(I0)),I0=0;end
corr=0.1473*log(90-(zen/pi*180))+0.3454;% 20070329

CIuncorr=radG./I0;
CI=CIuncorr+(1-corr);
I0et=Itoa*cos(zen)*D;%extra terrestial solar radiation
Kt=radG./I0et;
if isnan(CI),CI=inf;end