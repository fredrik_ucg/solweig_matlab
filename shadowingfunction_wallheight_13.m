function [sh wallsh wallsun facesh facesun]=shadowingfunction_wallheight_13(a,azimuth,altitude,scale,walls,aspect)
%This m.file calculates shadows on a DSM and shadow height on building
%walls.
%
%INPUTS:
%a = DSM
%azimuth and altitude = sun position
%scale= scale of DSM (1 meter pixels=1, 2 meter pixels=0.5)
%walls= pixel row 'outside' buildings. will be calculated if empty
%aspect = normal aspect of buildings walls
%
%OUTPUT:
%sh=ground and roof shadow
%wallsh = height of wall that is in shadow
%wallsun = hieght of wall that is in sun
%
%Fredrik Lindberg 2012-03-19
%fredrikl@gvc.gu.se
%
% Utdate 2013-03-13 - bugfix for walls alinged with sun azimuths

if isempty(walls)==1
    walls=ordfilt2(a,4,[0 1 0; 1 0 1; 0 1 0]);
    walls=walls-a;
    walls(walls<3)=0;
    sizex=size(a,1);%might be wrong
    sizey=size(a,2);
    dirwalls = filter1Goodwin_as_aspect_v3(walls,sizex,sizey,scale,a);
    aspect=dirwalls*pi/180;
end

% conversion
degrees=pi/180;
azimuth=azimuth*degrees;
altitude=altitude*degrees;

% measure the size of the image
sizex=size(a,1);
sizey=size(a,2);

% initialise parameters
f=a;
dx=0;
dy=0;
dz=0;
temp=zeros(sizex,sizey);
index=1;
wallbol=double(walls>0);
wallbol(wallbol==0)=NaN;

% other loop parameters
amaxvalue=max(a(:));
pibyfour=pi/4;
threetimespibyfour=3*pibyfour;
fivetimespibyfour=5*pibyfour;
seventimespibyfour=7*pibyfour;
sinazimuth=sin(azimuth);
cosazimuth=cos(azimuth);
tanazimuth=tan(azimuth);
signsinazimuth=sign(sinazimuth);
signcosazimuth=sign(cosazimuth);
dssin=abs(1/sinazimuth);
dscos=abs(1/cosazimuth);
tanaltitudebyscale=tan(altitude)/scale;

% main loop
while ((amaxvalue>=dz) && (abs(dx)<=sizex) && (abs(dy)<=sizey))
    
    if ((pibyfour<=azimuth)&&(azimuth<threetimespibyfour))||((fivetimespibyfour<=azimuth)&&(azimuth<seventimespibyfour))
        dy=signsinazimuth*index;
        dx=-1*signcosazimuth*abs(round(index/tanazimuth));
        ds=dssin;
    else
        dy=signsinazimuth*abs(round(index*tanazimuth));
        dx=-1*signcosazimuth*index;
        ds=dscos;
    end
    
    % note: dx and dy represent absolute values while ds is an incremental value
    dz=ds*index*tanaltitudebyscale;
    temp(1:sizex,1:sizey)=0;
    
    absdx=abs(dx);
    absdy=abs(dy);
    
    xc1=((dx+absdx)/2)+1;
    xc2=(sizex+(dx-absdx)/2);
    yc1=((dy+absdy)/2)+1;
    yc2=(sizey+(dy-absdy)/2);
    
    xp1=-((dx-absdx)/2)+1;
    xp2=(sizex-(dx+absdx)/2);
    yp1=-((dy-absdy)/2)+1;
    yp2=(sizey-(dy+absdy)/2);
    
    temp(xp1:xp2,yp1:yp2)= a(xc1:xc2,yc1:yc2)-dz;
    
    f=max(f,temp);
    
    index=index+1;
    
end

% Removing walls in shadow due to selfshadowing
azilow=azimuth-pi/2;
azihigh=azimuth+pi/2;
if azilow>=0 && azihigh<2*pi % 90 to 270  (SHADOW)
    facesh=double((aspect<azilow | aspect>=azihigh)-wallbol+1);
    facesh(facesh==2)=0;
elseif azilow<0 && azihigh<=2*pi % 0 to 90
    azilow=azilow+2*pi;
    facesh=(aspect>azilow | aspect<=azihigh)*-1+1; %(SHADOW)
elseif azilow>0 && azihigh>=2*pi % 270 to 360
    azihigh=azihigh-2*pi;
    facesh=(aspect>azilow | aspect<=azihigh)*-1+1;%(SHADOW)
end

sh=f-a; %shadow volume
facesun=double((facesh+double(walls>0))==1 & walls>0);
wallsun=walls-sh;
wallsun(wallsun<0)=0;
wallsun(facesh==1)=0;% Removing walls in "self"-shadow
wallsh=walls-wallsun;

sh=not(not(sh));
sh=double(sh);
sh=sh*-1+1;

% subplot(2,2,1),imagesc(facesh),axis image ,colorbar,title('facesh')%
% subplot(2,2,2),imagesc(wallsh,[0 20]),axis image, colorbar,title('Wallsh')%
% subplot(2,2,3),imagesc(sh), colorbar,axis image,title('Groundsh')%
% subplot(2,2,4),imagesc(wallsun,[0 20]),axis image, colorbar,title('Wallsun')%

%% old stuff
%     if index==0 %removing shadowed walls at first iteration
%         tempfirst(1:sizex,1:sizey)=0;
%         tempfirst(xp1:xp2,yp1:yp2)= a(xc1:xc2,yc1:yc2);
%         tempfirst=tempfirst-a;
%         tempfirst(tempfirst<2)=1;% 2 is smallest wall height. Should be variable
%         tempfirst(tempfirst>=2)=0;% walls in shadow at first movment (iteration)
%         tempwalls(1:sizex,1:sizey)=0;
%         tempwalls(xp1:xp2,yp1:yp2)= wallbol(xc1:xc2,yc1:yc2);
%         wallfirst=tempwalls.*wallbol;%wallpixels potentially shaded by adjacent wall pixels
%         wallfirstaspect=aspect.*wallfirst;
%         azinormal=azimuth-pi/2;
%         if azinormal<=0,azinormal=azinormal+2*pi;end
%         facesh=double(wallfirstaspect<azinormal|tempfirst<=0);
%         facesun=double((facesh+double(walls>0))==1);
%     end


% Removing walls in shadow due to selfshadowing (This only works on
% regular arrays)
%     if dy~=0 && firsty==0
%         if yp1>1
%             yp1f=2;yp2f=sizey;
%             yc1f=1;yc2f=sizey-1;
%         else
%             yp1f=1;yp2f=sizey-1;
%             yc1f=2;yc2f=sizey;
%         end
%         firsty=1;
%     end
%     if dx~=0 && firstx==0
%         if xp1>1
%             xp1f=2;xp2f=sizex;
%             xc1f=1;xc2f=sizex-1;
%         else
%             xp1f=1;xp2f=sizex-1;
%             xc1f=2;xc2f=sizex;
%         end
%         firstx=1;
%     end
%     if firsty==1 && firstx==1
%         facesh(xp1f:xp2f,yp1f:yp2f)= a(xc1f:xc2f,yc1f:yc2f);
%         facesh=facesh-a;
%         facesh(facesh<=0)=0;
%         facesh(facesh>0)=1;
%     end


%     if index<3 %removing shadowed walls 1
%         tempfirst(1:sizex,1:sizey)=0;
%         tempfirst(xp1:xp2,yp1:yp2)= a(xc1:xc2,yc1:yc2);
%         %removing walls in shadow
%         tempfirst=tempfirst-a;
%         tempfirst(tempfirst<2)=1;% 2 is smallest wall height. Should be variable
%         tempfirst(tempfirst>=2)=0;
%         if index==1 % removing shadowed walls 2
%             tempwalls(1:sizex,1:sizey)=0;
%             tempwalls(xp1:xp2,yp1:yp2)= wallbol(xc1:xc2,yc1:yc2);
%             %             wallfirst=((tempwalls+wallbol).*wallbol)==2;
%             wallfirst=tempwalls.*wallbol;
%             wallfirstaspect=aspect.*wallfirst;%.*wallbol
%             %             wallfirstaspect(wallfirstaspect==0)=NaN;
%             wallfirstsun=wallfirstaspect>(azimuth-pi/2);
%             %             wallfirstsun=(wallfirstaspect>=azimuth-pi/2 & wallfirstaspect<=azimuth+pi/2);%%%H�R�RJAG
%             wallfirstshade=wallfirst-wallfirstsun;
%         end
%     end

