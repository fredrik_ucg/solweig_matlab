function Solweig_outputfiles_day_2013b(output,fileformat,Tmrtday,Kupday,Kdownday,Lupday,Ldownday,gvfday,outputfolder,YYYY,DOY,PA,header,sizey)

if(exist([outputfolder 'ImagesAndAsciiGrids'],'dir')==0)
    mkdir([outputfolder 'ImagesAndAsciiGrids']);
end

% Output of Tmrtmap, daytime average
if output.tmrtday==1
    if fileformat.asc==1
    fn2=fopen([outputfolder 'ImagesAndAsciiGrids' filesep 'AverageDaytimeTmrt_' num2str(YYYY) '_' num2str(DOY) '_' PA '.asc'],'w');
    for p=1:6
        fprintf(fn2,'%6s', header{p});
        fprintf(fn2,'\r\n');
    end
    for p=1:sizey
        fprintf(fn2,'%6.4f ',Tmrtday(p,:));
        fprintf(fn2,'\r\n');
    end
    fclose(fn2);
    end
    if fileformat.tif==1
       image_write(Tmrtday,[outputfolder 'ImagesAndAsciiGrids' filesep 'AverageDaytimeTmrt_' num2str(YYYY) '_' num2str(DOY) '_' PA '.tif'],'tif',-10,80); 
    end
end

% Output of Kupmap, daytime average
if output.kupday==1
    if fileformat.asc==1
    fn2=fopen([outputfolder 'ImagesAndAsciiGrids' filesep 'AverageDaytimeKup_' num2str(YYYY) '_' num2str(DOY) '_' PA '.asc'],'w');
    for p=1:6
        fprintf(fn2,'%6s', header{p});
        fprintf(fn2,'\r\n');
    end
    for p=1:sizey
        fprintf(fn2,'%6.4f ',Kupday(p,:));
        fprintf(fn2,'\r\n');
    end
    fclose(fn2);
    end
    if fileformat.tif==1
       image_write(Kupday,[outputfolder 'ImagesAndAsciiGrids' filesep 'AverageDaytimeKup_' num2str(YYYY) '_' num2str(DOY) '_' PA '.tif'],'tif',0,300); 
    end
end

% Output of Kdownmap, daytime average
if output.kdownday==1
    if fileformat.asc==1
    fn2=fopen([outputfolder 'ImagesAndAsciiGrids' filesep 'AverageDaytimeKdown_' num2str(YYYY) '_' num2str(DOY) '_' PA '.asc'],'w');
    for p=1:6
        fprintf(fn2,'%6s', header{p});
        fprintf(fn2,'\r\n');
    end
    for p=1:sizey
        fprintf(fn2,'%6.4f ',Kdownday(p,:));
        fprintf(fn2,'\r\n');
    end
    fclose(fn2);
    end
    if fileformat.tif==1
       image_write(Kdownday,[outputfolder 'ImagesAndAsciiGrids' filesep 'AverageDaytimeKdown_' num2str(YYYY) '_' num2str(DOY) '_' PA '.tif'],'tif',0,1000); 
    end
end

% Output of Lupmap, daytime average
if output.lupday==1
    if fileformat.asc==1
    fn2=fopen([outputfolder 'ImagesAndAsciiGrids' filesep 'AverageDaytimeLup_' num2str(YYYY) '_' num2str(DOY) '_' PA '.asc'],'w');
    for p=1:6
        fprintf(fn2,'%6s', header{p});
        fprintf(fn2,'\r\n');
    end
    for p=1:sizey
        fprintf(fn2,'%6.4f ',Lupday(p,:));
        fprintf(fn2,'\r\n');
    end
    fclose(fn2);
    end
    if fileformat.tif==1
       image_write(Lupday,[outputfolder 'ImagesAndAsciiGrids' filesep 'AverageDaytimeLup_' num2str(YYYY) '_' num2str(DOY) '_' PA '.tif'],'tif',200,500); 
    end
end

% Output of Ldownmap, daytime average
if output.ldownday==1
    if fileformat.asc==1
    fn2=fopen([outputfolder 'ImagesAndAsciiGrids' filesep 'AverageDaytimeLdown_' num2str(YYYY) '_' num2str(DOY) '_' PA '.asc'],'w');
    for p=1:6
        fprintf(fn2,'%6s', header{p});
        fprintf(fn2,'\r\n');
    end
    for p=1:sizey
        fprintf(fn2,'%6.4f ',Ldownday(p,:));
        fprintf(fn2,'\r\n');
    end
    fclose(fn2);
    end
    if fileformat.tif==1
       image_write(Ldownday,[outputfolder 'ImagesAndAsciiGrids' filesep 'AverageDaytimeLdown_' num2str(YYYY) '_' num2str(DOY) '_' PA '.tif'],'tif',200,500); 
    end
end

% Output of GVF, daytime average
if output.gvfday==1
    if fileformat.asc==1
    fn2=fopen([outputfolder 'ImagesAndAsciiGrids' filesep 'AverageDaytimeGVF_' num2str(YYYY) '_' num2str(DOY) '_' PA '.asc'],'w');
    for p=1:6
        fprintf(fn2,'%6s', header{p});
        fprintf(fn2,'\r\n');
    end
    for p=1:sizey
        fprintf(fn2,'%6.4f ',gvfday(p,:));
        fprintf(fn2,'\r\n');
    end
    fclose(fn2);
    end
    if fileformat.tif==1
       image_write(gvfday,[outputfolder 'ImagesAndAsciiGrids' filesep 'AverageDaytimeGVF_' num2str(YYYY) '_' num2str(DOY) '_' PA '.tif'],'tif',0,1); 
    end
end