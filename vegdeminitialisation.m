function [vegdem,vegdem2,auto,id]=vegdeminitialisation(a,vegcoord_out)
% This function gereates empty vegetation DEMs

auto=0;
vegdem=zeros(size(a,1),size(a,2));
vegdem2=zeros(size(a,1),size(a,2));
id=1;
% hold off
% figure('Position',[50 100 750 650])
% imagesc(vegdem+a),axis square
% set(gcf,'Toolbar','none','menubar','none','Name','Vegetation DEM generation','Numbertitle','off');
% text(vegcoord_out(:,7),vegcoord_out(:,6),int2str(vegcoord_out(:,1)),'Color',[1 1 1],'FontSize',12);
% hold on
