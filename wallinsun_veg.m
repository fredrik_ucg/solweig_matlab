function [sunwall] = wallinsun_veg(a,buildings,azimuth,sh,vegsh)
% This m-file creates a boolean image of sunlit walls. 
% Shadows from both buildings and vegetation is accounted for

azimuth=azimuth+180;% moving building in the direction of the sun
if azimuth>=360
    azimuth=azimuth-360;
end
% conversion into radians
azimuth=azimuth*(pi/180);

index=1;
dx=0;
dy=0;
dz=0;
ds=0; %#ok<NASGU>

sizex=size(a,1);
sizey=size(a,2);
temp=zeros(sizex,sizey);
sh=vegsh+sh-1;
%% The Shadow casting algoritm
    if ((pi/4<=azimuth)&&(azimuth<3*pi/4))||((5*pi/4<=azimuth)&&(azimuth<7*pi/4))
        dy=1*sign(sin(azimuth))*index;
        dx=-1*sign(cos(azimuth))*abs(round(index/tan(azimuth)));
        ds=abs(1/sin(azimuth));
    else
        dy=1*sign(sin(azimuth))*abs(round(index*tan(azimuth)));
        dx=-1*sign(cos(azimuth))*index;
        ds=abs(1/cos(azimuth));
    end

    % note: dx and dy represent absolute values while ds is an incremental value
    temp(1:sizex,1:sizey)=0;

    xc1=((dx+abs(dx))/2)+1;
    xc2=(sizex+(dx-abs(dx))/2);
    yc1=((dy+abs(dy))/2)+1;
    yc2=(sizey+(dy-abs(dy))/2);

    xp1=-((dx-abs(dx))/2)+1;
    xp2=(sizex-(dx+abs(dx))/2);
    yp1=-((dy-abs(dy))/2)+1;
    yp2=(sizey-(dy+abs(dy))/2);

    temp(xp1:xp2,yp1:yp2)= buildings(xc1:xc2,yc1:yc2);
    
    f1=temp-buildings;
    f1(f1==1)=0;
    f1(f1==-1)=1;
    sunwall=sh.*f1;


