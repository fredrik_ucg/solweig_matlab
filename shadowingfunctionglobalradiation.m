function f=shadowingfunctionglobalradiation(a,azimuth,altitude,scale)
%This m.file calculates shadows on a DEM

% conversion
degrees=pi/180;
azimuth=azimuth*degrees;
altitude=altitude*degrees;

% measure the size of the image
sizex=size(a,1);
sizey=size(a,2);

% initialise parameters
f=a;
dx=0;
dy=0;
dz=0;
temp=zeros(sizex,sizey);
index=1;

% other loop parameters
amaxvalue=max(max(a));
pibyfour=pi/4;
threetimespibyfour=3*pibyfour;
fivetimespibyfour=5*pibyfour;
seventimespibyfour=7*pibyfour;
sinazimuth=sin(azimuth);
cosazimuth=cos(azimuth);
tanazimuth=tan(azimuth);
signsinazimuth=sign(sinazimuth);
signcosazimuth=sign(cosazimuth);
dssin=abs(1/sinazimuth);
dscos=abs(1/cosazimuth);
tanaltitudebyscale=tan(altitude)/scale;

% main loop
while ((amaxvalue>=dz) && (abs(dx)<sizex) && (abs(dy)<sizey))
    if ((pibyfour<=azimuth)&&(azimuth<threetimespibyfour))||((fivetimespibyfour<=azimuth)&&(azimuth<seventimespibyfour))
		dy=signsinazimuth*index;
        dx=-1*signcosazimuth*abs(round(index/tanazimuth));
        ds=dssin;
    else
		dy=signsinazimuth*abs(round(index*tanazimuth));
        dx=-1*signcosazimuth*index;
        ds=dscos;
    end
    
% note: dx and dy represent absolute values while ds is an incremental value
	dz=ds*index*tanaltitudebyscale;
    temp(1:sizex,1:sizey)=0;
	
	absdx=abs(dx);
	absdy=abs(dy);
    
    xc1=((dx+absdx)/2)+1;
    xc2=(sizex+(dx-absdx)/2);
    yc1=((dy+absdy)/2)+1;
    yc2=(sizey+(dy-absdy)/2);
    
    xp1=-((dx-absdx)/2)+1;
    xp2=(sizex-(dx+absdx)/2);
    yp1=-((dy-absdy)/2)+1;
    yp2=(sizey-(dy+absdy)/2);
    
    temp(xp1:xp2,yp1:yp2)= a(xc1:xc2,yc1:yc2)-dz;

    f=max(f,temp);
    index=index+1;
end

f=f-a;
f=not(not(f));
f=double(f);
f=1-f;
