function [vegdem,vegdem2,vegcoord_out,buildings,id,auto]=vegDEMautogeneration(a,sizex,sizey,inputvegunitfile,scale)
% This function reads vegetation textfile and generates the vegetation DEMs

% Setup and loading of textfile
vegdem=zeros(size(a,1),size(a,2));
vegdem2=zeros(size(a,1),size(a,2));
auto=1;
Newdata1=importdata(inputvegunitfile,' ',1);
vegcoord_out=Newdata1.data;

% Finding buildings
g=findbuildingedges(a);

%fills outer pixels with one
g(1:sizey,1)=1;
g(1:sizey,sizex)=1;
g(1,1:sizex)=1;
g(sizey,1:sizex)=1;
locations=vegcoord_out(:,8);
takeout=locations>0;
locations=locations(takeout);
if isempty(locations)
    buildings=vegdem;
    else
    [buildings]=imfill(g,locations);
end
buildings=(buildings-1)*-1;

% new code 2011-02-12
g=ordfilt2(buildings,5,ones(3,3));% Median filter to fill small holes
g=buildings-g;
g(g<0)=0;
buildings=buildings-g;
% buildings=ordfilt2(buildings,9,ones(3,3)); % (min) filter to shrink buildings

% Putting out vegetation units
for i=1:length(vegcoord_out(:,1))
    id=vegcoord_out(i,1);
    ttype=vegcoord_out(i,2);
%     disp(i)%,imagesc(a+vegdem);axis image;hold on;pause(0.1)
    dia=round(vegcoord_out(i,3));
    height=vegcoord_out(i,4);
    trunk=vegcoord_out(i,5);
    rowa=vegcoord_out(i,6);
    cola=vegcoord_out(i,7);
    if ttype == 0
        ttype=3;rowa=5;cola=5;dia=2;
    end
    [vegunit,vegdem,vegdem2]=vegunitsgeneration(a,sizex,sizey,buildings,vegdem,vegdem2,vegcoord_out,id,ttype,dia,height,trunk,auto,rowa,cola,scale);
end
id=max(vegcoord_out(:,1))+1;
auto=0;
buildings(1:sizey,1)=1;
buildings(1:sizey,sizex)=1;
buildings(1,1:sizex)=1;
buildings(sizey,1:sizex)=1;

