function [location,UTC]=Solweig_10_loc(cityname)
% This m-file specifies the location of the model domain on Earth
% Longitude and latitude scould be in decimal degrees (e.g 55'N30'0 will be
% 55.5)
%
% Input
% cityname = a string specifying a city

if strcmp(cityname,'Goteborg') == 1
%Location input (G�teborg, Sweden)
location.longitude = 11.94; 
location.latitude = 57.70; 
location.altitude = 3;
UTC=1;
elseif strcmp(cityname,'Kassel') == 1 
%Location input (Kassel, Germany)
location.longitude = 9.50; 
location.latitude = 51.31; 
location.altitude = 152;
UTC=1;
elseif strcmp(cityname,'Freiburg') == 1 
%Location input (Germany, Freiburg)				
location.longitude = 7.85; 
location.latitude = 48.0; 
location.altitude = 278;
UTC=1;
elseif strcmp(cityname,'London') == 1
%Location input (UK, London)				
location.longitude = -0.11; 
location.latitude = 51.5; 
location.altitude = 3;
UTC=0;    
elseif strcmp(cityname,'Hongkong') == 1
%Location input (China, HongKong)				
location.longitude =114.17;
location.latitude = 22.30;
location.altitude = 3;
UTC=8; 
elseif strcmp(cityname,'LondonSouth') == 1
%Location input (UK, London)				
location.longitude = -0.17; 
location.latitude = -51.5; 
location.altitude = 3;
UTC=0; 
elseif strcmp(cityname,'Stockholm') == 1
%Location input (Sweden,Stockholm)				
location.longitude = 18.0; 
location.latitude = 59.0; 
location.altitude = 3;
UTC=1;
elseif strcmp(cityname,'Lule�') == 1
%Location input (Sweden,Lule�)				
location.longitude = 22.11; 
location.latitude = 65.54; 
location.altitude = 3;
UTC=1;
elseif strcmp(cityname,'Frankfurt') == 1
% Location input (Germany, Frankfurt)
location.longitude = 8.68; 
location.latitude = 50.1; 
location.altitude = 103;
UTC=1;
elseif strcmp(cityname,'Porto') == 1
% Location input (Portugal, Porto)			
location.longitude = -8.62; 
location.latitude = 41.15; 
location.altitude = 95;
UTC=0;
elseif strcmp(cityname,'Rotterdam') == 1
% Location input (Netherlands, Rotterdam)	
location.longitude = 4.4811; 
location.latitude = 51.9217; 
location.altitude = 1;
UTC=0;
elseif strcmp(cityname,'Vancouver') == 1
% Location input (Canada, Vancover)	
location.longitude = -123.0784; 
location.latitude = 49.2261; 
location.altitude = 3;
UTC=-8;
end