function tree=conifertree(dia)
circle=imcircle(dia);
dia=size(circle,1);
index=1;
while dia-index*2>=1
    if dia-index*2>=2
        circle2=imcircle(dia-index*2);
        circle3=zeros(size(circle));
        circle3(index+1:length(circle2)+index,index+1:length(circle2)+index)=circle2;
        circle=circle+circle3;
        index=index+1;
    end
    if dia-index*2==1
        circle3=zeros(size(circle));
        circle3(index+1,index+1)=1;
        circle=circle+circle3;
        index=index+1;
    end
end
tree=circle./max(max(circle));