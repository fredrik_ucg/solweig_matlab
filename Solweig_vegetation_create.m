function [vegdem,vegdem2,buildings,walls]=Solweig_vegetation_create(usevegdem,gridfiles,oldtextfile,a,sizex,sizey,vegetationfile,scale,cdsmfile,tdsmfile,onlycdsm,trunkratio)
% This m-file is used to create vegetation DSMs and building grid
if usevegdem==1
    if gridfiles==0
%         oldvegdem=1;
        if oldtextfile==1;
            [vegdem,vegdem2,vegcoord_out,buildings,id,auto]=vegDEMautogeneration(a,sizex,sizey,vegetationfile,scale);
        end
        if oldtextfile ==0
            % Marking Buildings
            [build,index,loctemp,walls]=building_edges(a,sizey,sizex);
            k=1;
            while k ~= 3
                k = menuMarkBuildings('Options','Mark buildings','Undo','Done');
                if k == 1
                    [build,index,loctemp]=mark_buildings(build,index,loctemp,sizex,sizey);
                elseif k == 2
                    [build,index,loctemp]=undo_mark_building(build,index,loctemp,sizex,sizey);
                elseif k == 3
                    [buildings,vegcoord_out]=final_mark_buildings(build,index,loctemp,sizex,sizey);
                end
            end
            close_figure(1)
        end
        % Setting out Vegetation units
        if oldtextfile == 0
            [vegdem,vegdem2,auto,id]=vegdeminitialisation(a,vegcoord_out);
            figurehandler=show_figure([50 100 750 650],'none','BLA BLA','none');
            showinitialvegdem(a,sizex,sizey,vegdem,vegcoord_out);
        end
%          point=input('Start adding vegetation units? (Y or N):  ','s');
        point='n'; % this is inactive without the GUI
        while strcmpi(point,'y')
            ttype=input('Conifer, Deciduous or Bush (C, D or B) ','s');
            if ttype == 'c',ttype='C'; end
            if ttype == 'd',ttype='D'; end
            if ttype == 'b',ttype='B'; end
            if ttype == 'C',ttype=1; end
            if ttype == 'D',ttype=2; end
            if ttype == 'B',ttype=3; end
            if ttype < 3
                dia=input('Diameter of tree canopy (in meters) ');
                height=input('Height of tree canopy (in meters) ');
                trunk=input('Height of tree trunk (in meters) ');
            else
                dia=input('Diameter of bush (in meters) ');
                height=input('Height of bush canopy (in meters) ');
                trunk=0;
            end
            [vegcoord_out,vegdem,vegdem2,id]=vegDEMgen(a,sizex,sizey,buildings,vegdem,vegdem2,id,ttype,...
                dia,height,trunk,auto,vegcoord_out,scale);
            
            removetree=input('Do you want to delete a vegetation unit? (Y or N): ','s');
            if strcmpi(removetree,'y')
                idremove=input('What number? ');
                [vegcoord_out,vegdem,vegdem2]=Removetree(a,sizex,sizey,buildings,vegdem,vegdem2,idremove,vegcoord_out,scale);
            end
            point=input('Do you want to add a new vegetation unit? (Y or N):  ','s');
        end
        save_vegcoord(vegcoord_out,vegetationfile);
        if oldtextfile == 0, close_figure(figurehandler); end
    else
        [vegdem]=Solweig_10_loaddem(cdsmfile);
        if onlycdsm == 1
            vegdem2= vegdem * trunkratio;
        else
            [vegdem2]=Solweig_10_loaddem(tdsmfile);
        end
        [~,~,~,walls]=building_edges(a,sizey,sizex);
        buildings=[];
    end
    [~,~,~,walls]=building_edges(a,sizey,sizex);
else
   vegdem=[];vegdem2=[];buildings=[];
   [~,~,~,walls]=building_edges(a,sizey,sizex);
end