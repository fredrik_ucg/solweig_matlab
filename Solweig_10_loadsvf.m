function [svf,svfE,svfS,svfW,svfN,scale,header,headernum,headername,sizey,sizex]=Solweig_10_loadsvf(inputfiles)
% This function loads five SVF grids one non-directional and four others
% from the cardinal points (N,E,S,W). 
%20130208 - changed to use cell input

name=[];
for i=1:5
    [name(:,:,i), header]=Esriasciiimport(inputfiles{i});
    
    sizex=size(name,2);
    sizey=size(name,1);
    
    % put the headernumber in a vector
    for j=1:6
        [cell,rest]=strtok(header{j},' ');
        headernum(j,:)=cellstr(rest);
        headername(j,:)=cellstr(cell);
        if i == 5
            scale=1/str2num(rest);
        end
    end
    
end

% Adding rows or columns if DEMs not square
% if sizex<sizey
%     pad=zeros(sizey,(sizey-sizex),5);
%     name=[name(:,:,1:5) pad(:,:,1:5)];
% elseif sizex>sizey
%     pad=zeros((sizex-sizey),sizex,5);
%     name=[name(:,:,1:5); pad(:,:,1:5)];
% end

svf=name(:,:,1);
svfE=name(:,:,2);
svfS=name(:,:,3);
svfW=name(:,:,4);
svfN=name(:,:,5);
