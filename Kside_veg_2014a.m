function [Keast,Ksouth,Kwest,Knorth]=Kside_veg_2014a(radI,radG,radD,shadow,svfS,svfW,svfN,svfE,...
    svfEveg,svfSveg,svfWveg,svfNveg,azimuth,altitude,psi,t,albedo,F_sh)

% New reflection equation 2012-05-25
vikttot=4.4897;
aziE=azimuth+t;aziS=azimuth-90+t;aziW=azimuth-180+t;aziN=azimuth-270+t;
% sunw=cos(altitude*(pi/180)); % anngle to walls
%% Kside with weights
[viktveg,viktwall]=Kvikt_veg(svfE,svfEveg,vikttot);
svfviktbuveg=(viktwall+(viktveg)*(1-psi));
if azimuth > (360-t)  ||  azimuth <= (180-t)
    Keast=radI*shadow*cos(altitude*(pi/180))*sin(aziE*(pi/180))+...
        radD*(1-svfviktbuveg)+radG*albedo*svfviktbuveg.*(1-F_sh);%*sin(altitude*(pi/180));
else
    Keast=radD*(1-svfviktbuveg)+radG*albedo*svfviktbuveg.*(1-F_sh);%*sin(altitude*(pi/180));
end

[viktveg,viktwall]=Kvikt_veg(svfS,svfSveg,vikttot);
svfviktbuveg=(viktwall+(viktveg)*(1-psi));
if azimuth > (90-t)  &&  azimuth <= (270-t)
    Ksouth=radI*shadow*cos(altitude*(pi/180))*sin(aziS*(pi/180))+...
        radD*(1-svfviktbuveg)+radG*albedo*svfviktbuveg.*(1-F_sh);%*sin(altitude*(pi/180));
else
    Ksouth=radD*(1-svfviktbuveg)+radG*albedo*svfviktbuveg.*(1-F_sh);%*sin(altitude*(pi/180));
end

[viktveg,viktwall]=Kvikt_veg(svfW,svfWveg,vikttot);
svfviktbuveg=(viktwall+(viktveg)*(1-psi));
if azimuth > (180-t)  &&  azimuth <= (360-t)
    Kwest=radI*shadow*cos(altitude*(pi/180))*sin(aziW*(pi/180))+...
        radD*(1-svfviktbuveg)+radG*albedo*svfviktbuveg.*(1-F_sh);%*sin(altitude*(pi/180));
else
    Kwest=radD*(1-svfviktbuveg)+radG*albedo*svfviktbuveg.*(1-F_sh);%*sin(altitude*(pi/180));
end

[viktveg,viktwall]=Kvikt_veg(svfN,svfNveg,vikttot);
svfviktbuveg=(viktwall+(viktveg)*(1-psi));
if azimuth <= (90-t)  ||  azimuth > (270-t)
    Knorth=radI*shadow*cos(altitude*(pi/180))*sin(aziN*(pi/180))+...
        radD*(1-svfviktbuveg)+radG*albedo*svfviktbuveg.*(1-F_sh);%*sin(altitude*(pi/180));
else
    Knorth=radD*(1-svfviktbuveg)+radG*albedo*svfviktbuveg.*(1-F_sh);%*sin(altitude*(pi/180));
end